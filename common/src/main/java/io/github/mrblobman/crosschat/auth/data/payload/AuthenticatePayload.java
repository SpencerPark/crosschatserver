package io.github.mrblobman.crosschat.auth.data.payload;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.github.mrblobman.crosschat.auth.data.Agent;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class AuthenticatePayload implements Payload {
    private Agent agent;
    private String username;
    private String password;
    private String clientToken;

    public AuthenticatePayload(Agent agent, String username, String password, String clientToken) {
        this.agent = agent;
        this.username = username;
        this.password = password;
        this.clientToken = clientToken;
    }

    public Agent getAgent() {
        return agent;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getClientToken() {
        return clientToken;
    }

    @Override
    public JsonElement getAsJson() {
        JsonObject payload = new JsonObject();
        payload.add("agent", agent.toJson());
        payload.addProperty("username", username);
        payload.addProperty("password", password);
        payload.addProperty("clientToken", clientToken);
        return payload;
    }
}
