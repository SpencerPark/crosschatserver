package io.github.mrblobman.crosschat.auth.data.response;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import io.github.mrblobman.crosschat.auth.data.GameProfile;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class HasJoinedResponse implements Response {
    private boolean isFilled = false;

    private GameProfile profile;

    public HasJoinedResponse() {}

    public GameProfile getProfile() {
        if (!isFilled) throw new IllegalStateException("This response has not read any data yet. Response#fromJson(JsonElement)");
        return profile;
    }

    @Override
    public Response fromJson(JsonElement element) throws JsonParseException {
        this.isFilled = false;
        this.profile = GameProfile.fromJson(element);
        this.isFilled = true;
        return this;
    }
}
