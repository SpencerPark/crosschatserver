package io.github.mrblobman.crosschat.auth;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.github.mrblobman.crosschat.auth.data.response.HasJoinedResponse;
import io.github.mrblobman.crosschat.auth.data.response.Response;
import io.github.mrblobman.crosschat.auth.data.response.ErrorResponse;
import io.github.mrblobman.crosschat.auth.endpoint.*;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class AuthClient {
    private static String SESSION_SERVER_URL = "https://sessionserver.mojang.com/session/minecraft/";
    private static String HAS_JOINED_URL = SESSION_SERVER_URL + "hasJoined";
    private static String JOIN_URL = SESSION_SERVER_URL + "join";

    /**
     * Seal the class. Only static methods are required.
     */
    private AuthClient() {}

    /**
     * See:
     * <ul>
     *     <li>{@link AuthenticateEndpoint}</li>
     *     <li>{@link InvalidateEndpoint}</li>
     *     <li>{@link RefreshEndpoint}</li>
     *     <li>{@link SignoutEndpoint}</li>
     *     <li>{@link ValidateEndpoint}</li>
     * </ul>
     * @param endpoint the {@link Endpoint} being visited.
     * @return the {@link Response} from visiting the endpoint
     * @throws Exception
     */
    public static Response visitAuthEndpoint(Endpoint endpoint) throws Exception {
        HttpURLConnection connection = (HttpURLConnection) new URL(Endpoint.AUTH_URL + endpoint.getUrlSuffix()).openConnection();

        try {
            HttpUtils.post(connection, endpoint.getPayload().getAsJson());
            return HttpUtils.handleResponse(connection, endpoint::parseResponse);
        } finally {
            connection.disconnect();
        }
    }

    /**
     * Join a server. Required before an online mode server can log you in.
     * @param accessToken the clients accessToken
     * @param selectedProfile the uuid be logged in with
     * @param serverId the server hash
     * @return an {@link AuthException} if an error has occurred or null if successful
     * @throws Exception
     */
    public static AuthException joinServer(String accessToken, UUID selectedProfile, String serverId) throws Exception {
        JsonObject postPayload = new JsonObject();
        postPayload.addProperty("accessToken", accessToken);
        postPayload.addProperty("selectedProfile", selectedProfile.toString().replace("-", ""));
        postPayload.addProperty("serverId", serverId);

        HttpURLConnection connection = (HttpURLConnection) new URL(JOIN_URL).openConnection();
        try {
            HttpUtils.post(connection, postPayload);
            Response response = HttpUtils.handleResponse(connection, (reader) -> null);
            return (response == null ? null : ((ErrorResponse) response).getException());
        } finally {
            connection.disconnect();
        }
    }

    /**
     * Verify that a user has joined the server.
     * @param username the username to verify
     * @param serverId the server hash
     * @return a {@link HasJoinedResponse} on success or {@link ErrorResponse} on failure
     * @throws Exception
     */
    public static Response hasJoinedServer(String username, String serverId) throws Exception {
        URL url = HttpUtils.buildGet(HAS_JOINED_URL,
                new HttpUtils.QueryParam("username", username),
                new HttpUtils.QueryParam("serverId", serverId));

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            return HttpUtils.handleResponse(connection, (respReader) ->
                    new HasJoinedResponse().fromJson(new JsonParser().parse(respReader)));
        } finally {
            connection.disconnect();
        }
    }
}
