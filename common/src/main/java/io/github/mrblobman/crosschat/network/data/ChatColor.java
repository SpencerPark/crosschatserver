package io.github.mrblobman.crosschat.network.data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MrBlobman on 15-09-01.
 */
public enum ChatColor {
    BLACK('0', true),
    DARK_BLUE('1', true),
    DARK_GREEN('2', true),
    DARK_AQUA('3', true),
    DARK_RED('4', true),
    DARK_PURPLE('5', true),
    GOLD('6', true),
    GRAY('7', true),
    DARK_GRAY('8', true),
    BLUE('9', true),
    GREEN('a', true),
    AQUA('b', true),
    RED('c', true),
    LIGHT_PURPLE('d', true),
    YELLOW('e', true),
    WHITE('f', true),
    OBFUSCATED('k', false),
    BOLD('l', false),
    STRIKETHROUGH('m', false),
    UNDERLINE('n', false),
    ITALIC('o', false),
    RESET('r', true);

    public static char COLOR_CHAR = '\u00A7';

    private static Map<String, ChatColor> REVERSE_LOOKUP = new HashMap<>();

    static {
        for (ChatColor c : ChatColor.values()) {
            REVERSE_LOOKUP.put(c.getSerializedName(), c);
            REVERSE_LOOKUP.put(c.code, c);
        }
    }

    private String name;
    private String code;
    private boolean isColor;

    ChatColor(char code, boolean isColor) {
        name = this.name().toLowerCase();
        this.code = String.valueOf(code);
        this.isColor = isColor;
    }

    public String getSerializedName() {
        return name;
    }

    public boolean isColor() {
        return this.isColor;
    }

    @Override
    public String toString() {
        return COLOR_CHAR + this.code;
    }

    public static ChatColor getByName(String name) {
        return REVERSE_LOOKUP.get(name.toLowerCase().replace(" ", "_"));
    }

    public static ChatColor getByCode(char code) {
        switch (code) {
            case '0' : return ChatColor.BLACK;
            case '1' : return ChatColor.DARK_BLUE;
            case '2' : return ChatColor.DARK_GREEN;
            case '3' : return ChatColor.DARK_AQUA;
            case '4' : return ChatColor.DARK_RED;
            case '5' : return ChatColor.DARK_PURPLE;
            case '6' : return ChatColor.GOLD;
            case '7' : return ChatColor.GRAY;
            case '8' : return ChatColor.DARK_GRAY;
            case '9' : return ChatColor.BLUE;
            case 'a' : return ChatColor.GREEN;
            case 'b' : return ChatColor.AQUA;
            case 'c' : return ChatColor.RED;
            case 'd' : return ChatColor.LIGHT_PURPLE;
            case 'e' : return ChatColor.YELLOW;
            case 'f' : return ChatColor.WHITE;
            case 'k' : return ChatColor.OBFUSCATED;
            case 'l' : return ChatColor.BOLD;
            case 'm' : return ChatColor.STRIKETHROUGH;
            case 'n' : return ChatColor.UNDERLINE;
            case 'o' : return ChatColor.ITALIC;
            case 'r' : return ChatColor.RESET;
            default  : return null;
        }
    }
}
