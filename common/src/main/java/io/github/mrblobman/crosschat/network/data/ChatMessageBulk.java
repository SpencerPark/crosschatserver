package io.github.mrblobman.crosschat.network.data;

import com.google.gson.*;
import io.github.mrblobman.crosschat.network.data.parser.ChatMessageCompiler;
import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.io.Decodable;
import io.github.mrblobman.crosschat.network.io.Encodable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by MrBlobman on 15-09-01.
 */
public class ChatMessageBulk implements Encodable, Decodable, Iterable<ChatMessage> {
    private ChatMessage current;
    private List<ChatMessage> parts = new ArrayList<>();

    public ChatMessageBulk(String message) {
        current = new ChatMessage(message);
    }

    public ChatMessageBulk(ChatMessage message) {
        current = message;
    }

    public ChatMessageBulk setTooltip(String message) {
        current.setTooltip(message);
        return this;
    }

    public ChatMessageBulk setTooltip(String... message) {
        current.setTooltip(message);
        return this;
    }

    public ChatMessageBulk setClickAction(String action, String value) {
        current.setClickAction(action, value);
        return this;
    }

    public ChatMessageBulk setBold(boolean bold) {
        current.setBold(bold);
        return this;
    }

    public ChatMessageBulk setItalic(boolean italic) {
        current.setItalic(italic);
        return this;
    }

    public ChatMessageBulk setUnderlined(boolean underlined) {
        current.setUnderlined(underlined);
        return this;
    }

    public ChatMessageBulk setStrikethrough(boolean strikethrough) {
        current.setStrikethrough(strikethrough);
        return this;
    }

    public ChatMessageBulk setObfuscated(boolean obfuscated) {
        current.setObfuscated(obfuscated);
        return this;
    }

    public ChatMessageBulk setColor(ChatColor color) {
        current.setColor(color);
        return this;
    }

    public ChatMessageBulk setInsertionText(String text) {
        current.setInsertionText(text);
        return this;
    }

    public ChatMessageBulk appendText(String text) {
        current.setText(current.getText() + text);
        return this;
    }

    public ChatMessageBulk append(String text) {
        parts.add(current);
        current = new ChatMessage(text);
        return this;
    }

    public ChatMessageBulk append(ChatMessage msgPart) {
        parts.add(current);
        current = msgPart;
        return this;
    }

    public ChatMessageBulk append(ChatMessageBulk msg) {
        parts.add(current);
        parts.addAll(msg.parts);
        current = msg.current;
        return this;
    }

    private ChatMessageBulk() {}

    public static ChatMessageBulk read(Buffer buffer) throws Exception {
        ChatMessageBulk msg = new ChatMessageBulk();
        msg.decode(buffer);
        return msg;
    }

    public void deserialize(String rawData) throws JsonParseException {
        JsonObject msg = new JsonParser().parse(rawData).getAsJsonObject();
        if (msg.has("extra")) {
            JsonArray extra = msg.get("extra").getAsJsonArray();
            JsonElement part;
            part = extra.get(0);
            if (part.isJsonPrimitive()) {
                this.current = new ChatMessage(part.getAsString());
            } else {
                this.current = ChatMessage.fromJson(part.getAsJsonObject());
            }
            for (int i = 1; i < extra.size(); i++) {
                part = extra.get(i);
                if (part.isJsonPrimitive()) {
                    parts.add(new ChatMessage(part.getAsString()));
                } else {
                    parts.add(ChatMessage.fromJson(part.getAsJsonObject()));
                }
            }
        } else {
            if (msg.isJsonPrimitive()) {
                this.current = new ChatMessage(msg.getAsString());
            } else {
                this.current = ChatMessage.fromJson(msg.getAsJsonObject());
            }
        }
    }

    public static ChatMessageBulk fromDecoratedString(String message) {
        return new ChatMessageCompiler(message).getMessage();
    }

    @Override
    public void decode(Buffer buffer) throws Exception {
        deserialize(buffer.readString());
    }

    @Override
    public void encode(Buffer buffer) throws Exception {
        buffer.write(serialize().toString());
    }

    public JsonObject serialize() {
        JsonObject msg;
        if (parts.isEmpty()) {
            msg = current.toJson();
        } else {
            msg = parts.get(0).toJson();
            JsonArray extra = new JsonArray();
            for (int i = 1; i < parts.size(); i++) {
                extra.add(parts.get(i).toJson());
            }
            extra.add(current.toJson());
            msg.add("extra", extra);
        }
        return msg;
    }

    public String getCleanText() {
        if (parts.isEmpty()) {
            return current.getText();
        } else {
            StringBuilder msg = new StringBuilder(parts.get(0).getText());
            for (int i = 1; i < parts.size(); i++) {
                msg.append(parts.get(i).getText());
            }
            msg.append(current.getText());
            return msg.toString();
        }
    }

    @Override
    public String toString() {
        if (parts.isEmpty()) {
            return current.toString();
        } else {
            StringBuilder msg = new StringBuilder(parts.get(0).toString());
            for (int i = 1; i < parts.size(); i++) {
                msg.append(parts.get(i).toString()).append("\n");
            }
            msg.append(current.toString()).append("\n");
            return msg.toString();
        }
    }

    @Override
    public Iterator<ChatMessage> iterator() {
        List<ChatMessage> messagesSnapshot = new ArrayList<>();
        messagesSnapshot.addAll(this.parts);
        messagesSnapshot.add(this.current);
        return new ChatMessageBulkIterator(messagesSnapshot);
    }
}
