package io.github.mrblobman.crosschat.auth.data.response;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class ValidateResponse implements Response {
    private boolean isFilled = false;

    public ValidateResponse() {}

    @Override
    public Response fromJson(JsonElement element) throws JsonParseException {
        this.isFilled = false;
        this.isFilled = true;
        return this;
    }
}
