package io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.serverbound;

import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.PacketIds;

/**
 * Created by MrBlobman on 9/13/2015.
 */
public class CommandPacket extends Packet {
    private String command;

    public CommandPacket() {}

    public CommandPacket(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Override
    public byte getId() {
        return PacketIds.ServerBound.COMMAND;
    }

    @Override
    public void decode(Buffer buffer) throws Exception {
        this.command = buffer.readString();
    }

    @Override
    public void encode(Buffer buffer) throws Exception {
        buffer.write(this.command);
    }
}
