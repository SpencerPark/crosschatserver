package io.github.mrblobman.crosschat.auth.data.payload;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.github.mrblobman.crosschat.auth.data.GameProfile;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class RefreshPayload implements Payload {
    private String accessToken;
    private String clientToken;
    private GameProfile selectedProfile;

    public RefreshPayload(String accessToken, String clientToken, GameProfile selectedProfile) {
        this.accessToken = accessToken;
        this.clientToken = clientToken;
        this.selectedProfile = selectedProfile;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getClientToken() {
        return clientToken;
    }

    public GameProfile getSelectedProfile() {
        return selectedProfile;
    }

    @Override
    public JsonElement getAsJson() {
        JsonObject object = new JsonObject();
        object.addProperty("accessToken", accessToken);
        object.addProperty("clientToken", clientToken);
        object.add("selectedProfile", selectedProfile.toJson());
        return object;
    }
}
