package io.github.mrblobman.crosschat.network.packets.v1_0_R0.handshake.serverbound;

import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.PacketIds;

/**
 * Created by MrBlobman on 15-08-31.
 */
public class HandshakePacket extends Packet {
    public static final int STATUS = 1;
    public static final int LOGIN = 2;

    private int protocolVersion;
    private String hostname;
    private int port;
    private int intent;

    public HandshakePacket() {}

    public HandshakePacket(int protocolVersion, String hostname, int port, int intent) {
        this.protocolVersion = protocolVersion;
        this.hostname = hostname;
        this.port = port;
        this.intent = intent;
    }

    public int getProtocolVersion() {
        return this.protocolVersion;
    }

    public String getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }

    public int getIntent() {
        return intent;
    }

    @Override
    public byte getId() {
        return PacketIds.ServerBound.HANDSHAKE;
    }

    @Override
    public void encode(Buffer buffer) throws Exception {
        buffer.writeVarInt(this.protocolVersion);
        buffer.write(this.hostname);
        buffer.write((short) port);
        buffer.writeVarInt(intent);
    }

    @Override
    public void decode(Buffer buffer) throws Exception {
        this.protocolVersion = buffer.readVarInt();
        this.hostname = buffer.readString();
        this.port = buffer.readShort();
        this.intent = buffer.readVarInt();
    }
}
