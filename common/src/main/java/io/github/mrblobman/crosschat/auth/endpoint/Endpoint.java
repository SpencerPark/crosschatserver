package io.github.mrblobman.crosschat.auth.endpoint;

import io.github.mrblobman.crosschat.auth.data.payload.Payload;
import io.github.mrblobman.crosschat.auth.data.response.Response;

import java.io.Reader;

/**
 * Created by MrBlobman on 15-09-03.
 */
public interface Endpoint {
    public static String AUTH_URL = "https://authserver.mojang.com";

    /**
     * @return The String to be appended to the {@link Endpoint#AUTH_URL}
     */
    String getUrlSuffix();

    /**
     * Sets the payload to be sent to this endpoint.
     * @return the payload
     */
    Payload getPayload();

    /**
     * The response returned from this endpoint.
     * @param rawResponse the raw response returned from the request
     * @return the response as a {@link Response}.
     */
    Response parseResponse(Reader rawResponse);
}
