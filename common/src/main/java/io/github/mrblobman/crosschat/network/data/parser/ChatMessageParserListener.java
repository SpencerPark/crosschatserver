package io.github.mrblobman.crosschat.network.data.parser;// Generated from ChatMessageParser.g4 by ANTLR 4.5.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ChatMessageParser}.
 */
public interface ChatMessageParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ChatMessageParser#message}.
	 * @param ctx the parse tree
	 */
	void enterMessage(ChatMessageParser.MessageContext ctx);
	/**
	 * Exit a parse tree produced by {@link ChatMessageParser#message}.
	 * @param ctx the parse tree
	 */
	void exitMessage(ChatMessageParser.MessageContext ctx);
	/**
	 * Enter a parse tree produced by {@link ChatMessageParser#messagePart}.
	 * @param ctx the parse tree
	 */
	void enterMessagePart(ChatMessageParser.MessagePartContext ctx);
	/**
	 * Exit a parse tree produced by {@link ChatMessageParser#messagePart}.
	 * @param ctx the parse tree
	 */
	void exitMessagePart(ChatMessageParser.MessagePartContext ctx);
	/**
	 * Enter a parse tree produced by {@link ChatMessageParser#plainMessage}.
	 * @param ctx the parse tree
	 */
	void enterPlainMessage(ChatMessageParser.PlainMessageContext ctx);
	/**
	 * Exit a parse tree produced by {@link ChatMessageParser#plainMessage}.
	 * @param ctx the parse tree
	 */
	void exitPlainMessage(ChatMessageParser.PlainMessageContext ctx);
	/**
	 * Enter a parse tree produced by {@link ChatMessageParser#blankMessage}.
	 * @param ctx the parse tree
	 */
	void enterBlankMessage(ChatMessageParser.BlankMessageContext ctx);
	/**
	 * Exit a parse tree produced by {@link ChatMessageParser#blankMessage}.
	 * @param ctx the parse tree
	 */
	void exitBlankMessage(ChatMessageParser.BlankMessageContext ctx);
	/**
	 * Enter a parse tree produced by {@link ChatMessageParser#decoratedMessage}.
	 * @param ctx the parse tree
	 */
	void enterDecoratedMessage(ChatMessageParser.DecoratedMessageContext ctx);
	/**
	 * Exit a parse tree produced by {@link ChatMessageParser#decoratedMessage}.
	 * @param ctx the parse tree
	 */
	void exitDecoratedMessage(ChatMessageParser.DecoratedMessageContext ctx);
}