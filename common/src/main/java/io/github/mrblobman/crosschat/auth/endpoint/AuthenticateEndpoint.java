package io.github.mrblobman.crosschat.auth.endpoint;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import io.github.mrblobman.crosschat.auth.data.response.AuthenticationResponse;
import io.github.mrblobman.crosschat.auth.data.Agent;
import io.github.mrblobman.crosschat.auth.data.payload.AuthenticatePayload;
import io.github.mrblobman.crosschat.auth.data.payload.Payload;
import io.github.mrblobman.crosschat.auth.data.response.Response;

import java.io.Reader;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class AuthenticateEndpoint implements Endpoint {
    private AuthenticatePayload payload;

    public AuthenticateEndpoint(Agent agent, String username, String password, String clientToken) {
        this.payload = new AuthenticatePayload(agent, username, password, clientToken);
    }

    @Override
    public String getUrlSuffix() {
        return "/authenticate";
    }

    @Override
    public Payload getPayload() {
        return payload;
    }

    @Override
    public Response parseResponse(Reader rawResponse) {
        JsonElement element = new JsonParser().parse(rawResponse);
        return new AuthenticationResponse().fromJson(element);
    }
}
