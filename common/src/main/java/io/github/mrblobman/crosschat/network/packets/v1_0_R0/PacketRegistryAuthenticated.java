package io.github.mrblobman.crosschat.network.packets.v1_0_R0;

import io.github.mrblobman.crosschat.network.packets.PacketRegistry;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.clientbound.KickPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.clientbound.MessagePacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.clientbound.PostSetCompressionPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.serverbound.ChannelMessagePacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.serverbound.CommandPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.serverbound.PrivateMessagePacket;

/**
 * Created by MrBlobman on 15-09-01.
 */
public class PacketRegistryAuthenticated implements PacketRegistry {
    @SuppressWarnings("unchecked")
    private final Class<? extends Packet>[] clientBoundPackets = new Class[] {
            MessagePacket.class,
            KickPacket.class,
            PostSetCompressionPacket.class
    };

    @SuppressWarnings("unchecked")
    private final Class<? extends Packet>[] serverBoundPackets = new Class[] {
            ChannelMessagePacket.class,
            PrivateMessagePacket.class,
            CommandPacket.class
    };

    @Override
    public Class<? extends Packet> getPacketById(int id, boolean serverBound) {
        if (serverBound) {
            if (id < 0 || id > serverBoundPackets.length) return null;
            return serverBoundPackets[id];
        } else {
            if (id < 0 || id > clientBoundPackets.length) return null;
            return clientBoundPackets[id];
        }
    }
}
