package io.github.mrblobman.crosschat.network.packets.v1_0_R0;

import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.PacketRegistry;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.DisconnectPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.EncryptionRequestPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.LoginSuccessPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.serverbound.EncryptionResponsePacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.serverbound.LoginStartPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.SetCompressionPacket;

/**
 * Created on 15-09-01.
 */
public class PacketRegistryLogin implements PacketRegistry {
    @SuppressWarnings("unchecked")
    private final Class<? extends Packet>[] clientBoundPackets = new Class[] {
            DisconnectPacket.class,
            EncryptionRequestPacket.class,
            LoginSuccessPacket.class,
            SetCompressionPacket.class
    };

    @SuppressWarnings("unchecked")
    private final Class<? extends Packet>[] serverBoundPackets = new Class[] {
            LoginStartPacket.class,
            EncryptionResponsePacket.class
    };

    @Override
    public Class<? extends Packet> getPacketById(int id, boolean serverBound) {
        if (serverBound) {
            if (id < 0 || id > serverBoundPackets.length) return null;
            return serverBoundPackets[id];
        } else {
            if (id < 0 || id > clientBoundPackets.length) return null;
            return clientBoundPackets[id];
        }
    }
}
