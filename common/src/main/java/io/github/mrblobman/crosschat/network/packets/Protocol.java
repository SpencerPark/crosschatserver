package io.github.mrblobman.crosschat.network.packets;

import io.github.mrblobman.crosschat.network.ClientState;

/**
 * Created by MrBlobman on 15-08-31.
 */
public interface Protocol {
    PacketRegistry getRegistry(ClientState state);
    String getVersion();
    int getVersionId();
}
