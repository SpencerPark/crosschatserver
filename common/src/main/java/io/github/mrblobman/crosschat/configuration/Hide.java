package io.github.mrblobman.crosschat.configuration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Mark a field as hidden from the {@link ConfigParser}.<br>
 * Hiding a class will hide all fields not marked with @{@link Reveal}.
 * By default classes and fields are marked with @{@link Reveal}.
 */
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Hide {}
