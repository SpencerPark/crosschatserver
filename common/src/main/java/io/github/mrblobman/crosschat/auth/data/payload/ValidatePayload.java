package io.github.mrblobman.crosschat.auth.data.payload;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class ValidatePayload implements Payload {
    private String accessToken;
    private String clientToken;

    public ValidatePayload(String accessToken, String clientToken) {
        this.accessToken = accessToken;
        this.clientToken = clientToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getClientToken() {
        return clientToken;
    }

    @Override
    public JsonElement getAsJson() {
        JsonObject object = new JsonObject();
        object.addProperty("accessToken", accessToken);
        object.addProperty("clientToken", clientToken);
        return object;
    }
}
