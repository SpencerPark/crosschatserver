package io.github.mrblobman.crosschat.network.packets.v1_0_R0;

import io.github.mrblobman.crosschat.network.ClientState;
import io.github.mrblobman.crosschat.network.packets.PacketRegistry;
import io.github.mrblobman.crosschat.network.packets.Protocol;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created by MrBlobman on 15-08-31.
 */
public class ProtocolImpl implements Protocol {
    private Map<ClientState, PacketRegistry> protocols = new EnumMap<>(ClientState.class);

    public ProtocolImpl() {
        protocols.put(ClientState.HANDSHAKE, new PacketRegistryHandshake());
        protocols.put(ClientState.LOGIN, new PacketRegistryLogin());
        protocols.put(ClientState.AUTHENTICATED, new PacketRegistryAuthenticated());
        protocols.put(ClientState.STATUS, new PacketRegistryStatus());
    }

    @Override
    public PacketRegistry getRegistry(ClientState state) {
        return protocols.get(state);
    }

    @Override
    public String getVersion() {
        return "1_0_R0";
    }

    @Override
    public int getVersionId() {
        return 47;
    }
}
