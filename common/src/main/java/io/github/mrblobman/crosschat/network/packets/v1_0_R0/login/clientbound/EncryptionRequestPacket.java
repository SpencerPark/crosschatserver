package io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound;

import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.PacketIds;
import io.github.mrblobman.crosschat.util.CryptUtil;

import java.security.PublicKey;

/**
 * Created by MrBlobman on 15-08-30.
 */
public class EncryptionRequestPacket extends Packet {
    private String serverID;
    private PublicKey yellowPaint;
    private byte[] verifyToken;

    public EncryptionRequestPacket() {}

    public EncryptionRequestPacket(String serverId, PublicKey publicKey) {
        this.serverID = serverId;
        this.yellowPaint = publicKey;
        this.verifyToken = CryptUtil.generateVerifyToken(4);
    }

    public String getServerID() {
        return serverID;
    }

    public PublicKey getPublicKey() {
        return yellowPaint;
    }

    public byte[] getVerifyToken() {
        return verifyToken;
    }

    @Override
    public byte getId() {
        return PacketIds.ClientBound.ENCRYPTION_REQUEST;
    }

    @Override
    public void encode(Buffer buffer) throws Exception {
        buffer.write(serverID);
        buffer.write(CryptUtil.toX905Encoding(this.yellowPaint).getEncoded());
        buffer.write(this.verifyToken);
    }

    @Override
    public void decode(Buffer buffer) throws Exception {
        this.serverID = buffer.readString();
        this.yellowPaint = CryptUtil.decodeX905(buffer.readByteArray());
        this.verifyToken = buffer.readByteArray();
    }
}
