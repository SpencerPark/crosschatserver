package io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound;

import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.PacketIds;

import java.util.UUID;

/**
 * Created by MrBlobman on 15-09-01.
 */
public class LoginSuccessPacket extends Packet {
    private String uuid;
    private String name;

    public LoginSuccessPacket() {}

    public LoginSuccessPacket(UUID uuid, String name) {
        this.uuid = uuid.toString();
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    @Override
    public byte getId() {
        return PacketIds.ClientBound.LOGIN_SUCCESS;
    }

    @Override
    public void encode(Buffer buffer) throws Exception {
        buffer.write(this.uuid);
        buffer.write(this.name);
    }

    @Override
    public void decode(Buffer buffer) throws Exception {
        this.uuid = buffer.readString();
        this.name = buffer.readString();
    }
}
