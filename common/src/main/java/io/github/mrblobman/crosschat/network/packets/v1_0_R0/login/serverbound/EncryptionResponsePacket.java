package io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.serverbound;

import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.PacketIds;
import io.github.mrblobman.crosschat.util.CryptUtil;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * Created by MrBlobman on 15-08-30.
 */
public class EncryptionResponsePacket extends Packet {
    private byte[] sharedKey;
    private byte[] verifyToken;

    public EncryptionResponsePacket() {}

    public EncryptionResponsePacket(SecretKey secretKey, PublicKey publicKey, byte[] verifyToken) throws GeneralSecurityException {
        this.sharedKey = CryptUtil.encrypt(publicKey, secretKey.getEncoded());
        this.verifyToken = CryptUtil.encrypt(publicKey, verifyToken);
    }

    public SecretKey getSharedKey(PrivateKey privateKey) throws GeneralSecurityException {
        return new SecretKeySpec(CryptUtil.decrypt(privateKey, sharedKey), "AES");
    }

    public byte[] getVerifyToken(PrivateKey privateKey) throws GeneralSecurityException {
        return CryptUtil.decrypt(privateKey, verifyToken);
    }

    @Override
    public byte getId() {
        return PacketIds.ServerBound.ENCRYPTION_RESPONSE;
    }

    @Override
    public void encode(Buffer buffer) throws Exception {
        buffer.write(this.sharedKey);
        buffer.write(this.verifyToken);
    }

    @Override
    public void decode(Buffer buffer) throws Exception {
        this.sharedKey = buffer.readByteArray();
        this.verifyToken = buffer.readByteArray();
    }
}
