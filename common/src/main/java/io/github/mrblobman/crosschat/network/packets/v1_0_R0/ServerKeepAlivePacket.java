package io.github.mrblobman.crosschat.network.packets.v1_0_R0;

import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;

/**
 * Created by MrBlobman on 15-08-30.
 */
public class ServerKeepAlivePacket extends Packet {
    private int pingId;

    public ServerKeepAlivePacket() {}

    public ServerKeepAlivePacket(int pingId) throws Exception {
        this.pingId = pingId;
    }

    @Override
    public byte getId() {
        //TODO Use correct packet id
        return 0;
    }

    @Override
    public void encode(Buffer buffer) throws Exception {
        buffer.writeVarInt(pingId);
    }

    @Override
    public void decode(Buffer buffer) throws Exception {
        pingId = buffer.readVarInt();
    }
}
