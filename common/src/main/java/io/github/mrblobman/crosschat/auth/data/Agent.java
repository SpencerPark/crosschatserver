package io.github.mrblobman.crosschat.auth.data;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class Agent implements Data {
    private String name;
    private int version;

    public Agent(String name, int version) {
        this.name = name;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public int getVersion() {
        return version;
    }

    @Override
    public JsonElement toJson() {
        JsonObject object = new JsonObject();
        object.addProperty("name", name);
        object.addProperty("version", version);
        return object;
    }

    public static Agent fromJson(JsonElement element) throws JsonParseException {
        JsonObject object = element.getAsJsonObject();
        if (!object.has("name")) throw new JsonParseException("Missing required field 'name'");
        if (!object.has("version")) throw new JsonParseException("Missing required field 'version'");
        return new Agent(
                object.get("name").getAsString(),
                object.get("version").getAsInt());
    }
}
