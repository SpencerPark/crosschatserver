package io.github.mrblobman.crosschat.auth.endpoint;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import io.github.mrblobman.crosschat.auth.data.payload.Payload;
import io.github.mrblobman.crosschat.auth.data.payload.SignoutPayload;
import io.github.mrblobman.crosschat.auth.data.response.Response;
import io.github.mrblobman.crosschat.auth.data.response.SignoutResponse;

import java.io.Reader;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class SignoutEndpoint implements Endpoint {
    private SignoutPayload payload;

    public SignoutEndpoint(String username, String password) {
        this.payload = new SignoutPayload(username, password);
    }

    @Override
    public String getUrlSuffix() {
        return "/signout";
    }

    @Override
    public Payload getPayload() {
        return payload;
    }

    @Override
    public Response parseResponse(Reader rawResponse) {
        JsonElement element = new JsonParser().parse(rawResponse);
        return new SignoutResponse().fromJson(element);
    }
}
