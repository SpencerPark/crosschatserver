package io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.clientbound;

import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.PacketIds;

/**
 * Created on 02/10/2015.
 */
public class PostSetCompressionPacket extends Packet {
    private int threshold;

    public PostSetCompressionPacket() { }

    public PostSetCompressionPacket(int threshold) {
        this.threshold = threshold;
    }

    public int getThreshold() {
        return this.threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public byte getId() {
        return PacketIds.ClientBound.POSTSET_COMPRESSION;
    }

    @Override
    public void decode(Buffer buffer) throws Exception {
        this.threshold = buffer.readVarInt();
    }

    @Override
    public void encode(Buffer buffer) throws Exception {
        buffer.writeVarInt(this.threshold);
    }
}

