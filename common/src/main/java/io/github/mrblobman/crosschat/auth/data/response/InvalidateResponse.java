package io.github.mrblobman.crosschat.auth.data.response;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class InvalidateResponse implements Response {
    private boolean isFilled = false;

    public InvalidateResponse() {}

    @Override
    public Response fromJson(JsonElement element) throws JsonParseException {
        this.isFilled = false;
        this.isFilled = true;
        return this;
    }
}
