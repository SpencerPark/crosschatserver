package io.github.mrblobman.crosschat.util;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;

/**
 * Created by MrBlobman on 15-08-30.
 */
public class CryptUtil {
    public static final String CIPHER_ALGORITHM = "RSA/ECB/PKCS1Padding";
    private static SecureRandom random = new SecureRandom();

    public static byte[] generateVerifyToken(int size) {
        byte[] token = new byte[size];
        random.nextBytes(token);
        return token;
    }

    public static SecretKey generateSecretKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(128);
        return keyGenerator.generateKey();
    }

    public static KeyPair generateRSAKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(1024);
        return generator.generateKeyPair();
    }

    public static Key toX905Encoding(Key toEncode) throws GeneralSecurityException {
        X509EncodedKeySpec encodedKeySpec = new X509EncodedKeySpec(toEncode.getEncoded());
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(encodedKeySpec);
    }

    public static PublicKey decodeX905(byte[] encoded) throws GeneralSecurityException {
        X509EncodedKeySpec encodedKeySpec = new X509EncodedKeySpec(encoded);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(encodedKeySpec);
    }

    public static byte[] encrypt(Key key, byte[] data) throws GeneralSecurityException {
        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(data);
    }

    public static byte[] decrypt(Key key, byte[] data) throws GeneralSecurityException {
        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(data);
    }

    public static String generateServerHash(SecretKey sharedSecret, PublicKey publicKey, String serverId) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        digest.update(serverId.getBytes("ISO_8859_1"));
        digest.update(sharedSecret.getEncoded());
        digest.update(publicKey.getEncoded());
        return new BigInteger(digest.digest()).toString(16);
    }
}
