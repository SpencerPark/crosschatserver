package io.github.mrblobman.crosschat.configuration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Mark a field as visible by the {@link ConfigParser}.<br>
 * Revealing a class will reveal all fields not marked with @{@link Hide}.
 * By default classes and fields are marked with @{@link Reveal}.
 */
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Reveal {}
