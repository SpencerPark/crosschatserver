package io.github.mrblobman.crosschat.network.data.parser;// Generated from ChatMessageLexer.g4 by ANTLR 4.5.1

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.LexerATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ChatMessageLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COLOR_CODE=1, FORMATTING_CODE=2, L_BRACKET=3, R_BRACKET=4, L_BRACE=5, 
		R_BRACE=6, ATTRIBUTE=7, ESC_CHAR=8, WS=9, CHAR=10, STRING=11;
	public static final int ESCAPED = 1;
	public static String[] modeNames = {
		"DEFAULT_MODE", "ESCAPED"
	};

	public static final String[] ruleNames = {
		"COLOR_CODE", "FORMATTING_CODE", "L_BRACKET", "R_BRACKET", "L_BRACE", 
		"R_BRACE", "ATTRIBUTE", "URL_ATTR_NAME", "URL_PARAM", "STRING_ATTR_NAME", 
		"STRING_PARAM", "STRING_LIST_ATTR_NAME", "STRING_LIST_PARAM", "ASSIGN", 
		"TEXT", "ESC_CHAR", "LOWERCASE_LETTER", "URL", "WS", "CHAR", "STRING", 
		"STRING_LIST", "SERVER_CHAR", "A", "B", "C", "D", "E", "F", "G", "H", 
		"I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", 
		"W", "X", "Y", "Z", "UNICODE", "HEX", "SPECIAL_CHAR", "ESCAPED_CHAR", 
		"UNESC"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'&'", null, "'['", "']'", "'{'", "'}'", null, "'\\'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "COLOR_CODE", "FORMATTING_CODE", "L_BRACKET", "R_BRACKET", "L_BRACE", 
		"R_BRACE", "ATTRIBUTE", "ESC_CHAR", "WS", "CHAR", "STRING"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public ChatMessageLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "ChatMessageLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 49:
			UNICODE_action((RuleContext)_localctx, actionIndex);
			break;
		case 51:
			SPECIAL_CHAR_action((RuleContext)_localctx, actionIndex);
			break;
		case 53:
			UNESC_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void UNICODE_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			setText(Character.toString((char) Integer.parseInt(getText().substring(1), 16)));
			break;
		}
	}
	private void SPECIAL_CHAR_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:
			setText("\t");
			break;
		case 2:
			setText("\n");
			break;
		case 3:
			setText("\r");
			break;
		}
	}
	private void UNESC_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 4:
			setText("\\"+getText());
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\r\u0186\b\1\b\1\4"+
		"\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n"+
		"\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64"+
		"\t\64\4\65\t\65\4\66\t\66\4\67\t\67\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3"+
		"\6\3\6\3\7\3\7\3\b\3\b\3\b\5\b\u0080\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\5\t\u008b\n\t\3\n\3\n\7\n\u008f\n\n\f\n\16\n\u0092\13\n\3\n\3\n"+
		"\7\n\u0096\n\n\f\n\16\n\u0099\13\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3"+
		"\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3"+
		"\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u00b8\n\13\3\f\3\f\7\f\u00bc"+
		"\n\f\f\f\16\f\u00bf\13\f\3\f\3\f\7\f\u00c3\n\f\f\f\16\f\u00c6\13\f\3\f"+
		"\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u00d8"+
		"\n\r\3\16\3\16\7\16\u00dc\n\16\f\16\16\16\u00df\13\16\3\16\3\16\7\16\u00e3"+
		"\n\16\f\16\16\16\u00e6\13\16\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3"+
		"\21\3\21\3\21\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\5\23\u00fb\n\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\6\23\u0103\n\23\r\23\16\23\u0104\3\23\3"+
		"\23\3\23\3\23\5\23\u010b\n\23\3\23\3\23\7\23\u010f\n\23\f\23\16\23\u0112"+
		"\13\23\5\23\u0114\n\23\3\24\6\24\u0117\n\24\r\24\16\24\u0118\3\25\3\25"+
		"\3\26\3\26\6\26\u011f\n\26\r\26\16\26\u0120\3\27\3\27\3\27\7\27\u0126"+
		"\n\27\f\27\16\27\u0129\13\27\3\27\3\27\3\30\3\30\5\30\u012f\n\30\3\31"+
		"\3\31\3\32\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3 \3"+
		" \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3\'\3\'\3(\3(\3)\3)\3*\3*\3+\3"+
		"+\3,\3,\3-\3-\3.\3.\3/\3/\3\60\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\63"+
		"\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\64\3\64\3\65\3\65\3\65\3\65\3\65"+
		"\3\65\5\65\u0177\n\65\3\65\3\65\3\65\3\66\3\66\3\66\3\66\3\66\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\2\28\4\3\6\4\b\5\n\6\f\7\16\b\20\t\22\2\24\2\26\2"+
		"\30\2\32\2\34\2\36\2 \2\"\n$\2&\2(\13*\f,\r.\2\60\2\62\2\64\2\66\28\2"+
		":\2<\2>\2@\2B\2D\2F\2H\2J\2L\2N\2P\2R\2T\2V\2X\2Z\2\\\2^\2`\2b\2d\2f\2"+
		"h\2j\2l\2n\2\4\2\3$\6\2\62;chmqtt\6\2\62;C\\aac|\3\2c|\5\2\13\f\17\17"+
		"\"\"\6\2((]_}}\177\177\4\2CCcc\4\2DDdd\4\2EEee\4\2FFff\4\2GGgg\4\2HHh"+
		"h\4\2IIii\4\2JJjj\4\2KKkk\4\2LLll\4\2MMmm\4\2NNnn\4\2OOoo\4\2PPpp\4\2"+
		"QQqq\4\2RRrr\4\2SSss\4\2TTtt\4\2UUuu\4\2VVvv\4\2WWww\4\2XXxx\4\2YYyy\4"+
		"\2ZZzz\4\2[[{{\4\2\\\\||\5\2\62;CHch\5\2((]_}\177\13\2((\61\61]_ddhhp"+
		"pttvv}\177\u0176\2\4\3\2\2\2\2\6\3\2\2\2\2\b\3\2\2\2\2\n\3\2\2\2\2\f\3"+
		"\2\2\2\2\16\3\2\2\2\2\20\3\2\2\2\2\"\3\2\2\2\2(\3\2\2\2\2*\3\2\2\2\2,"+
		"\3\2\2\2\3f\3\2\2\2\3j\3\2\2\2\3l\3\2\2\2\3n\3\2\2\2\4p\3\2\2\2\6r\3\2"+
		"\2\2\bt\3\2\2\2\nv\3\2\2\2\fx\3\2\2\2\16z\3\2\2\2\20\177\3\2\2\2\22\u008a"+
		"\3\2\2\2\24\u008c\3\2\2\2\26\u00b7\3\2\2\2\30\u00b9\3\2\2\2\32\u00d7\3"+
		"\2\2\2\34\u00d9\3\2\2\2\36\u00e9\3\2\2\2 \u00eb\3\2\2\2\"\u00ed\3\2\2"+
		"\2$\u00f2\3\2\2\2&\u00f4\3\2\2\2(\u0116\3\2\2\2*\u011a\3\2\2\2,\u011e"+
		"\3\2\2\2.\u0127\3\2\2\2\60\u012e\3\2\2\2\62\u0130\3\2\2\2\64\u0132\3\2"+
		"\2\2\66\u0134\3\2\2\28\u0136\3\2\2\2:\u0138\3\2\2\2<\u013a\3\2\2\2>\u013c"+
		"\3\2\2\2@\u013e\3\2\2\2B\u0140\3\2\2\2D\u0142\3\2\2\2F\u0144\3\2\2\2H"+
		"\u0146\3\2\2\2J\u0148\3\2\2\2L\u014a\3\2\2\2N\u014c\3\2\2\2P\u014e\3\2"+
		"\2\2R\u0150\3\2\2\2T\u0152\3\2\2\2V\u0154\3\2\2\2X\u0156\3\2\2\2Z\u0158"+
		"\3\2\2\2\\\u015a\3\2\2\2^\u015c\3\2\2\2`\u015e\3\2\2\2b\u0160\3\2\2\2"+
		"d\u0162\3\2\2\2f\u0164\3\2\2\2h\u016e\3\2\2\2j\u0176\3\2\2\2l\u017b\3"+
		"\2\2\2n\u0180\3\2\2\2pq\7(\2\2q\5\3\2\2\2rs\t\2\2\2s\7\3\2\2\2tu\7]\2"+
		"\2u\t\3\2\2\2vw\7_\2\2w\13\3\2\2\2xy\7}\2\2y\r\3\2\2\2z{\7\177\2\2{\17"+
		"\3\2\2\2|\u0080\5\24\n\2}\u0080\5\30\f\2~\u0080\5\34\16\2\177|\3\2\2\2"+
		"\177}\3\2\2\2\177~\3\2\2\2\u0080\21\3\2\2\2\u0081\u0082\5H$\2\u0082\u0083"+
		"\5B!\2\u0083\u0084\5L&\2\u0084\u0085\5F#\2\u0085\u008b\3\2\2\2\u0086\u0087"+
		"\5Z-\2\u0087\u0088\5T*\2\u0088\u0089\5H$\2\u0089\u008b\3\2\2\2\u008a\u0081"+
		"\3\2\2\2\u008a\u0086\3\2\2\2\u008b\23\3\2\2\2\u008c\u0090\5\22\t\2\u008d"+
		"\u008f\5(\24\2\u008e\u008d\3\2\2\2\u008f\u0092\3\2\2\2\u0090\u008e\3\2"+
		"\2\2\u0090\u0091\3\2\2\2\u0091\u0093\3\2\2\2\u0092\u0090\3\2\2\2\u0093"+
		"\u0097\5\36\17\2\u0094\u0096\5(\24\2\u0095\u0094\3\2\2\2\u0096\u0099\3"+
		"\2\2\2\u0097\u0095\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u009a\3\2\2\2\u0099"+
		"\u0097\3\2\2\2\u009a\u009b\5&\23\2\u009b\25\3\2\2\2\u009c\u009d\5V+\2"+
		"\u009d\u009e\5Z-\2\u009e\u009f\5>\37\2\u009f\u00a0\5>\37\2\u00a0\u00a1"+
		"\5:\35\2\u00a1\u00a2\5V+\2\u00a2\u00a3\5X,\2\u00a3\u00b8\3\2\2\2\u00a4"+
		"\u00a5\5\66\33\2\u00a5\u00a6\5N\'\2\u00a6\u00a7\5J%\2\u00a7\u00a8\5J%"+
		"\2\u00a8\u00a9\5\62\31\2\u00a9\u00aa\5L&\2\u00aa\u00ab\58\34\2\u00ab\u00b8"+
		"\3\2\2\2\u00ac\u00ad\5\66\33\2\u00ad\u00ae\5J%\2\u00ae\u00af\58\34\2\u00af"+
		"\u00b8\3\2\2\2\u00b0\u00b1\5B!\2\u00b1\u00b2\5L&\2\u00b2\u00b3\5V+\2\u00b3"+
		"\u00b4\5:\35\2\u00b4\u00b5\5T*\2\u00b5\u00b6\5X,\2\u00b6\u00b8\3\2\2\2"+
		"\u00b7\u009c\3\2\2\2\u00b7\u00a4\3\2\2\2\u00b7\u00ac\3\2\2\2\u00b7\u00b0"+
		"\3\2\2\2\u00b8\27\3\2\2\2\u00b9\u00bd\5\26\13\2\u00ba\u00bc\5(\24\2\u00bb"+
		"\u00ba\3\2\2\2\u00bc\u00bf\3\2\2\2\u00bd\u00bb\3\2\2\2\u00bd\u00be\3\2"+
		"\2\2\u00be\u00c0\3\2\2\2\u00bf\u00bd\3\2\2\2\u00c0\u00c4\5\36\17\2\u00c1"+
		"\u00c3\5(\24\2\u00c2\u00c1\3\2\2\2\u00c3\u00c6\3\2\2\2\u00c4\u00c2\3\2"+
		"\2\2\u00c4\u00c5\3\2\2\2\u00c5\u00c7\3\2\2\2\u00c6\u00c4\3\2\2\2\u00c7"+
		"\u00c8\5,\26\2\u00c8\31\3\2\2\2\u00c9\u00ca\5@ \2\u00ca\u00cb\5N\'\2\u00cb"+
		"\u00cc\5\\.\2\u00cc\u00cd\5:\35\2\u00cd\u00ce\5T*\2\u00ce\u00d8\3\2\2"+
		"\2\u00cf\u00d0\5X,\2\u00d0\u00d1\5N\'\2\u00d1\u00d2\5N\'\2\u00d2\u00d3"+
		"\5H$\2\u00d3\u00d4\5X,\2\u00d4\u00d5\5B!\2\u00d5\u00d6\5P(\2\u00d6\u00d8"+
		"\3\2\2\2\u00d7\u00c9\3\2\2\2\u00d7\u00cf\3\2\2\2\u00d8\33\3\2\2\2\u00d9"+
		"\u00dd\5\32\r\2\u00da\u00dc\5(\24\2\u00db\u00da\3\2\2\2\u00dc\u00df\3"+
		"\2\2\2\u00dd\u00db\3\2\2\2\u00dd\u00de\3\2\2\2\u00de\u00e0\3\2\2\2\u00df"+
		"\u00dd\3\2\2\2\u00e0\u00e4\5\36\17\2\u00e1\u00e3\5(\24\2\u00e2\u00e1\3"+
		"\2\2\2\u00e3\u00e6\3\2\2\2\u00e4\u00e2\3\2\2\2\u00e4\u00e5\3\2\2\2\u00e5"+
		"\u00e7\3\2\2\2\u00e6\u00e4\3\2\2\2\u00e7\u00e8\5.\27\2\u00e8\35\3\2\2"+
		"\2\u00e9\u00ea\7?\2\2\u00ea\37\3\2\2\2\u00eb\u00ec\t\3\2\2\u00ec!\3\2"+
		"\2\2\u00ed\u00ee\7^\2\2\u00ee\u00ef\3\2\2\2\u00ef\u00f0\b\21\2\2\u00f0"+
		"\u00f1\b\21\3\2\u00f1#\3\2\2\2\u00f2\u00f3\t\4\2\2\u00f3%\3\2\2\2\u00f4"+
		"\u00f5\7j\2\2\u00f5\u00f6\7v\2\2\u00f6\u00f7\7v\2\2\u00f7\u00f8\7r\2\2"+
		"\u00f8\u00fa\3\2\2\2\u00f9\u00fb\7u\2\2\u00fa\u00f9\3\2\2\2\u00fa\u00fb"+
		"\3\2\2\2\u00fb\u00fc\3\2\2\2\u00fc\u00fd\7<\2\2\u00fd\u00fe\7\61\2\2\u00fe"+
		"\u00ff\7\61\2\2\u00ff\u0100\3\2\2\2\u0100\u0102\5\60\30\2\u0101\u0103"+
		"\5\60\30\2\u0102\u0101\3\2\2\2\u0103\u0104\3\2\2\2\u0104\u0102\3\2\2\2"+
		"\u0104\u0105\3\2\2\2\u0105\u0106\3\2\2\2\u0106\u0107\7\60\2\2\u0107\u0108"+
		"\5$\22\2\u0108\u010a\5$\22\2\u0109\u010b\5$\22\2\u010a\u0109\3\2\2\2\u010a"+
		"\u010b\3\2\2\2\u010b\u0113\3\2\2\2\u010c\u0110\7\61\2\2\u010d\u010f\5"+
		" \20\2\u010e\u010d\3\2\2\2\u010f\u0112\3\2\2\2\u0110\u010e\3\2\2\2\u0110"+
		"\u0111\3\2\2\2\u0111\u0114\3\2\2\2\u0112\u0110\3\2\2\2\u0113\u010c\3\2"+
		"\2\2\u0113\u0114\3\2\2\2\u0114\'\3\2\2\2\u0115\u0117\t\5\2\2\u0116\u0115"+
		"\3\2\2\2\u0117\u0118\3\2\2\2\u0118\u0116\3\2\2\2\u0118\u0119\3\2\2\2\u0119"+
		")\3\2\2\2\u011a\u011b\n\6\2\2\u011b+\3\2\2\2\u011c\u011f\5*\25\2\u011d"+
		"\u011f\5(\24\2\u011e\u011c\3\2\2\2\u011e\u011d\3\2\2\2\u011f\u0120\3\2"+
		"\2\2\u0120\u011e\3\2\2\2\u0120\u0121\3\2\2\2\u0121-\3\2\2\2\u0122\u0123"+
		"\5,\26\2\u0123\u0124\7~\2\2\u0124\u0126\3\2\2\2\u0125\u0122\3\2\2\2\u0126"+
		"\u0129\3\2\2\2\u0127\u0125\3\2\2\2\u0127\u0128\3\2\2\2\u0128\u012a\3\2"+
		"\2\2\u0129\u0127\3\2\2\2\u012a\u012b\5,\26\2\u012b/\3\2\2\2\u012c\u012f"+
		"\5 \20\2\u012d\u012f\4/\60\2\u012e\u012c\3\2\2\2\u012e\u012d\3\2\2\2\u012f"+
		"\61\3\2\2\2\u0130\u0131\t\7\2\2\u0131\63\3\2\2\2\u0132\u0133\t\b\2\2\u0133"+
		"\65\3\2\2\2\u0134\u0135\t\t\2\2\u0135\67\3\2\2\2\u0136\u0137\t\n\2\2\u0137"+
		"9\3\2\2\2\u0138\u0139\t\13\2\2\u0139;\3\2\2\2\u013a\u013b\t\f\2\2\u013b"+
		"=\3\2\2\2\u013c\u013d\t\r\2\2\u013d?\3\2\2\2\u013e\u013f\t\16\2\2\u013f"+
		"A\3\2\2\2\u0140\u0141\t\17\2\2\u0141C\3\2\2\2\u0142\u0143\t\20\2\2\u0143"+
		"E\3\2\2\2\u0144\u0145\t\21\2\2\u0145G\3\2\2\2\u0146\u0147\t\22\2\2\u0147"+
		"I\3\2\2\2\u0148\u0149\t\23\2\2\u0149K\3\2\2\2\u014a\u014b\t\24\2\2\u014b"+
		"M\3\2\2\2\u014c\u014d\t\25\2\2\u014dO\3\2\2\2\u014e\u014f\t\26\2\2\u014f"+
		"Q\3\2\2\2\u0150\u0151\t\27\2\2\u0151S\3\2\2\2\u0152\u0153\t\30\2\2\u0153"+
		"U\3\2\2\2\u0154\u0155\t\31\2\2\u0155W\3\2\2\2\u0156\u0157\t\32\2\2\u0157"+
		"Y\3\2\2\2\u0158\u0159\t\33\2\2\u0159[\3\2\2\2\u015a\u015b\t\34\2\2\u015b"+
		"]\3\2\2\2\u015c\u015d\t\35\2\2\u015d_\3\2\2\2\u015e\u015f\t\36\2\2\u015f"+
		"a\3\2\2\2\u0160\u0161\t\37\2\2\u0161c\3\2\2\2\u0162\u0163\t \2\2\u0163"+
		"e\3\2\2\2\u0164\u0165\7w\2\2\u0165\u0166\5h\64\2\u0166\u0167\5h\64\2\u0167"+
		"\u0168\5h\64\2\u0168\u0169\5h\64\2\u0169\u016a\b\63\4\2\u016a\u016b\3"+
		"\2\2\2\u016b\u016c\b\63\5\2\u016c\u016d\b\63\6\2\u016dg\3\2\2\2\u016e"+
		"\u016f\t!\2\2\u016fi\3\2\2\2\u0170\u0171\7v\2\2\u0171\u0177\b\65\7\2\u0172"+
		"\u0173\7p\2\2\u0173\u0177\b\65\b\2\u0174\u0175\7t\2\2\u0175\u0177\b\65"+
		"\t\2\u0176\u0170\3\2\2\2\u0176\u0172\3\2\2\2\u0176\u0174\3\2\2\2\u0177"+
		"\u0178\3\2\2\2\u0178\u0179\b\65\5\2\u0179\u017a\b\65\6\2\u017ak\3\2\2"+
		"\2\u017b\u017c\t\"\2\2\u017c\u017d\3\2\2\2\u017d\u017e\b\66\5\2\u017e"+
		"\u017f\b\66\6\2\u017fm\3\2\2\2\u0180\u0181\n#\2\2\u0181\u0182\b\67\n\2"+
		"\u0182\u0183\3\2\2\2\u0183\u0184\b\67\5\2\u0184\u0185\b\67\6\2\u0185o"+
		"\3\2\2\2\31\2\3\177\u008a\u0090\u0097\u00b7\u00bd\u00c4\u00d7\u00dd\u00e4"+
		"\u00fa\u0104\u010a\u0110\u0113\u0118\u011e\u0120\u0127\u012e\u0176\13"+
		"\7\3\2\b\2\2\3\63\2\6\2\2\t\r\2\3\65\3\3\65\4\3\65\5\3\67\6";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}