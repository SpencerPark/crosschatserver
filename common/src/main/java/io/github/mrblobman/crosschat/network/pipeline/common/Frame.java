package io.github.mrblobman.crosschat.network.pipeline.common;

import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.io.BufferReadException;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;

import java.util.List;

/**
 * Created by MrBlobman on 15-09-01.
 * Stalls message decoding until enough bytes make it to the server
 * to properly read the packet.
 */
public class Frame extends ByteToMessageCodec<ByteBuf> {

    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf in, ByteBuf out) throws Exception {
        Buffer buffer = new Buffer(out);
        buffer.writeVarInt(in.readableBytes());
        buffer.copy(in);
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        in.markReaderIndex();
        Buffer buffer = new Buffer(in);
        if (!buffer.canReadVarInt()) return;
        int size;
        try {
            size = buffer.readVarInt();
        } catch (BufferReadException e) {
            in.resetReaderIndex();
            return;
        }
        if (in.readableBytes() < size) {
            //Not enough bytes yet
            in.resetReaderIndex();
            return;
        }
        out.add(in.readBytes(size));
    }
}
