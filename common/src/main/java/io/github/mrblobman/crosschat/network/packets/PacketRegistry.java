package io.github.mrblobman.crosschat.network.packets;

/**
 * Created by MrBlobman on 15-08-31.
 */
public interface PacketRegistry {
    Class<? extends Packet> getPacketById(int id, boolean serverBound);
}
