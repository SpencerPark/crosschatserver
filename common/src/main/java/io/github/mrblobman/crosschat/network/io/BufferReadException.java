package io.github.mrblobman.crosschat.network.io;

/**
 * Created by MrBlobman on 15-08-30.
 */
public class BufferReadException extends Exception {
    public BufferReadException(String message) {
        super(message);
    }

    public BufferReadException(String message, Throwable cause) {
        super(message, cause);
    }

    public BufferReadException(Throwable cause) {
        super(cause);
    }

    public BufferReadException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
