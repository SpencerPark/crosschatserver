package io.github.mrblobman.crosschat.network.data.parser;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.data.ChatColor;
import io.github.mrblobman.crosschat.network.data.ChatMessage;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.Stack;

/**
 * Created on 11/10/2015.
 */
public class ChatMessageCompiler extends ChatMessageParserBaseListener {
    private ChatMessageBulk message;
    private boolean decorationsChanged = true;
    private Stack<ChatMessageParser.DecoratedMessageContext> decorationContexts;

    public ChatMessageCompiler(String rawMessage) {
        ChatMessageLexer lexer = new ChatMessageLexer(new ANTLRInputStream(rawMessage));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ChatMessageParser parser = new ChatMessageParser(tokens);
        parser.setErrorHandler(new BailErrorStrategy());
        ParseTreeWalker walker = new ParseTreeWalker();
        this.decorationContexts = new Stack<>();
        try {
            walker.walk(this, parser.message());
        } catch (ParseCancellationException e) {
            message = new ChatMessageBulk(rawMessage);
        }
    }

    private ChatMessage decorate(ChatMessage msgPart, ChatMessageParser.DecoratedMessageContext decorations) {
        if (decorations.codes != null && !decorations.codes.isEmpty())
            for (Token code : decorations.codes)
                    msgPart.setColor(ChatColor.getByCode(code.getText().charAt(0)));

        for (Token attribute : decorations.attrs) {
            String[] attr = attribute.getText().split("=");
            String action = attr[0].toLowerCase();
            String value = attr[1];
            switch (action) {
                case "link":
                case "url":
                    msgPart.setClickAction(ChatMessage.CLICK_OPEN_URL, value);
                    break;
                case "suggest":
                    msgPart.setClickAction(ChatMessage.CLICK_SUGGEST_COMMAND, value);
                    break;
                case "command":
                case "cmd":
                    msgPart.setClickAction(ChatMessage.CLICK_RUN_COMMAND, value);
                    break;
                case "hover":
                case "tooltip":
                    msgPart.setTooltip(value.split("\\n"));
                    break;
                case "insert":
                    msgPart.setInsertionText(value);
                    break;
            }
        }
        return msgPart;
    }

    private void addMessagePart(String message, boolean blank) {
        if (decorationsChanged && !blank) {
            ChatMessage chatMessage = new ChatMessage(message);
            for (ChatMessageParser.DecoratedMessageContext decoration : this.decorationContexts)
                decorate(chatMessage, decoration);
            if (this.message == null) this.message = new ChatMessageBulk(chatMessage);
            else                      this.message.append(chatMessage);
            decorationsChanged = false;
        } else {
            if (this.message == null) this.message = new ChatMessageBulk(message);
            else                      this.message.appendText(message);
        }
    }

    public ChatMessageBulk getMessage() {
        return this.message;
    }

    @Override
    public void enterPlainMessage(ChatMessageParser.PlainMessageContext ctx) {
        addMessagePart(ctx.getText(), false);
    }

    @Override
    public void enterBlankMessage(ChatMessageParser.BlankMessageContext ctx) {
        addMessagePart(ctx.getText(), true);
    }

    @Override
    public void enterDecoratedMessage(ChatMessageParser.DecoratedMessageContext ctx) {
        decorationsChanged = true;
        this.decorationContexts.push(ctx);
    }

    @Override
    public void exitDecoratedMessage(ChatMessageParser.DecoratedMessageContext ctx) {
        decorationsChanged = true;
        this.decorationContexts.pop();
    }
}
