package io.github.mrblobman.crosschat.network;

/**
 * Created by MrBlobman on 15-08-30.
 */
public enum ClientState {
    /**
     * The client is introducing themselves to the server.
     */
    HANDSHAKE,

    /**
     * The client is in the middle of logging in. Authenticating their
     * profile, exchanging encryption keys and compression thresholds.
     */
    LOGIN,

    /**
     * The client is logged in and can go about their business on
     * the server.
     */
    AUTHENTICATED,

    /**
     * The client is not joining the server, just grabbing info about it.
     */
    STATUS;
}
