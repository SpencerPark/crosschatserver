package io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.clientbound;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.data.MessageScope;
import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.PacketIds;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class MessagePacket extends Packet {
    private String sender;
    private MessageScope scope;
    private ChatMessageBulk message;

    public MessagePacket() {}

    public MessagePacket(String sender, MessageScope scope, ChatMessageBulk message) {
        this.sender = sender;
        this.scope = scope;
        this.message = message;
    }

    public ChatMessageBulk getMessage() {
        return message;
    }

    public MessageScope getScope() {
        return scope;
    }

    public String getSender() {
        return this.sender;
    }

    @Override
    public byte getId() {
        return PacketIds.ClientBound.MESSAGE;
    }

    @Override
    public void decode(Buffer buffer) throws Exception {
        this.scope = MessageScope.getById(buffer.readByte());
        this.message = ChatMessageBulk.read(buffer);
        this.sender = buffer.readString();
    }

    @Override
    public void encode(Buffer buffer) throws Exception {
        buffer.write((byte) scope.ordinal());
        this.message.encode(buffer);
        buffer.write(this.sender);
    }
}
