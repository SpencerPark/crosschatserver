package io.github.mrblobman.crosschat.auth;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MrBlobman on 15-09-03.
 */
public enum AuthException {
    METHOD_NOT_ALLOWED("Method Not Allowed", "The method specified in the request is not allowed for the resource identified by the request URI"),
    NON_EXISTING_ENDPOINT("Not Found", "The server has not found anything matching the request URI"),
    USER_MIGRATED_EXCEPTION("ForbiddenOperationException", "Invalid credentials. Account migrated, use e-mail as username."),
    INVALID_CREDENTIALS("ForbiddenOperationException", "Invalid credentials. Invalid username or password."),
    TOO_MANY_LOGIN_ATTEMPTS("ForbiddenOperationException", "Invalid credentials."),
    INVALID_ACCESS_TOKEN("ForbiddenOperationException", "Invalid token."),
    PROFILE_ALREADY_SELECTED("IllegalArgumentException", "Access token already has a profile assigned."),
    ILLEGAL_CREDENTIALS("IllegalArgumentException", "credentials is null"),
    INVALID_FORMAT("Unsupported Media Type", "The server is refusing to service the request because the entity of the request is in a format not supported by the requested resource for the requested method");

    private String error;
    private String errorMsg;

    AuthException(String error, String errorMsg) {
        this.error = error;
        this.errorMsg = errorMsg;
    }

    public String getError() {
        return error;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    private static Map<String, Map<String, AuthException>> lookup = new HashMap<>();

    static {
        for (AuthException exception : AuthException.values()) {
            if (lookup.containsKey(exception.error)) {
               lookup.get(exception.error).put(exception.errorMsg, exception);
            } else {
                Map<String, AuthException> exceptions = new HashMap<>();
                exceptions.put(exception.errorMsg, exception);
                lookup.put(exception.error, exceptions);
            }
        }
    }

    public static AuthException getFromResponse(JsonObject error) {
        if (error == null) return null;
        if (!error.has("error") || !error.has("errorMessage")) return null;
        Map<String, AuthException> possibleExceptions = lookup.get(error.get("error").getAsString());
        if (possibleExceptions == null) return null;
        return possibleExceptions.get(error.get("errorMessage").getAsString());
    }
}
