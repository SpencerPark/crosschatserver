package io.github.mrblobman.crosschat.auth.endpoint;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import io.github.mrblobman.crosschat.auth.data.GameProfile;
import io.github.mrblobman.crosschat.auth.data.payload.Payload;
import io.github.mrblobman.crosschat.auth.data.payload.RefreshPayload;
import io.github.mrblobman.crosschat.auth.data.response.RefreshResponse;
import io.github.mrblobman.crosschat.auth.data.response.Response;

import java.io.Reader;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class RefreshEndpoint implements Endpoint {
    private RefreshPayload payload;

    public RefreshEndpoint(String accessToken, String clientToken, GameProfile selectedProfile) {
        this.payload = new RefreshPayload(accessToken, clientToken, selectedProfile);
    }

    @Override
    public String getUrlSuffix() {
        return "/refresh";
    }

    @Override
    public Payload getPayload() {
        return this.payload;
    }

    @Override
    public Response parseResponse(Reader rawResponse) {
        JsonElement element = new JsonParser().parse(rawResponse);
        return new RefreshResponse().fromJson(element);
    }
}
