package io.github.mrblobman.crosschat.auth.endpoint;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import io.github.mrblobman.crosschat.auth.data.payload.Payload;
import io.github.mrblobman.crosschat.auth.data.payload.ValidatePayload;
import io.github.mrblobman.crosschat.auth.data.response.Response;
import io.github.mrblobman.crosschat.auth.data.response.ValidateResponse;

import java.io.Reader;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class ValidateEndpoint implements Endpoint {
    private ValidatePayload payload;

    public ValidateEndpoint(String accessToken, String clientToken) {
        this.payload = new ValidatePayload(accessToken, clientToken);
    }

    @Override
    public String getUrlSuffix() {
        return "/validate";
    }

    @Override
    public Payload getPayload() {
        return payload;
    }

    @Override
    public Response parseResponse(Reader rawResponse) {
        JsonElement element = new JsonParser().parse(rawResponse);
        return new ValidateResponse().fromJson(element);
    }
}
