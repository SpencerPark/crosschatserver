package io.github.mrblobman.crosschat.auth;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import io.github.mrblobman.crosschat.auth.data.response.ErrorResponse;
import io.github.mrblobman.crosschat.auth.data.response.Response;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created on 06/12/2015.
 */
public class HttpUtils {
    @FunctionalInterface
    protected interface ResponseHandler {
        /**
         * Handles the parsing of a successful response. This will be called if the
         * {@link HttpURLConnection#getResponseCode()} is in the range [200, 300).
         * @param response the response from the connection parsed as a {@link JsonElement}.
         * @return the parsed response.
         * Note: You are not responsible for closing the reader.
         */
        Response handle(Reader response);
    }

    /**
     * A data bean to hold the key-value pair needed for a url query string parameter.
     */
    public static final class QueryParam {
        public String key;
        public String value;

        public QueryParam(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    private static JsonParser parser = new JsonParser();

    /**
     * Post the payload to the connection.
     * @param connection the connection to post to. You must open it.
     * @param payload the payload for the post.
     * @throws IOException if the post could not be completed.
     */
    protected static void post(HttpURLConnection connection, JsonElement payload) throws IOException {
        connection.setDoOutput(true); //We are posting
        connection.setChunkedStreamingMode(0); //Cheapest (memory wise) as the smallest size buffer is used
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

        OutputStreamWriter post = null;
        try {
            post = new OutputStreamWriter(connection.getOutputStream());
            post.write(payload.toString());
        } finally {
            if (post != null) {
                try {
                    post.close();
                } catch (IOException ignored) {}
            }
        }
    }

    /**
     * Handle the input from the given connection.
     * @param connection the connection to read from. You must open it.
     * @param handler the handler to handle a successful return.
     * @return the {@link Response} from the {@code handler} or an {@link ErrorResponse}
     * if the read was successful but the response from the connection was an error.
     * @throws IOException if the read was not successful.
     */
    protected static Response handleResponse(HttpURLConnection connection, ResponseHandler handler) throws IOException {
        Reader responseReader = null;
        try {
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_NO_CONTENT) {
                responseReader = new StringReader("{}"); //Feed it an empty object
            } else if (responseCode != HttpURLConnection.HTTP_OK) {
                responseReader = new InputStreamReader(connection.getErrorStream());
            } else {
                responseReader = new InputStreamReader(connection.getInputStream());
            }
            if (responseCode < 200 || responseCode > 299) {
                //An error has occurred
                return new ErrorResponse().fromJson(parser.parse(responseReader));
            }
            return handler.handle(responseReader);
        } finally {
            if (responseReader != null) {
                try {
                    responseReader.close();
                } catch (IOException ignored) {}
            }
        }
    }

    /**
     * Build a get request.
     * @param base the base url (if this url is invalid the method will toss a {@link MalformedURLException})
     * @param parameters the query parameters
     * @return a url representing the request.
     * @throws MalformedURLException if the {@code base} is invalid.
     */
    protected static URL buildGet(String base, QueryParam... parameters) throws MalformedURLException {
        StringBuilder urlString = new StringBuilder(base);
        if (parameters != null && parameters.length > 0) {
            urlString.append("?");
            urlString.append(parameters[0].key).append("=").append(parameters[0].value);
            for (int i = 1; i < parameters.length; i++) {
                urlString.append("&").append(parameters[i].key).append("=").append(parameters[i].value);
            }
        }
        return new URL(urlString.toString());
    }
}
