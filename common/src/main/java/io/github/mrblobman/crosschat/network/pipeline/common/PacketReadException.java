package io.github.mrblobman.crosschat.network.pipeline.common;

/**
 * Created by MrBlobman on 15-09-01.
 */
public class PacketReadException extends Exception {
    public PacketReadException(String message) {
        super(message);
    }

    public PacketReadException(String message, Throwable cause) {
        super(message, cause);
    }

    public PacketReadException(Throwable cause) {
        super(cause);
    }

    public PacketReadException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
