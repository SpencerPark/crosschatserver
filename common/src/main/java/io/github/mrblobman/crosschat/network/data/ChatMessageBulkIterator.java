package io.github.mrblobman.crosschat.network.data;

import java.util.Iterator;
import java.util.List;

/**
 * Created on 10/10/2015.
 */
public class ChatMessageBulkIterator implements Iterator<ChatMessage> {
    private Iterator<ChatMessage> messageSnapshot;

    public ChatMessageBulkIterator(List<ChatMessage> messageSnapshot) {
        this.messageSnapshot = messageSnapshot.iterator();
    }

    @Override
    public boolean hasNext() {
        return messageSnapshot.hasNext();
    }

    @Override
    public ChatMessage next() {
        return messageSnapshot.next();
    }

    @Override
    public void remove() {
        messageSnapshot.remove();
    }
}
