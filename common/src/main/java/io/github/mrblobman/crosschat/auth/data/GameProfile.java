package io.github.mrblobman.crosschat.auth.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class GameProfile implements Data {
    private UUID id;
    private String name;
    //Will only appear if true
    private boolean legacy;
    private List<PlayerProperty> properties;

    public GameProfile(UUID id, String name, boolean legacy) {
        this.id = id;
        this.name = name;
        this.legacy = legacy;
        this.properties = new ArrayList<>();
    }

    public GameProfile(UUID id, String name, boolean legacy, List<PlayerProperty> playerProperties) {
        this.id = id;
        this.name = name;
        this.legacy = legacy;
        this.properties = playerProperties;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isLegacy() {
        return legacy;
    }

    public List<PlayerProperty> getProperties() {
        return this.properties;
    }

    @Override
    public JsonElement toJson() {
        JsonObject object = new JsonObject();
        object.addProperty("id", id.toString().replace("-", ""));
        object.addProperty("name", name);
        if (legacy) object.addProperty("legacy", true);
        if (!properties.isEmpty()) {
            JsonArray props = new JsonArray();
            for (PlayerProperty prop : this.properties) props.add(prop.toJson());
            object.add("properties", props);
        }
        return object;
    }

    public static GameProfile fromJson(JsonElement element) throws JsonParseException {
        JsonObject object = element.getAsJsonObject();
        if (!object.has("id")) throw new JsonParseException("Missing required field 'id'");
        if (!object.has("name")) throw new JsonParseException("Missing required field 'name'");
        if (object.has("properties")) {
            JsonArray properties = object.get("properties").getAsJsonArray();
            List<PlayerProperty> props = new ArrayList<>();
            for (JsonElement property : properties)  props.add(PlayerProperty.fromJson(property));
            return new GameProfile(UUID.fromString(object.get("id").getAsString().replaceFirst("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5")),
                    object.get("name").getAsString(),
                    object.has("legacy"),
                    props
            );
        } else {
            return new GameProfile(UUID.fromString(object.get("id").getAsString().replaceFirst("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5")),
                    object.get("name").getAsString(),
                    object.has("legacy")
            );
        }
    }

    @Override
    public String toString() {
        return toJson().toString();
    }
}
