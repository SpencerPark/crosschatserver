package io.github.mrblobman.crosschat.auth.data.payload;

import com.google.gson.JsonElement;

/**
 * Created by MrBlobman on 15-09-03.
 */
@FunctionalInterface
public interface Payload {

    JsonElement getAsJson();
}
