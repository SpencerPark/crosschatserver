package io.github.mrblobman.crosschat.auth.data.response;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * Created by MrBlobman on 15-09-03.
 */
@FunctionalInterface
public interface Response {

    Response fromJson(JsonElement element) throws JsonParseException;
}
