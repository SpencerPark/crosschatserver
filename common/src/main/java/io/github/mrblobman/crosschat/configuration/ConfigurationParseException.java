package io.github.mrblobman.crosschat.configuration;

/**
 * Created by MrBlobman on 15-09-06.
 */
public class ConfigurationParseException extends RuntimeException {
    public ConfigurationParseException(String message) {
        super(message);
    }

    public ConfigurationParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConfigurationParseException(Throwable cause) {
        super(cause);
    }

    public ConfigurationParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
