package io.github.mrblobman.crosschat.network.data.parser;// Generated from ChatMessageParser.g4 by ANTLR 4.5.1

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ChatMessageParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COLOR_CODE=1, FORMATTING_CODE=2, L_BRACKET=3, R_BRACKET=4, L_BRACE=5, 
		R_BRACE=6, ATTRIBUTE=7, ESC_CHAR=8, WS=9, CHAR=10, STRING=11;
	public static final int
		RULE_message = 0, RULE_messagePart = 1, RULE_plainMessage = 2, RULE_blankMessage = 3, 
		RULE_decoratedMessage = 4;
	public static final String[] ruleNames = {
		"message", "messagePart", "plainMessage", "blankMessage", "decoratedMessage"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'&'", null, "'['", "']'", "'{'", "'}'", null, "'\\'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "COLOR_CODE", "FORMATTING_CODE", "L_BRACKET", "R_BRACKET", "L_BRACE", 
		"R_BRACE", "ATTRIBUTE", "ESC_CHAR", "WS", "CHAR", "STRING"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "ChatMessageParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ChatMessageParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class MessageContext extends ParserRuleContext {
		public List<MessagePartContext> messagePart() {
			return getRuleContexts(MessagePartContext.class);
		}
		public MessagePartContext messagePart(int i) {
			return getRuleContext(MessagePartContext.class,i);
		}
		public MessageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_message; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChatMessageParserListener) ((ChatMessageParserListener)listener).enterMessage(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChatMessageParserListener) ((ChatMessageParserListener)listener).exitMessage(this);
		}
	}

	public final MessageContext message() throws RecognitionException {
		MessageContext _localctx = new MessageContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_message);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(11); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(10);
				messagePart();
				}
				}
				setState(13); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << COLOR_CODE) | (1L << L_BRACKET) | (1L << L_BRACE) | (1L << WS) | (1L << STRING))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MessagePartContext extends ParserRuleContext {
		public PlainMessageContext plainMessage() {
			return getRuleContext(PlainMessageContext.class,0);
		}
		public BlankMessageContext blankMessage() {
			return getRuleContext(BlankMessageContext.class,0);
		}
		public DecoratedMessageContext decoratedMessage() {
			return getRuleContext(DecoratedMessageContext.class,0);
		}
		public MessagePartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_messagePart; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChatMessageParserListener) ((ChatMessageParserListener)listener).enterMessagePart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChatMessageParserListener) ((ChatMessageParserListener)listener).exitMessagePart(this);
		}
	}

	public final MessagePartContext messagePart() throws RecognitionException {
		MessagePartContext _localctx = new MessagePartContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_messagePart);
		try {
			setState(18);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(15);
				plainMessage();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(16);
				blankMessage();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(17);
				decoratedMessage();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PlainMessageContext extends ParserRuleContext {
		public List<TerminalNode> STRING() { return getTokens(ChatMessageParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(ChatMessageParser.STRING, i);
		}
		public PlainMessageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plainMessage; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChatMessageParserListener) ((ChatMessageParserListener)listener).enterPlainMessage(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChatMessageParserListener) ((ChatMessageParserListener)listener).exitPlainMessage(this);
		}
	}

	public final PlainMessageContext plainMessage() throws RecognitionException {
		PlainMessageContext _localctx = new PlainMessageContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_plainMessage);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(21); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(20);
					match(STRING);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(23); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			} while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlankMessageContext extends ParserRuleContext {
		public List<TerminalNode> WS() { return getTokens(ChatMessageParser.WS); }
		public TerminalNode WS(int i) {
			return getToken(ChatMessageParser.WS, i);
		}
		public BlankMessageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blankMessage; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChatMessageParserListener) ((ChatMessageParserListener)listener).enterBlankMessage(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChatMessageParserListener) ((ChatMessageParserListener)listener).exitBlankMessage(this);
		}
	}

	public final BlankMessageContext blankMessage() throws RecognitionException {
		BlankMessageContext _localctx = new BlankMessageContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_blankMessage);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(26); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(25);
					match(WS);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(28); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			} while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DecoratedMessageContext extends ParserRuleContext {
		public Token FORMATTING_CODE;
		public List<Token> codes = new ArrayList<Token>();
		public Token ATTRIBUTE;
		public List<Token> attrs = new ArrayList<Token>();
		public TerminalNode L_BRACE() { return getToken(ChatMessageParser.L_BRACE, 0); }
		public TerminalNode R_BRACE() { return getToken(ChatMessageParser.R_BRACE, 0); }
		public List<TerminalNode> COLOR_CODE() { return getTokens(ChatMessageParser.COLOR_CODE); }
		public TerminalNode COLOR_CODE(int i) {
			return getToken(ChatMessageParser.COLOR_CODE, i);
		}
		public List<TerminalNode> WS() { return getTokens(ChatMessageParser.WS); }
		public TerminalNode WS(int i) {
			return getToken(ChatMessageParser.WS, i);
		}
		public List<TerminalNode> L_BRACKET() { return getTokens(ChatMessageParser.L_BRACKET); }
		public TerminalNode L_BRACKET(int i) {
			return getToken(ChatMessageParser.L_BRACKET, i);
		}
		public List<TerminalNode> R_BRACKET() { return getTokens(ChatMessageParser.R_BRACKET); }
		public TerminalNode R_BRACKET(int i) {
			return getToken(ChatMessageParser.R_BRACKET, i);
		}
		public List<MessagePartContext> messagePart() {
			return getRuleContexts(MessagePartContext.class);
		}
		public MessagePartContext messagePart(int i) {
			return getRuleContext(MessagePartContext.class,i);
		}
		public List<TerminalNode> FORMATTING_CODE() { return getTokens(ChatMessageParser.FORMATTING_CODE); }
		public TerminalNode FORMATTING_CODE(int i) {
			return getToken(ChatMessageParser.FORMATTING_CODE, i);
		}
		public List<TerminalNode> ATTRIBUTE() { return getTokens(ChatMessageParser.ATTRIBUTE); }
		public TerminalNode ATTRIBUTE(int i) {
			return getToken(ChatMessageParser.ATTRIBUTE, i);
		}
		public DecoratedMessageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decoratedMessage; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ChatMessageParserListener) ((ChatMessageParserListener)listener).enterDecoratedMessage(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ChatMessageParserListener) ((ChatMessageParserListener)listener).exitDecoratedMessage(this);
		}
	}

	public final DecoratedMessageContext decoratedMessage() throws RecognitionException {
		DecoratedMessageContext _localctx = new DecoratedMessageContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_decoratedMessage);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(34);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COLOR_CODE) {
				{
				{
				setState(30);
				match(COLOR_CODE);
				setState(31);
				((DecoratedMessageContext)_localctx).FORMATTING_CODE = match(FORMATTING_CODE);
				((DecoratedMessageContext)_localctx).codes.add(((DecoratedMessageContext)_localctx).FORMATTING_CODE);
				}
				}
				setState(36);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(38);
			_la = _input.LA(1);
			if (_la==WS) {
				{
				setState(37);
				match(WS);
				}
			}

			setState(54);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==L_BRACKET) {
				{
				{
				setState(40);
				match(L_BRACKET);
				setState(42);
				_la = _input.LA(1);
				if (_la==WS) {
					{
					setState(41);
					match(WS);
					}
				}

				setState(44);
				((DecoratedMessageContext)_localctx).ATTRIBUTE = match(ATTRIBUTE);
				((DecoratedMessageContext)_localctx).attrs.add(((DecoratedMessageContext)_localctx).ATTRIBUTE);
				setState(46);
				_la = _input.LA(1);
				if (_la==WS) {
					{
					setState(45);
					match(WS);
					}
				}

				setState(48);
				match(R_BRACKET);
				setState(50);
				_la = _input.LA(1);
				if (_la==WS) {
					{
					setState(49);
					match(WS);
					}
				}

				}
				}
				setState(56);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(57);
			match(L_BRACE);
			setState(59); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(58);
				messagePart();
				}
				}
				setState(61); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << COLOR_CODE) | (1L << L_BRACKET) | (1L << L_BRACE) | (1L << WS) | (1L << STRING))) != 0) );
			setState(63);
			match(R_BRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\rD\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\6\2\16\n\2\r\2\16\2\17\3\3\3\3\3\3\5\3"+
		"\25\n\3\3\4\6\4\30\n\4\r\4\16\4\31\3\5\6\5\35\n\5\r\5\16\5\36\3\6\3\6"+
		"\7\6#\n\6\f\6\16\6&\13\6\3\6\5\6)\n\6\3\6\3\6\5\6-\n\6\3\6\3\6\5\6\61"+
		"\n\6\3\6\3\6\5\6\65\n\6\7\6\67\n\6\f\6\16\6:\13\6\3\6\3\6\6\6>\n\6\r\6"+
		"\16\6?\3\6\3\6\3\6\2\2\7\2\4\6\b\n\2\2J\2\r\3\2\2\2\4\24\3\2\2\2\6\27"+
		"\3\2\2\2\b\34\3\2\2\2\n$\3\2\2\2\f\16\5\4\3\2\r\f\3\2\2\2\16\17\3\2\2"+
		"\2\17\r\3\2\2\2\17\20\3\2\2\2\20\3\3\2\2\2\21\25\5\6\4\2\22\25\5\b\5\2"+
		"\23\25\5\n\6\2\24\21\3\2\2\2\24\22\3\2\2\2\24\23\3\2\2\2\25\5\3\2\2\2"+
		"\26\30\7\r\2\2\27\26\3\2\2\2\30\31\3\2\2\2\31\27\3\2\2\2\31\32\3\2\2\2"+
		"\32\7\3\2\2\2\33\35\7\13\2\2\34\33\3\2\2\2\35\36\3\2\2\2\36\34\3\2\2\2"+
		"\36\37\3\2\2\2\37\t\3\2\2\2 !\7\3\2\2!#\7\4\2\2\" \3\2\2\2#&\3\2\2\2$"+
		"\"\3\2\2\2$%\3\2\2\2%(\3\2\2\2&$\3\2\2\2\')\7\13\2\2(\'\3\2\2\2()\3\2"+
		"\2\2)8\3\2\2\2*,\7\5\2\2+-\7\13\2\2,+\3\2\2\2,-\3\2\2\2-.\3\2\2\2.\60"+
		"\7\t\2\2/\61\7\13\2\2\60/\3\2\2\2\60\61\3\2\2\2\61\62\3\2\2\2\62\64\7"+
		"\6\2\2\63\65\7\13\2\2\64\63\3\2\2\2\64\65\3\2\2\2\65\67\3\2\2\2\66*\3"+
		"\2\2\2\67:\3\2\2\28\66\3\2\2\289\3\2\2\29;\3\2\2\2:8\3\2\2\2;=\7\7\2\2"+
		"<>\5\4\3\2=<\3\2\2\2>?\3\2\2\2?=\3\2\2\2?@\3\2\2\2@A\3\2\2\2AB\7\b\2\2"+
		"B\13\3\2\2\2\r\17\24\31\36$(,\60\648?";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}