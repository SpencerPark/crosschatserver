package io.github.mrblobman.crosschat.configuration;

import com.google.gson.*;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by MrBlobman on 15-09-06.
 * This is essentially a wrapped {@link Gson} parser.
 */
public class ConfigParser {
    private static final Pattern COMMENT_PATTERN = Pattern.compile("[ \t]*(//|#|%)[^\n]*\n");
    private Gson gson;
    private GsonBuilder builder;

    public ConfigParser(boolean prettyPrinting) {
        this.builder = new GsonBuilder();
        if (prettyPrinting) this.builder.setPrettyPrinting();
        this.builder.serializeSpecialFloatingPointValues();
        this.builder.serializeNulls();
        this.builder.addDeserializationExclusionStrategy(new ExclusionStrategy() {

            private boolean shouldSkip;

            @Override
            public synchronized boolean shouldSkipField(FieldAttributes attributes) {
                shouldSkip = attributes.getDeclaringClass().getAnnotation(Hide.class) != null ? attributes.getAnnotation(Reveal.class) == null : attributes.getAnnotation(Hide.class) != null;
                if (!shouldSkip && attributes.getDeclaredClass().equals(attributes.getDeclaringClass()))
                    throw new ConfigurationParseException("Circular reference detected. Cannot serialize revealed field " + attributes.getName()
                            + " in " + attributes.getDeclaringClass() + " as it is the same type as the class it is declared inside.");
                return shouldSkip;
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        });
        this.gson = builder.create();
    }

    public void addCustomDeserializer(Class<?> type, JsonDeserializer<?> deserializer) {
        this.builder.registerTypeHierarchyAdapter(type, deserializer);
        //rebuild
        this.gson = this.builder.create();
    }

    public void addCustomSerializer(Class<?> type, JsonSerializer<?> serializer) {
        this.builder.registerTypeHierarchyAdapter(type, serializer);
        //rebuild
        this.gson = this.builder.create();
    }

    public void addCustomInstanceCreator(Class<?> type, InstanceCreator<?> instanceCreator) {
        this.builder.registerTypeHierarchyAdapter(type, instanceCreator);
        //rebuild
        this.gson = this.builder.create();
    }

    public void writeTo(Object data, File file) throws IOException {
        String toWrite = gson.toJson(data);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(toWrite);
        }
    }

    private String prepareRawJson(String rawJson) {
        StringBuffer prepared = new StringBuffer();
        Matcher m = COMMENT_PATTERN.matcher(rawJson);
        while (m.find()) {
            m.appendReplacement(prepared, "");
        }
        m.appendTail(prepared);
        return prepared.toString();
    }

    private Object parse(String json, Class<?> type) {
        return gson.fromJson(prepareRawJson(json), type);
    }

    /**
     * Serialize {@code toCompose} to a writable string.
     * @param toCompose the object to serialize
     * @return the composed String
     */
    public String compose(Object toCompose) {
        return gson.toJson(toCompose);
    }

    /**
     * Write the {@link ConfigParser#compose(Object)}ed string to
     * the {@code writeLocation}.
     * @param writeLocation the {@link File} to write to.
     * @param toCompose the object to compose and then write.
     * @throws IOException if a I/O exception occurs during writing
     */
    public void composeAndWrite(File writeLocation, Object toCompose) throws IOException {
        String toWrite = compose(toCompose);
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(writeLocation));
            writer.write(toWrite);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    /**
     * Parse the reader with the given parser as a type T.
     * @param parser the {@link ConfigParser} to parse with.
     * @param reader the reader used to read the input data.
     * @param type the type to parse as
     * @return the object parsed from the reader.
     */
    public static <T> T parse(ConfigParser parser, BufferedReader reader, Class<T> type) {
        StringBuilder stringBuilder = new StringBuilder();
        //We need the \n added back so we can properly handle comments
        try {
            String line = reader.readLine();
            while (line != null) {
                stringBuilder.append(line).append('\n');
                line = reader.readLine();
            }
        } catch (IOException e) {
            return (T) parser.parse("{}", type);
        }
        return (T) parser.parse(stringBuilder.toString(), type);
    }

    /**
     * Parse the reader with the given parser as a type T.
     * @param parser the {@link ConfigParser} to parse with.
     * @param file the file where the data is saved
     * @param type the type to parse as
     * @return the object parsed from the reader.
     * @throws FileNotFoundException if the input file does not exist
     */
    public static <T> T parse(ConfigParser parser, File file, Class<T> type) throws IOException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            return parse(parser, reader, type);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }
}
