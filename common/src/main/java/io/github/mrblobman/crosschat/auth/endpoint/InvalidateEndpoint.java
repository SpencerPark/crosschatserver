package io.github.mrblobman.crosschat.auth.endpoint;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import io.github.mrblobman.crosschat.auth.data.payload.Payload;
import io.github.mrblobman.crosschat.auth.data.response.Response;
import io.github.mrblobman.crosschat.auth.data.payload.InvalidatePayload;
import io.github.mrblobman.crosschat.auth.data.response.InvalidateResponse;

import java.io.Reader;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class InvalidateEndpoint implements Endpoint {
    private InvalidatePayload payload;

    public InvalidateEndpoint(String accessToken, String clientToken) {
        this.payload = new InvalidatePayload(accessToken, clientToken);
    }

    @Override
    public String getUrlSuffix() {
        return "/invalidate";
    }

    @Override
    public Payload getPayload() {
        return payload;
    }

    @Override
    public Response parseResponse(Reader rawResponse) {
        JsonElement element = new JsonParser().parse(rawResponse);
        return new InvalidateResponse().fromJson(element);
    }
}
