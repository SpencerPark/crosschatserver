package io.github.mrblobman.crosschat.configuration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * Created by MrBlobman on 15-09-07.
 */
public class FileConfiguration<DATATYPE> {
    private DATATYPE data;
    private ConfigParser parser;
    private File file;
    private Class<DATATYPE> datatypeClass;

    public FileConfiguration(Class<DATATYPE> type, String defaultData, File saveLocation, ConfigParser parser) {
        this.datatypeClass = type;
        this.file = saveLocation;
        this.parser = parser;
        if (!saveLocation.exists()) {
            InputStream stream = null;
            try {
                stream = FileConfiguration.class.getClassLoader().getResourceAsStream(defaultData);
                Files.copy(stream, saveLocation.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException ignored) {}
                }
            }
        }
    }

    public FileConfiguration(Class<DATATYPE> type, File saveLocation, ConfigParser parser) {
        this.datatypeClass = type;
        this.parser = parser;
        this.file = saveLocation;
    }

    /**
     * Get the data object for this configuration.
     * @return the data
     * @exception IOException if the data was not loaded and {@link FileConfiguration#load()}
     * throws an exception
     */
    public DATATYPE getData() throws IOException {
        if (this.data == null) load();
        return this.data;
    }

    /**
     * Set the data object for this configuration.
     * @param data the data
     */
    public void setData(DATATYPE data) {
        this.data = data;
    }

    public void save() throws IOException {
        parser.composeAndWrite(file, data);
    }

    public void load() throws IOException {
        this.data = ConfigParser.parse(parser, file, datatypeClass);
    }
}
