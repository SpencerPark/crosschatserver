package io.github.mrblobman.crosschat.network.io;

import io.netty.buffer.ByteBuf;

import java.nio.charset.StandardCharsets;

public class Buffer {
    private final ByteBuf buffer;

    public Buffer(ByteBuf buffer) {
        this.buffer = buffer;
    }

    public ByteBuf getWrappedBuffer() {
        return buffer;
    }

    public int readVarInt() throws BufferReadException {
        int result = 0;
        int count = 0;
        while (true) {
            byte in = readByte();
            result |= (in & 0x7f) << (count++ * 7);
            if (count > 5) {
                throw new BufferReadException("VarInt byte count > 5");
            }
            if ((in & 0x80) != 0x80) {
                break;
            }
        }
        return result;
    }

    public void writeVarInt(int value) {
        while (true) {
            byte part = (byte) (value & 0x7f);
            value >>>= 7;
            if (value != 0) {
                part |= 0x80;
            }
            write(part);
            if (value == 0) {
                break;
            }
        }
    }

    public long readVarLong() throws BufferReadException {
        long result = 0;
        int count = 0;
        while (true) {
            byte in = readByte();
            result |= (in & 0x7f) << (count++ * 7);
            if (count > 10) {
                throw new BufferReadException("VarLong byte count > 10");
            }
            if ((in & 0x80) != 0x80) {
                break;
            }
        }
        return result;
    }

    public void writeVarLong(long value) {
        while (true) {
            byte part = (byte) (value & 0x7f);
            value >>>= 7;
            if (value != 0) {
                part |= 0x80;
            }
            write(part);
            if (value == 0) {
                break;
            }
        }
    }

    public byte[] readByteArray() throws BufferReadException {
        int size = readVarInt();
        byte[] result = new byte[size];
        this.buffer.readBytes(result);
        return result;
    }

    public void readBytes(ByteBuf dst, int length) {
        this.buffer.readBytes(dst, length);
    }

    public void readBytes(ByteBuf dst) {
        this.buffer.readBytes(dst);
    }

    public void readBytes(byte[] dst) {
        this.buffer.readBytes(dst);
    }

    public void write(byte[] value) {
        writeVarInt(value.length);
        this.buffer.writeBytes(value);
    }

    public String readString() throws BufferReadException {
        return new String(readByteArray(), StandardCharsets.UTF_8);
    }

    public void write(String value) {
        write(value.getBytes(StandardCharsets.UTF_8));
    }

    public int readableBytes() {
        return this.buffer.readableBytes();
    }

    public int writeableBytes() {
        return this.buffer.writableBytes();
    }

    public void skipBytes(int amt) {
        this.buffer.skipBytes(amt);
    }
    
    public void write(boolean b) {
        this.buffer.writeBoolean(b);
    }

    public boolean readBoolean() {
        return this.buffer.readBoolean();
    }

    public void write(byte b) {
        this.buffer.writeByte(b);
    }

    public byte readByte() {
        return this.buffer.readByte();
    }

    public void write(char c) {
        this.buffer.writeChar(c);
    }

    public char readChar() {
        return this.buffer.readChar();
    }

    public void write(double d) {
        this.buffer.writeDouble(d);
    }

    public double readDouble() {
        return this.buffer.readDouble();
    }

    public void write(float f) {
        this.buffer.writeFloat(f);
    }

    public float readFloat() {
        return this.buffer.readFloat();
    }

    public void write(int i) {
        this.buffer.writeInt(i);
    }

    public int readInt() {
        return this.buffer.readInt();
    }

    public void write(long l) {
        this.buffer.writeLong(l);
    }

    public long readLong() {
        return this.buffer.readLong();
    }

    public void write(short s) {
        this.buffer.writeShort(s);
    }

    public short readShort() {
        return this.buffer.readShort();
    }

    public void copy(ByteBuf bytes) {
        this.buffer.writeBytes(bytes);
    }

    public boolean canReadVarInt() {
        if (this.buffer.readableBytes() > 5) {
            return true;
        }
        //Mark the position to return to
        int readPos = this.buffer.readerIndex();
        byte in;
        do {
            if (this.buffer.readableBytes() < 1) {
                //Reset reader
                this.buffer.readerIndex(readPos);
                return false;
            }
            in = this.buffer.readByte();
        } while ((in & 0x80) != 0);
        //Reset reader
        this.buffer.readerIndex(readPos);
        return true;
    }

    public int readerIndex() {
        return buffer.readerIndex();
    }

    public int writerIndex() {
        return buffer.writerIndex();
    }

    public void setIndex(int readerIndex, int writerIndex) {
        buffer.setIndex(readerIndex, writerIndex);
    }

    public void writerIndex(int writerIndex) {
        buffer.writerIndex(writerIndex);
    }

    public void readerIndex(int readerIndex) {
        buffer.readerIndex(readerIndex);
    }

    public void retain() {
        buffer.retain();
    }
    public void retain(int increment) {
        buffer.retain(increment);
    }
}
