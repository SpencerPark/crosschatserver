package io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.serverbound;

import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.PacketIds;

/**
 * Created by MrBlobman on 15-09-01.
 */
public class LoginStartPacket extends Packet {
    private String name;

    public LoginStartPacket() {}

    public LoginStartPacket(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public byte getId() {
        return PacketIds.ServerBound.LOGIN_START;
    }

    @Override
    public void encode(Buffer buffer) throws Exception {
        buffer.write(this.name);
    }

    @Override
    public void decode(Buffer buffer) throws Exception {
        this.name = buffer.readString();
    }
}
