package io.github.mrblobman.crosschat.network.data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MrBlobman on 15-09-03.
 */
public enum MessageScope {

    /**
     * A global message is a message sent to all recipients on
     * the server.
     */
    GLOBAL,
    /**
     * A channel message is one that everyone in the same
     * channel as the recipient can read.
     */
    CHANNEL,
    /**
     * A private message is a message that only the recipient
     * is sent.
     */
    PRIVATE,
    /**
     * A generic message is never concerned about
     * the sender. It should be used as a default scope
     * unless another scope better applies.
     */
    GENERIC;

    public static Map<Byte, MessageScope> lookup = new HashMap<>();

    static {
        for (MessageScope scope : MessageScope.values()) {
            lookup.put((byte) scope.ordinal(), scope);
        }
    }

    public static MessageScope getById(byte id) {
        return lookup.get(id);
    }
}
