package io.github.mrblobman.crosschat.network.data;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created on 06/12/2015.
 */
public class ConsoleColors {
    private static Map<ChatColor, String> replacements = new EnumMap<>(ChatColor.class);
    static {
        replacements.put(ChatColor.BLACK, "\u001B[0;30m");
        replacements.put( ChatColor.DARK_BLUE, "\u001B[0;34m" );
        replacements.put( ChatColor.DARK_GREEN, "\u001B[0;32m" );
        replacements.put( ChatColor.DARK_AQUA, "\u001B[0;36m" );
        replacements.put( ChatColor.DARK_RED, "\u001B[0;31m" );
        replacements.put( ChatColor.DARK_PURPLE, "\u001B[0;35m" );
        replacements.put( ChatColor.GOLD, "\u001B[0;33m" );
        replacements.put( ChatColor.GRAY, "\u001B[0;37m" );
        replacements.put( ChatColor.DARK_GRAY, "\u001B[1;30m" );
        replacements.put( ChatColor.BLUE, "\u001B[1;34m" );
        replacements.put( ChatColor.GREEN, "\u001B[1;32m" );
        replacements.put( ChatColor.AQUA, "\u001B[1;36m" );
        replacements.put( ChatColor.RED, "\u001B[1;31m" );
        replacements.put( ChatColor.LIGHT_PURPLE, "\u001B[1;35m" );
        replacements.put( ChatColor.YELLOW, "\u001B[1;33m" );
        replacements.put( ChatColor.WHITE, "\u001B[1;37m" );
        replacements.put( ChatColor.OBFUSCATED, "\u001B[5m" );
        replacements.put( ChatColor.BOLD, "\u001B[21m" );
        replacements.put( ChatColor.STRIKETHROUGH, "\u001B[9m" );
        replacements.put( ChatColor.UNDERLINE, "\u001B[4m" );
        replacements.put( ChatColor.ITALIC, "\u001B[3m" );
        replacements.put( ChatColor.RESET, "\u001B[0m" );
    }

    /**
     * Translate the given message into is colored representation with the appropriate
     * ANSI control chars for colored text in console.
     * @param message the message to translate.
     * @return the System.out.print() ready message.
     */
    public static String translate(ChatMessageBulk message) {
        StringBuilder sb = new StringBuilder();
        for (ChatMessage messagePart : message) {
            sb.append(replacements.get(messagePart.getColor()));
            if (messagePart.isBold()) sb.append(replacements.get(ChatColor.BOLD));
            if (messagePart.isItalic()) sb.append(replacements.get(ChatColor.ITALIC));
            if (messagePart.isObfuscated()) sb.append(replacements.get(ChatColor.OBFUSCATED));
            if (messagePart.isStrikethrough()) sb.append(replacements.get(ChatColor.STRIKETHROUGH));
            if (messagePart.isUnderlined()) sb.append(replacements.get(ChatColor.UNDERLINE));
            sb.append(messagePart.getText());
            sb.append(replacements.get(ChatColor.RESET));
        }
        return sb.toString();
    }
}
