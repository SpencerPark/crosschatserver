package io.github.mrblobman.crosschat.network.pipeline.common;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerAdapter;

/**
 * Created by MrBlobman on 15-08-30.
 */
@ChannelHandler.Sharable
public class NullHandler extends ChannelHandlerAdapter {}
