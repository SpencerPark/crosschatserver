package io.github.mrblobman.crosschat.network.pipeline.common;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageCodec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.List;

/**
 * Created by MrBlobman on 15-08-28.
 */
public class EncryptionHandler extends MessageToMessageCodec<ByteBuf, ByteBuf> {
    private final Cipher encryptor;
    private final Cipher decryptor;

    public EncryptionHandler(SecretKey commonSecret) throws GeneralSecurityException {
        //Create the ciphers
        encryptor = Cipher.getInstance("AES/CFB8/NoPadding");
        encryptor.init(Cipher.ENCRYPT_MODE, commonSecret, new IvParameterSpec(commonSecret.getEncoded()));
        decryptor = Cipher.getInstance("AES/CFB8/NoPadding");
        decryptor.init(Cipher.DECRYPT_MODE, commonSecret, new IvParameterSpec(commonSecret.getEncoded()));
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        ByteBuffer encodedMsg = ByteBuffer.allocate(msg.readableBytes());
        encryptor.update(msg.nioBuffer(), encodedMsg);
        encodedMsg.flip();
        out.add(Unpooled.wrappedBuffer(encodedMsg));
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        ByteBuffer decodedMsg = ByteBuffer.allocate(msg.readableBytes());
        decryptor.update(msg.nioBuffer(), decodedMsg);
        decodedMsg.flip();
        out.add(Unpooled.wrappedBuffer(decodedMsg));
    }
}
