package io.github.mrblobman.crosschat.network.packets;

import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.io.Decodable;
import io.github.mrblobman.crosschat.network.io.Encodable;

/**
 * Created by MrBlobman on 15-08-28.
 * All implementing classes <b>MUST</b> have a no args, public constructor
 */
public abstract class Packet implements Encodable, Decodable {

    protected Packet() {}

    /**
     * @return the id of this packet.
     */
    public abstract byte getId();

    public static <T extends Packet> T decode(Buffer buffer, Class<T> type) throws Exception {
        T packet = type.newInstance();
        packet.decode(buffer);
        return packet;
    }
}
