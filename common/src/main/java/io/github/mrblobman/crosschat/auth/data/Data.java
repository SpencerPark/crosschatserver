package io.github.mrblobman.crosschat.auth.data;

import com.google.gson.JsonElement;

/**
 * Created by MrBlobman on 15-09-03.
 * All implementing classes should implement a static method with the signature<br>
 * {@code public static <implementingClassname> fromJson(JsonElement element) throws JsonParseException}
 */
@FunctionalInterface
public interface Data {
    JsonElement toJson();
}
