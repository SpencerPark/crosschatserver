package io.github.mrblobman.crosschat.network.packets.v1_0_R0;


/**
 * Created by MrBlobman on 15-09-01.
 */
public final class PacketIds {
    /**
     * A list of packet ids that the server can receive.
     */
    public static final class ServerBound {
        //ClientState.HANDSHAKE
        public static final byte HANDSHAKE = 0x00;

        //ClientState.LOGIN
        public static final byte LOGIN_START = 0x00;
        public static final byte ENCRYPTION_RESPONSE = 0x01;

        //ClientState.AUTHENTICATED
        public static final byte CHANNEL_MESSAGE = 0x00;
        public static final byte PRIVATE_MESSAGE = 0x01;
        public static final byte COMMAND = 0x02;

        //ClientState.STATUS
    }

    /**
     * A list of packet ids that the client can receive.
     */
    public static final class ClientBound {
        //ClientState.HANDSHAKE

        //ClientState.LOGIN
        public static final byte DISCONNECT = 0x00;
        public static final byte ENCRYPTION_REQUEST = 0x01;
        public static final byte LOGIN_SUCCESS = 0x02;
        public static final byte SET_COMPRESSION = 0x03;

        //ClientState.AUTHENTICATED
        public static final byte MESSAGE = 0x00;
        public static final byte KICKED = 0x01;
        public static final byte POSTSET_COMPRESSION = 0x02;

        //ClientState.STATUS
    }
}
