package io.github.mrblobman.crosschat.network.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by MrBlobman on 15-09-01.
 */
public class ChatMessage {
    private static final Pattern URL_PATTERN = Pattern.compile("^(?:(https?)://)?([-\\w_\\.]{2,}\\.[a-z]{2,4})(/\\S*)?$");

    public static final String CLICK_OPEN_URL = "open_url";
    public static final String CLICK_OPEN_FILE = "open_file";
    public static final String CLICK_RUN_COMMAND = "run_command";
    public static final String CLICK_SUGGEST_COMMAND = "suggest_command";

    private String text;
    private boolean bold = false;
    private boolean italic = false;
    private boolean underlined = false;
    private boolean strikethrough = false;
    private boolean obfuscated = false;
    private ChatColor color = ChatColor.WHITE;
    private JsonElement hoverAction;
    private JsonElement hoverValue;
    private JsonElement clickAction;
    private JsonElement clickValue;
    private String insertionText;

    public ChatMessage(String text) {
        this.text = text;
    }

    public ChatMessage(String text, boolean bold, boolean italic, boolean underlined, boolean strikethrough, boolean obfuscated, ChatColor color, String clickAction, String clickValue, String insertionText, String... tooltip) {
        this.text = text;
        this.bold = bold;
        this.italic = italic;
        this.underlined = underlined;
        this.strikethrough = strikethrough;
        this.obfuscated = obfuscated;
        this.color = color;
        this.setTooltip(tooltip);
        if (clickAction != null && clickValue != null) {
            setClickAction(clickAction, clickValue);
        }
        this.insertionText = insertionText;
    }

    public static ChatMessage fromJson(JsonObject message) {
        if (!message.has("text")) throw new IllegalArgumentException("Invalid message. Missing required 'text' field.");
        ChatMessage msg = new ChatMessage(message.get("text").getAsString());
        if (message.has("bold"))
            msg.bold = message.get("bold").getAsBoolean();
        if (message.has("italic"))
            msg.italic = message.get("italic").getAsBoolean();
        if (message.has("underlined"))
            msg.underlined = message.get("underlined").getAsBoolean();
        if (message.has("strikethrough"))
            msg.strikethrough = message.get("strikethrough").getAsBoolean();
        if (message.has("obfuscated"))
            msg.obfuscated = message.get("obfuscated").getAsBoolean();
        if (message.has("color"))
            msg.color = ChatColor.getByName(message.get("color").getAsString());
        if (message.has("hoverEvent")) {
            JsonObject hoverEvent = message.get("hoverEvent").getAsJsonObject();
            if (!hoverEvent.has("action")) throw new IllegalArgumentException("Invalid hoverEvent. Missing required 'action' field.");
            if (!hoverEvent.has("value")) throw new IllegalArgumentException("Invalid hoverEvent. Missing required 'value' field.");
            msg.hoverAction = hoverEvent.get("action");
            msg.hoverValue = hoverEvent.get("value");
        }
        if (message.has("clickEvent")) {
            JsonObject clickEvent = message.get("clickEvent").getAsJsonObject();
            if (!clickEvent.has("action")) throw new IllegalArgumentException("Invalid clickEvent. Missing required 'action' field.");
            if (!clickEvent.has("value")) throw new IllegalArgumentException("Invalid clickEvent. Missing required 'value' field.");
            msg.clickAction = clickEvent.get("action");
            msg.clickValue = clickEvent.get("value");
        }
        if (message.has("insertion"))
            msg.insertionText = message.get("insertion").getAsString();
        return msg;
    }

    public JsonObject toJson() {
        JsonObject msg = new JsonObject();
        msg.add("text", new JsonPrimitive(text));
        if (bold) msg.add("bold", new JsonPrimitive(true));
        if (italic) msg.add("italic", new JsonPrimitive(true));
        if (underlined) msg.add("underlined", new JsonPrimitive(true));
        if (strikethrough) msg.add("strikethrough", new JsonPrimitive(true));
        if (obfuscated) msg.add("obfuscated", new JsonPrimitive(true));
        msg.add("color", new JsonPrimitive(color.getSerializedName()));
        if (hoverAction != null && hoverValue != null) {
            JsonObject hoverEvent = new JsonObject();
            hoverEvent.add("action", hoverAction);
            hoverEvent.add("value", hoverValue);
            msg.add("hoverEvent", hoverEvent);
        }
        if (clickAction != null && clickValue != null) {
            JsonObject clickEvent = new JsonObject();
            clickEvent.add("action", clickAction);
            clickEvent.add("value", clickValue);
            msg.add("clickEvent", clickEvent);
        }
        if (insertionText != null) msg.add("insertion", new JsonPrimitive(insertionText));
        return msg;
    }

    /**
     * Set the toolip message for this message.
     * This method supports the use of the \n char
     * as well as {@link ChatColor}.
     * @param message the tooltip message
     */
    public void setTooltip(String message) {
        setTooltip(message.split("\n"));
    }

    /**
     * Set the toolip message for this message.
     * This method DOES NOT support the use of the \n char.
     * For using \n see {@link ChatMessage#setTooltip(String)}.
     * Each element in {@code message} is considered a line in
     * the tooltip. Supports {@link ChatColor}.
     * @param message the tooltip message
     */
    public void setTooltip(String... message) {
        if (message.length == 1) {
            hoverAction = new JsonPrimitive("show_text");
            hoverValue = new JsonPrimitive(message[0]);
        } else {
            JsonObject msg = new JsonObject();
            msg.add("id", new JsonPrimitive(0));
            JsonObject display = new JsonObject();
            display.add("name", new JsonPrimitive(message[0]));
            JsonArray lore = new JsonArray();
            for (int i = 1; i < message.length; i++)
                lore.add(new JsonPrimitive(message[i]));
            display.add("lore", lore);
            msg.add("display", display);
            hoverAction = new JsonPrimitive("show_item");
            hoverValue = msg;
        }
    }

    public List<String> getToolTip() {
        List<String> tooltip = new ArrayList<>();
        if (hoverAction.getAsString().equals("show_text")) {
            //Simple 1 liner
            tooltip.add(hoverValue.getAsString());
        } else {
            JsonObject itemData = hoverValue.getAsJsonObject().get("display").getAsJsonObject();
            tooltip.add(itemData.get("name").getAsString());
            for (JsonElement line : itemData.get("lore").getAsJsonArray())
                tooltip.add(line.getAsString());
        }
        return tooltip;
    }

    /**
     * Set the action to take place when the client clicks on the message.
     * Parameter values:
     * <ul>
     *     <li>Action: {@link ChatMessage#CLICK_OPEN_URL}        Value: the url to open</li>
     *     <li>Action: {@link ChatMessage#CLICK_OPEN_FILE}       Value: the file to open</li>
     *     <li>Action: {@link ChatMessage#CLICK_RUN_COMMAND}     Value: the command to execute</li>
     *     <li>Action: {@link ChatMessage#CLICK_SUGGEST_COMMAND} Value: the text to insert into the user's chat box</li>
     * </ul>
     * @param action the type of action to preform
     * @param value the value associated with the action
     */
    public void setClickAction(String action, String value) {
        switch (action) {
            case CLICK_OPEN_URL:
                if (!URL_PATTERN.matcher(value).matches()) throw new IllegalArgumentException("Invalid url.");
            case CLICK_OPEN_FILE:
            case CLICK_RUN_COMMAND:
            case CLICK_SUGGEST_COMMAND:
                this.clickAction = new JsonPrimitive(action);
                this.clickValue = new JsonPrimitive(value);
                break;
            default:
                throw new IllegalArgumentException("Invalid action. Acceptable actions are ["+CLICK_OPEN_URL+","+CLICK_OPEN_FILE+","+CLICK_RUN_COMMAND+","+CLICK_SUGGEST_COMMAND+"]");
        }
    }

    public String getClickAction() {
        return this.clickAction.getAsString();
    }

    public String getClickValue() {
        return this.clickValue.getAsString();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }

    public boolean isItalic() {
        return italic;
    }

    public void setItalic(boolean italic) {
        this.italic = italic;
    }

    public boolean isUnderlined() {
        return underlined;
    }

    public void setUnderlined(boolean underlined) {
        this.underlined = underlined;
    }

    public boolean isStrikethrough() {
        return strikethrough;
    }

    public void setStrikethrough(boolean strikethrough) {
        this.strikethrough = strikethrough;
    }

    public boolean isObfuscated() {
        return obfuscated;
    }

    public void setObfuscated(boolean obfuscated) {
        this.obfuscated = obfuscated;
    }

    public ChatColor getColor() {
        return color;
    }

    public void setColor(ChatColor color) {
        switch (color) {
            case BLACK:
            case DARK_BLUE:
            case DARK_GREEN:
            case DARK_AQUA:
            case DARK_RED:
            case DARK_PURPLE:
            case GOLD:
            case GRAY:
            case DARK_GRAY:
            case BLUE:
            case GREEN:
            case AQUA:
            case RED:
            case LIGHT_PURPLE:
            case YELLOW:
            case WHITE:
                this.color = color;
                return;
            case OBFUSCATED:
                this.obfuscated = true;
                return;
            case BOLD:
                this.bold = true;
                return;
            case STRIKETHROUGH:
                this.strikethrough = true;
                return;
            case UNDERLINE:
                this.underlined = true;
                return;
            case ITALIC:
                this.italic = true;
                return;
            case RESET:
                this.color = ChatColor.WHITE;
                this.obfuscated = false;
                this.bold = false;
                this.strikethrough = false;
                this.underlined = false;
                this.italic = false;
                return;
            default:
                break;
        }
    }

    public String getInsertionText() {
        return this.insertionText;
    }
    /**
     * Set the text to be pasted in the client's text
     * box upon shift clicking it.
     * @param text the text to insert
     */
    public void setInsertionText(String text) {
        this.insertionText = text;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ChatMessage{\n\"");
        sb.append(this.getText()).append("\"");
        sb.append("\n\t[");
        sb.append(this.getColor().getSerializedName());
        sb.append(",");
        if (this.isBold()) sb.append("bold,");
        if (this.isItalic()) sb.append("italic,");
        if (this.isObfuscated()) sb.append("obfuscated,");
        if (this.isStrikethrough()) sb.append("strikethrough,");
        if (this.isUnderlined()) sb.append("underlined,");
        sb.setLength(sb.length()-1);
        sb.append("],");
        if (this.hoverAction != null) {
            sb.append("\n\tOnHover=");
            sb.append(hoverAction.toString());
            sb.append(" -> ");
            sb.append(hoverValue.toString());
            sb.append(",");
        }
        if (this.clickAction != null) {
            sb.append("\n\tOnClick=");
            sb.append(clickAction.toString());
            sb.append(" -> ");
            sb.append(clickValue.toString());
            sb.append(",");
        }
        if (this.insertionText != null) {
            sb.append("\n\tInsertionText=");
            sb.append(insertionText);
            sb.append(",");
        }
        sb.setLength(sb.length()-1);
        sb.append("\n}");
        return sb.toString();
    }
}
