package io.github.mrblobman.crosschat.auth.data;

import com.google.common.io.BaseEncoding;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.security.*;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class PlayerProperty implements Data {
    private final String name;
    private final String value;
    private final String signature;

    public PlayerProperty(String name, String value, String signature) {
        this.name = name;
        this.value = value;
        this.signature = signature;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getSignature() {
        return signature;
    }

    public boolean isSigned() {
        return this.signature != null;
    }

    public boolean isSignatureValid(final PublicKey publicKey) {
        try {
            final Signature signature = Signature.getInstance("SHA1withRSA");
            signature.initVerify(publicKey);
            signature.update(this.value.getBytes());
            return signature.verify(BaseEncoding.base64().decode(this.signature));
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public JsonElement toJson() {
        JsonObject object = new JsonObject();
        object.addProperty("name", name);
        object.addProperty("value", value);
        object.addProperty("signature", signature);
        return object;
    }

    public static PlayerProperty fromJson(JsonElement element) throws JsonParseException {
        JsonObject object = element.getAsJsonObject();
        if (!object.has("name")) throw new JsonParseException("Missing required field 'name'");
        if (!object.has("value")) throw new JsonParseException("Missing required field 'value'");
        if (!object.has("signature")) throw new JsonParseException("Missing required field 'signature'");
        return new PlayerProperty(
                object.get("name").getAsString(),
                object.get("value").getAsString(),
                object.get("signature").getAsString()
        );
    }
}
