package io.github.mrblobman.crosschat.network.io;

/**
 * Created by MrBlobman on 15-09-01.
 */
public interface Decodable {

    /**
     * Decode {@code buffer} into this message.
     * @param buffer the {@link Buffer} containing the data to decode
     * @throws Exception if something goes wrong while decoding
     */
    void decode(Buffer buffer) throws Exception;
}
