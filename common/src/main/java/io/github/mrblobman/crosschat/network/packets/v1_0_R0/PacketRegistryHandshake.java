package io.github.mrblobman.crosschat.network.packets.v1_0_R0;

import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.PacketRegistry;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.handshake.serverbound.HandshakePacket;

/**
 * Created by MrBlobman on 15-08-31.
 */
public class PacketRegistryHandshake implements PacketRegistry {
    @SuppressWarnings("unchecked")
    private final Class<? extends Packet>[] clientBoundPackets = new Class[] {
    };

    @SuppressWarnings("unchecked")
    private final Class<? extends Packet>[] serverBoundPackets = new Class[] {
            HandshakePacket.class
    };

    @Override
    public Class<? extends Packet> getPacketById(int id, boolean serverBound) {
        if (serverBound) {
            if (id < 0 || id > serverBoundPackets.length) return null;
            return serverBoundPackets[id];
        } else {
            if (id < 0 || id > clientBoundPackets.length) return null;
            return clientBoundPackets[id];
        }
    }
}
