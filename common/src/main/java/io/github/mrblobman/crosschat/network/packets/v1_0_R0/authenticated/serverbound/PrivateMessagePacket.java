package io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.serverbound;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.PacketIds;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class PrivateMessagePacket extends Packet {
    private String target;
    private ChatMessageBulk message;

    public PrivateMessagePacket() {}

    public PrivateMessagePacket(String target, ChatMessageBulk message) {
        this.target = target;
        this.message = message;
    }

    public ChatMessageBulk getMessage() {
        return message;
    }

    public String getTarget() {
        return this.target;
    }

    @Override
    public byte getId() {
        return PacketIds.ServerBound.PRIVATE_MESSAGE;
    }

    @Override
    public void decode(Buffer buffer) throws Exception {
        this.target = buffer.readString();
        this.message = ChatMessageBulk.read(buffer);
    }

    @Override
    public void encode(Buffer buffer) throws Exception {
        buffer.write(this.target);
        this.message.encode(buffer);
    }
}
