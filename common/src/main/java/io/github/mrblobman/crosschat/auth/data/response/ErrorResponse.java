package io.github.mrblobman.crosschat.auth.data.response;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import io.github.mrblobman.crosschat.auth.AuthException;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class ErrorResponse implements Response {
    private boolean isFilled = false;

    private AuthException exception;

    public ErrorResponse() {}

    public AuthException getException() {
        if (!isFilled) throw new IllegalStateException("This response has not read any data yet. Response#fromJson(JsonElement)");
        return this.exception;
    }

    @Override
    public Response fromJson(JsonElement element) throws JsonParseException {
        this.isFilled = false;
        this.exception = AuthException.getFromResponse(element.getAsJsonObject());
        this.isFilled = true;
        return this;
    }
}
