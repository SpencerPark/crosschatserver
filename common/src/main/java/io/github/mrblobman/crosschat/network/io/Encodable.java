package io.github.mrblobman.crosschat.network.io;

/**
 * Created by MrBlobman on 15-09-01.
 */
public interface Encodable {
    /**
     * Encode this message.
     * @param buffer the {@link Buffer} in which to store the encoded message
     * @throws Exception if something goes wrong while encoding.
     */
    void encode(Buffer buffer) throws Exception;
}
