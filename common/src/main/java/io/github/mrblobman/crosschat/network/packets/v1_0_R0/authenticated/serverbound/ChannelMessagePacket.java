package io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.serverbound;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.PacketIds;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class ChannelMessagePacket extends Packet {
    private ChatMessageBulk message;

    public ChannelMessagePacket() {}

    public ChannelMessagePacket(ChatMessageBulk message) {
        this.message = message;
    }

    public ChatMessageBulk getMessage() {
        return message;
    }

    @Override
    public byte getId() {
        return PacketIds.ServerBound.CHANNEL_MESSAGE;
    }

    @Override
    public void decode(Buffer buffer) throws Exception {
        this.message = ChatMessageBulk.read(buffer);
    }

    @Override
    public void encode(Buffer buffer) throws Exception {
        this.message.encode(buffer);
    }
}
