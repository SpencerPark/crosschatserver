package io.github.mrblobman.crosschat.auth.data.payload;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class SignoutPayload implements Payload {
    private String username;
    private String password;

    public SignoutPayload(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public JsonElement getAsJson() {
        JsonObject object = new JsonObject();
        object.addProperty("username", username);
        object.addProperty("password", password);
        return object;
    }
}
