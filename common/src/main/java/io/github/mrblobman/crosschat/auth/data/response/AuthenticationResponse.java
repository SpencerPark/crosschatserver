package io.github.mrblobman.crosschat.auth.data.response;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import io.github.mrblobman.crosschat.auth.data.GameProfile;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class AuthenticationResponse implements Response {
    boolean isFilled = false;

    private String accessToken;
    private String clientToken;
    //Will be empty if log in was successful but user has no minecraft license
    private GameProfile[] availableProfiles;
    //Will be null if log in was successful but user has no minecraft license
    private GameProfile selectedProfile;

    public AuthenticationResponse() { }

    public String getAccessToken() {
        if (!isFilled) throw new IllegalStateException("This response has not read any data yet. Response#fromJson(JsonElement)");
        return accessToken;
    }

    public String getClientToken() {
        if (!isFilled) throw new IllegalStateException("This response has not read any data yet. Response#fromJson(JsonElement)");
        return clientToken;
    }

    public GameProfile[] getAvailableProfiles() {
        if (!isFilled) throw new IllegalStateException("This response has not read any data yet. Response#fromJson(JsonElement)");
        return availableProfiles;
    }

    public GameProfile getSelectedProfile() {
        if (!isFilled) throw new IllegalStateException("This response has not read any data yet. Response#fromJson(JsonElement)");
        return selectedProfile;
    }

    @Override
    public Response fromJson(JsonElement element) throws JsonParseException {
        this.isFilled = false;
        JsonObject object = element.getAsJsonObject();
        if (!object.has("accessToken")) throw new JsonParseException("Missing required field 'accessToken'");
        if (!object.has("clientToken")) throw new JsonParseException("Missing required field 'clientToken'");
        if (!object.has("availableProfiles")) throw new JsonParseException("Missing required field 'availableProfiles'");
        this.accessToken = object.get("accessToken").getAsString();
        this.clientToken = object.get("clientToken").getAsString();
        JsonArray availableProfiles = object.get("availableProfiles").getAsJsonArray();
        this.availableProfiles = new GameProfile[availableProfiles.size()];
        int i = 0;
        for (JsonElement profile : availableProfiles) this.availableProfiles[i++] = GameProfile.fromJson(profile);
        if (object.has("selectedProfile")) this.selectedProfile = GameProfile.fromJson(object.get("selectedProfile"));
        this.isFilled = true;
        return this;
    }
}
