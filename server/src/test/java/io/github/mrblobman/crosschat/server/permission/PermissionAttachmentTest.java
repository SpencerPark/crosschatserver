package io.github.mrblobman.crosschat.server.permission;

import com.google.gson.JsonObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created on 14/12/2015.
 */
public class PermissionAttachmentTest {
    private PermissionGroup testGroupA;
    private PermissionGroup testGroupB;

    private PermissionAttachment attachment;

    @Before
    public void setUp() throws Exception {
        PermissionStore permsForA = new PermissionStore();
        PermissionStore permsForB = new PermissionStore();
        permsForA.addPermission("test.A.perm1");
        permsForA.addPermission("test.A.perm2");
        permsForA.addPermission("test.A.perm1.subperm1");
        permsForA.addPermission("test.A.perm1.subperm2");
        permsForB.addPermission("test.B.perm1");
        permsForB.addPermission("test.B.perm2");
        permsForB.addPermission("test.B.perm1.subperm1");
        permsForB.addPermission("test.B.perm1.subperm2");
        testGroupA = PermissionGroup.createGroup("GroupA", permsForA);
        testGroupB = PermissionGroup.createGroup("GroupB", permsForB);
        attachment = new PermissionAttachment();
    }

    @After
    public void tearDown() throws Exception {
        testGroupA = null;
        testGroupB = null;
        attachment = null;
    }

    @Test
    public void testGivePermission() throws Exception {
        assertFalse("attachment had the permission give.perm.test before it was added", attachment.hasPermission("give.perm.test"));
        attachment.givePermission("give.perm.test");
        assertTrue("attachment did not retain the added permission give.perm.test", attachment.hasPermission("give.perm.test"));
    }

    @Test
    public void testWildCard() throws Exception {
        attachment.givePermission("test.*");
        assertTrue("attachment has test.* but attachment.hasPermission(\"test.*\") returns false", attachment.hasPermission("test.*"));
        assertTrue("attachment has test.* but attachment.hasPermission(\"test.sample\") returns false", attachment.hasPermission("test.sample"));
        assertTrue("attachment has test.* but attachment.hasPermission(\"test.sample.deeper\") returns false", attachment.hasPermission("test.sample.deeper"));
        assertFalse("attachment has test.* but attachment.hasPermission(\"testing\") returns true", attachment.hasPermission("testing"));
        assertFalse("attachment has test.* but attachment.hasPermission(\"testing.sample\") returns true", attachment.hasPermission("testing.sample"));
    }

    @Test
    public void testRootWildCard() throws Exception {
        attachment.givePermission("*");
        assertTrue("attachment has * but attachment.hasPermission(\"test.*\") returns false", attachment.hasPermission("test.*"));
        assertTrue("attachment has * but attachment.hasPermission(\"test\") returns false", attachment.hasPermission("test"));
        assertTrue("attachment has * but attachment.hasPermission(\"test.sample\") returns false", attachment.hasPermission("test.sample"));
    }

    @Test
    public void testBlankPermission() throws  Exception {
        assertTrue("attachment.hasPermission(\"\") returns false", attachment.hasPermission(""));
    }

    @Test
    public void serializePerms() throws Exception {
        attachment.giveGroup(testGroupA);
        attachment.givePermission("test.permission");
        JsonObject serialized = attachment.serialize();
        PermissionAttachment deserialized = new PermissionAttachment(serialized);
        assertTrue("attachment did not match it previous state when serialized and then deserialized", deserialized.equals(attachment));
    }

    @Test
    public void testGroupPermissions() throws Exception {
        attachment.giveGroup(testGroupA);
        assertTrue("attachment has group that has \"test.A.perm1\" but attachment.hasPermission(\"test.A.perm1\") returns false", attachment.hasPermission("test.A.perm1"));
        attachment.giveGroup(testGroupB);
        assertTrue("attachment has group that has \"test.B.perm1\" but attachment.hasPermission(\"test.B.perm1\") returns false", attachment.hasPermission("test.B.perm1"));
        attachment.givePermission("test.A.perm1");
        attachment.removePermission("test.A.perm1");
        assertTrue("attachment modifies group permissions", attachment.hasPermission("test.A.perm1"));
    }

    @Test
    public void testRemoveGroup() throws Exception {
        attachment.giveGroup(testGroupA);
        attachment.giveGroup(testGroupB);
        assertTrue("attachment has group that has \"test.A.perm1\" but attachment.hasPermission(\"test.A.perm1\") returns false", attachment.hasPermission("test.A.perm1"));
        assertTrue("attachment has group that has \"test.B.perm1\" but attachment.hasPermission(\"test.B.perm1\") returns false", attachment.hasPermission("test.B.perm1"));
        attachment.removeGroup(testGroupA);
        assertFalse("GroupA was not removed", attachment.isInGroup("GroupA"));
        assertTrue("Reference group checking does not notice group", attachment.isInGroup(testGroupB));
        attachment.removeGroup("GroupB");
        assertFalse("GroupB was not removed", attachment.isInGroup(testGroupB));
    }

    @Test
    public void testCaseInsensitivity() throws Exception {
        attachment.givePermission("Sample.PERM");
        assertTrue("permission checking is not case insensitive", attachment.hasPermission("samPle.PeRM"));
    }

    @Test
    public void testRemoveSinglePerm() throws Exception {
        attachment.givePermission("test.perm1");
        attachment.givePermission("test.perm2");
        attachment.givePermission("test.perm2.sub1");
        attachment.givePermission("test.perm2.sub2");

        attachment.removePermission("test.perm1");
        assertFalse("test.perm1 was not removed", attachment.hasPermission("test.perm1"));
        assertTrue("test.perm2 was removed when removing test.perm1", attachment.hasPermission("test.perm2"));

        attachment.removePermission("test.perm2");
        assertFalse("test.perm2.sub1 was not removed when test.perm2 was removed", attachment.hasPermission("test.perm2.sub1"));
        assertFalse("test.perm2.sub2 was not removed when test.perm2 was removed", attachment.hasPermission("test.perm2.sub2"));
        assertFalse("test.perm2 was not removed when test.perm2 was removed", attachment.hasPermission("test.perm2"));

        //Test top level too
        attachment.givePermission("test.perm1");
        attachment.givePermission("test.perm2");
        attachment.removePermission("test");
        assertFalse("test.perm1 was not removed when test was removed", attachment.hasPermission("test.perm1"));
        assertFalse("test.perm2 was not removed when test was removed", attachment.hasPermission("test.perm2"));
    }

    @Test
    public void testRemoveWildcard() throws Exception {
        attachment.givePermission("test.perm1");
        attachment.givePermission("test.perm1.sub1");
        attachment.givePermission("test.perm1.sub2");

        attachment.removePermission("test.perm1.*");
        assertFalse("test.perm1.sub1 was not removed when test.perm1.* was removed", attachment.hasPermission("test.perm1.sub1"));
        assertFalse("test.perm1.sub2 was not removed when test.perm1.* was removed", attachment.hasPermission("test.perm1.sub2"));

        attachment.removePermission("*");
        assertFalse("test.perm1 was not removed when * was removed", attachment.hasPermission("test.perm1"));
    }
}