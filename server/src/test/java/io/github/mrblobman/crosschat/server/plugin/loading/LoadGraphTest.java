package io.github.mrblobman.crosschat.server.plugin.loading;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created on 18/12/2015.
 */
public class LoadGraphTest {
    private LoadGraph loadGraph;

    @Before
    public void setUp() {
        loadGraph = new LoadGraph();
    }

    @After
    public void tearDown() {
        loadGraph = null;
    }

    @Test(expected = CircularLoadDependencyException.class)
    public void testCatchesCircularDependency() throws Exception {
        LoadConstraints pluginA = new LoadConstraints("PluginA", LoadPrecedenceLevel.PLUGIN);
        LoadConstraints pluginB = new LoadConstraints("PluginB", LoadPrecedenceLevel.PLUGIN);
        pluginA.addLoadAfter("PluginB");
        pluginB.addLoadAfter("PluginA");
        loadGraph.addConstraints(pluginA, pluginB);
        loadGraph.getLoadOrder(); //Should throw the CircularLoadDependencyException
    }

    @Test
    public void testIdentifiesProblemPlugins() throws Exception {
        LoadConstraints pluginA = new LoadConstraints("PluginA", LoadPrecedenceLevel.PLUGIN);
        LoadConstraints pluginB = new LoadConstraints("PluginB", LoadPrecedenceLevel.PLUGIN);
        LoadConstraints pluginC = new LoadConstraints("PluginC", LoadPrecedenceLevel.PLUGIN);
        pluginA.addLoadAfter("PluginB");
        pluginB.addLoadAfter("PluginA");
        pluginC.addLoadAfter("PluginA");
        loadGraph.addConstraints(pluginA, pluginB, pluginC);
        try {
            loadGraph.getLoadOrder();
        } catch (CircularLoadDependencyException ignored) {
            System.out.println(ignored.getLocalizedMessage());
            System.out.println(loadGraph.getLoadOrder());
        }
        assertTrue("getCyclicDependents() didn't catch the problematic PluginA", loadGraph.getCyclicDependents().contains("PluginA".toLowerCase()));
        assertTrue("getCyclicDependents() didn't catch the problematic PluginB", loadGraph.getCyclicDependents().contains("PluginB".toLowerCase()));
        assertFalse("getCyclicDependents() did catch the not problematic PluginC", loadGraph.getCyclicDependents().contains("PluginC".toLowerCase()));
        assertTrue("loadOrder doesn't remove problematic plugins", loadGraph.getLoadOrder().equals(Collections.singletonList("pluginc")));
    }

    @Test
    public void testIdentifiesProblemPluginsBigLoop() throws Exception {
        LoadConstraints pluginA = new LoadConstraints("PluginA", LoadPrecedenceLevel.PLUGIN);
        LoadConstraints pluginB = new LoadConstraints("PluginB", LoadPrecedenceLevel.PLUGIN);
        LoadConstraints pluginC = new LoadConstraints("PluginC", LoadPrecedenceLevel.PLUGIN);
        pluginA.addLoadAfter("PluginC");
        pluginB.addLoadAfter("PluginA");
        pluginC.addLoadAfter("PluginB");
        loadGraph.addConstraints(pluginA, pluginB, pluginC);
        try {
            loadGraph.getLoadOrder();
        } catch (CircularLoadDependencyException ignored) {}
        assertTrue("getCyclicDependents() didn't catch the problematic PluginA", loadGraph.getCyclicDependents().contains("PluginA".toLowerCase()));
        assertTrue("getCyclicDependents() didn't catch the problematic PluginB", loadGraph.getCyclicDependents().contains("PluginB".toLowerCase()));
        assertTrue("getCyclicDependents() didn't catch the problematic PluginC", loadGraph.getCyclicDependents().contains("PluginC".toLowerCase()));
        assertTrue("loadOrder doesn't remove problematic plugins", loadGraph.getLoadOrder().isEmpty());
    }

    @Test
    public void testLoadOrder() throws Exception {
        LoadConstraints pluginA = new LoadConstraints("PluginA", LoadPrecedenceLevel.PLUGIN);
        LoadConstraints pluginB = new LoadConstraints("PluginB", LoadPrecedenceLevel.PLUGIN);
        LoadConstraints pluginC = new LoadConstraints("PluginC", LoadPrecedenceLevel.PLUGIN);
        pluginB.addLoadAfter("PluginA");
        pluginC.addLoadAfter("PluginB");
        loadGraph.addConstraints(pluginA, pluginB, pluginC);
        assertTrue("incorrect load order", loadGraph.getLoadOrder().equals(Arrays.asList("plugina", "pluginb", "pluginc")));
    }

    @Test
    public void testEmptyConstraints() throws Exception {
        LoadConstraints pluginA = new LoadConstraints("PluginA", LoadPrecedenceLevel.PLUGIN);
        LoadConstraints pluginB = new LoadConstraints("PluginB", LoadPrecedenceLevel.PLUGIN);
        LoadConstraints pluginC = new LoadConstraints("PluginC", LoadPrecedenceLevel.PLUGIN);
        loadGraph.addConstraints(pluginA, pluginB, pluginC);
        assertTrue("load order dropped plugina", loadGraph.getLoadOrder().contains("plugina"));
        assertTrue("load order dropped pluginb", loadGraph.getLoadOrder().contains("pluginb"));
        assertTrue("load order dropped pluginc", loadGraph.getLoadOrder().contains("pluginc"));
    }

    @Test
    public void testSemiComplexLoadConstraints() throws Exception {
        LoadConstraints pluginA = new LoadConstraints("PluginA", LoadPrecedenceLevel.PLUGIN);
        LoadConstraints pluginB = new LoadConstraints("PluginB", LoadPrecedenceLevel.PLUGIN);
        LoadConstraints pluginC = new LoadConstraints("PluginC", LoadPrecedenceLevel.PLUGIN);
        LoadConstraints pluginD = new LoadConstraints("PluginD", LoadPrecedenceLevel.PLUGIN);
        pluginD.addLoadAfter("PluginC");
        pluginD.addLoadAfter("PluginB");
        pluginC.addLoadAfter("PluginA");
        pluginB.addLoadAfter("PluginC");
        loadGraph.addConstraints(pluginA, pluginB, pluginC, pluginD);

        List<String> loadOrder = loadGraph.getLoadOrder();
        assertTrue("Expected load order of [plugina, pluginc, pluginb, plugind] but was "+loadOrder, loadOrder.equals(Arrays.asList("plugina", "pluginc", "pluginb", "plugind")));
    }
}