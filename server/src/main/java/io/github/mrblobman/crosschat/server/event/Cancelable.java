package io.github.mrblobman.crosschat.server.event;

/**
 * Created by MrBlobman on 15-09-04.
 */
public interface Cancelable {
    /**
     * Set this event's canceled state.
     * @param cancel true to set this event as canceled
     */
    void setCanceled(boolean cancel);

    /**
     * Check if this event is canceled.
     * @return true if the event is set to canceled.
     */
    boolean isCanceled();
}
