package io.github.mrblobman.crosschat.server.event.message;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.event.CancelableEvent;
import io.github.mrblobman.crosschat.server.model.Player;

/**
 * Created by MrBlobman on 15-09-03.
 */
public abstract class PlayerMessageEvent extends CancelableEvent {
    private Player sender;
    private ChatMessageBulk message;
    private long timestamp = System.currentTimeMillis();

    public PlayerMessageEvent(Player sender, ChatMessageBulk message) {
        this.sender = sender;
        this.message = message;
    }

    public ChatMessageBulk getMessage() {
        return message;
    }

    public void setMessage(ChatMessageBulk message) {
        this.message = message;
    }

    public Player getSender() {
        return sender;
    }

    public void setSender(Player sender) {
        this.sender = sender;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
