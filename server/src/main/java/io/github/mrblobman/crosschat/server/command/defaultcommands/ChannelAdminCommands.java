package io.github.mrblobman.crosschat.server.command.defaultcommands;

import io.github.mrblobman.crosschat.network.data.ChatColor;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.command.management.CommandHandle;
import io.github.mrblobman.crosschat.server.command.management.CommandHandler;
import io.github.mrblobman.crosschat.server.command.management.CommandSender;
import io.github.mrblobman.crosschat.server.model.*;
import io.github.mrblobman.crosschat.server.permission.DefaultPermissions;

import java.util.regex.Pattern;

/**
 * Created by MrBlobman on 9/13/2015.
 */
public class ChannelAdminCommands implements CommandHandler {
    private static final Pattern INT_PATTERN = Pattern.compile("\\d+");
    private ChannelManager channels;
    private PlayerRegistry players;

    public ChannelAdminCommands(ChannelManager channels, PlayerRegistry players) {
        this.channels = channels;
        this.players = players;
    }

    @CommandHandle(command = {"channel|c", "moveuser|moveother"}, permission = DefaultPermissions.COMMAND_CHANNEL_MOVE_OTHER, description = "Move another user to a specific channel.")
    private void move(CommandSender sender, String playerToMoveName, String targetChannel) {
        Player player = players.getByName(playerToMoveName, false);
        if (player == null) {
            //Try to see if we can find something similar
            sender.sendMessage(CommandUtils.buildCommandSuggestionWithValidPlayer(players, playerToMoveName, "/channel moveuser {player} "+targetChannel));
            return;
        }

        ChatChannel channel;
        if (INT_PATTERN.matcher(targetChannel).matches()) {
            channel = channels.getById(Integer.parseInt(targetChannel));
            if (channel == null) {
                sender.sendMessage(new ChatMessageBulk("Unknown channel id.").setColor(ChatColor.RED));
                return;
            }
        } else {
            channel = channels.getByName(targetChannel, false);
            if (channel == null) {
                sender.sendMessage(CommandUtils.buildCommandSuggestionWithValidChannel(channels, targetChannel, "/channel moveuser " + playerToMoveName + " {channel}"));
                return;
            }
        }

        try {
            //This is an admin command so we want to skip the permission checks
            channel.add(player, true);
        } catch (ChannelException e) {
            //Won't happen so swallow
        }
    }

    @CommandHandle(command = {"channel|c", "kickuser"}, permission = DefaultPermissions.COMMAND_CHANNEL_KICK, description = "Kick a user from the channel they are in.")
    private void kick(CommandSender sender, String playerToKickName, String... reason) throws ChannelException {
        String kickMessage = CommandUtils.rebuildSplitVarargs(reason);

        Player player = players.getByName(playerToKickName, false);
        if (player == null) {
            //Try to see if we can find something similar
            sender.sendMessage(CommandUtils.buildCommandSuggestionWithValidPlayer(players, playerToKickName, "/channel kickuser {player} " + kickMessage));
            return;
        }

        player.sendMessage(ChatMessageBulk.fromDecoratedString(kickMessage));
        player.switchChannels(channels.getDefaultChannel());
    }
}
