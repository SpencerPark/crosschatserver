package io.github.mrblobman.crosschat.server;

import io.github.mrblobman.crosschat.network.packets.Protocol;
import io.github.mrblobman.crosschat.network.pipeline.common.Frame;
import io.github.mrblobman.crosschat.network.pipeline.common.NullHandler;
import io.github.mrblobman.crosschat.network.pipeline.server.InBoundHandler;
import io.github.mrblobman.crosschat.network.pipeline.server.PacketHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * Created by MrBlobman on 15-08-30.
 */
public class CrossChatServerChannelInitializer extends ChannelInitializer<NioSocketChannel> {
    private static final NullHandler NULL_HANDLER = new NullHandler();

    private ClientManager clientManager;
    private Protocol serverProtocol;
    private NetworkServer netServer;
    private CrossChatServer server;

    public CrossChatServerChannelInitializer(ClientManager clientManager, Protocol serverProtocol, NetworkServer netServer, CrossChatServer server) {
        this.clientManager = clientManager;
        this.serverProtocol = serverProtocol;
        this.netServer = netServer;
        this.server = server;
    }

    @Override
    protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
        //Register a client
        Client client = clientManager.wrap(nioSocketChannel);
        //Setup the pipeline
        nioSocketChannel.pipeline()
                .addLast("encryption", NULL_HANDLER)
                .addLast("frame", new Frame())
                .addLast("compression", NULL_HANDLER)
                .addLast("packet", new PacketHandler(serverProtocol, client))
                .addLast("handler", new InBoundHandler(client, netServer, server));
    }


}
