package io.github.mrblobman.crosschat.server.plugin.loading;

/**
 * Created on 17/11/2015.
 */
public class LoadConstraint {
    private String first;
    private String second;

    LoadConstraint(String first, String second) {
        this.first = first;
        this.second = second;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoadConstraint that = (LoadConstraint) o;

        return first.equals(that.first) && second.equals(that.second);

    }

    @Override
    public int hashCode() {
        int result = first.hashCode();
        result = 31 * result + second.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return this.first + " -> " + this.second;
    }
}
