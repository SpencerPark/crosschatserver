package io.github.mrblobman.crosschat.server.permission;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by MrBlobman on 15-09-04.
 */
public class PermissionAttachment {
    private Set<PermissionGroup> permissionGroups = new HashSet<>();
    private PermissionStore permissions = new PermissionStore();

    public PermissionAttachment() {}

    public PermissionAttachment(JsonObject serialized) throws JsonParseException {
        try {
            serialized.get("groups").getAsJsonArray().forEach(serializedGroup -> permissionGroups.add(PermissionGroup.createBlankGroup(serializedGroup.getAsString())));
            this.permissions = new PermissionStore(serialized.get("perms").getAsJsonArray());
        } catch (IllegalStateException e) {
            throw new JsonParseException(e);
        }
    }

    public void givePermission(String permission) {
        this.permissions.addPermission(permission);
    }

    public boolean hasPermission(String permission) {
        return permissions.hasPermission(permission)
                || permissionGroups.stream().anyMatch(group -> group.hasPermission(permission));
    }

    public void removePermission(String permission) {
        permissions.removePermission(permission);
    }

    public void giveGroup(PermissionGroup group) {
        this.permissionGroups.add(group);
    }

    public void removeGroup(PermissionGroup group) {
        this.permissionGroups.remove(group);
    }

    public boolean removeGroup(String groupName) {
        return this.permissionGroups.removeIf(group -> group.getGroupName().equalsIgnoreCase(groupName));
    }

    public boolean isInGroup(PermissionGroup group) {
        return this.permissionGroups.contains(group);
    }

    public boolean isInGroup(String groupName) {
        return this.permissionGroups.parallelStream().anyMatch(group -> group.getGroupName().equalsIgnoreCase(groupName));
    }

    public JsonObject serialize() {
        JsonObject serialized = new JsonObject();
        JsonArray groups = new JsonArray();
        permissionGroups.forEach(group -> groups.add(new JsonPrimitive(group.getGroupName())));
        serialized.add("groups", groups);
        serialized.add("perms", permissions.serialize());
        return serialized;
    }

    @Override
    public String toString() {
        StringBuilder info = new StringBuilder("Groups:");
        this.permissionGroups.forEach(group -> info.append("\n\t- ").append(group.getGroupName()));
        info.append('\n');
        info.append("Permissions:");
        info.append(this.permissions.toString());
        return info.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermissionAttachment that = (PermissionAttachment) o;

        return permissionGroups.equals(that.permissionGroups) && permissions.equals(that.permissions);

    }

    @Override
    public int hashCode() {
        int result = permissionGroups.hashCode();
        result = 31 * result + permissions.hashCode();
        return result;
    }
}
