package io.github.mrblobman.crosschat.server.permission;

/**
 * Created by Spencer on 9/12/2015.
 */
public interface Permissible {
    /**
     * Check if this permissible has the given permission.
     * @param permission the permission to check
     * @return true if this {@link Permissible} has the {@code permission}
     */
    boolean hasPermission(String permission);

    /**
     * Give this {@link Permissible} the given {@code permission}
     * @param permission the permission to give this {@link Permissible}
     */
    void givePermission(String permission);

    void removePermission(String permission);

    boolean isInPermissionGroup(String permissionGroupName);

    void joinPermissionGroup(PermissionGroup group);

    void leavePermissionGroup(String group);
}
