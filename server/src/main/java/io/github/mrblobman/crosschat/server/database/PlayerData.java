package io.github.mrblobman.crosschat.server.database;

import io.github.mrblobman.crosschat.server.permission.PermissionAttachment;

/**
 * Created by MrBlobman on 10/09/2015.
 */
public class PlayerData {
    private PermissionAttachment permissions;

    public PlayerData(PermissionAttachment permissions) {
        this.permissions = permissions;
    }

    public PermissionAttachment getPermissions() {
        return permissions;
    }

    public void setPermissions(PermissionAttachment permissions) {
        this.permissions = permissions;
    }

    /**
     * For use by the {@link PlayerData#blankData()} method.
     */
    private PlayerData() {
        permissions = new PermissionAttachment();
    }

    public static PlayerData blankData() {
        return new PlayerData();
    }
}
