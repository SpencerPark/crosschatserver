package io.github.mrblobman.crosschat.server.plugin;

/**
 * Created by MrBlobman on 15-09-07.
 */
public class PluginLoadExcetpion extends Exception {
    public PluginLoadExcetpion(String message) {
        super(message);
    }

    public PluginLoadExcetpion(String message, Throwable cause) {
        super(message, cause);
    }

    public PluginLoadExcetpion(Throwable cause) {
        super(cause);
    }

    public PluginLoadExcetpion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
