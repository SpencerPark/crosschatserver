package io.github.mrblobman.crosschat.server.event;

import java.lang.reflect.Method;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class Handler<T extends Event> {
    private Object handler;
    private Method handle;

    protected Handler(Object handler, Method handle) {
        this.handler = handler;
        this.handle = handle;
    }

    /**
     * Invoke the handle.
     * @param event the event being called
     * @throws Exception
     */
    public void handle(Event event) throws Exception {
        handle.invoke(handler, event);
    }

    public Object getHandler() {
        return handler;
    }
}
