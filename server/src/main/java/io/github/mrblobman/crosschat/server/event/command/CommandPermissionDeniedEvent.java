package io.github.mrblobman.crosschat.server.event.command;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.command.management.CommandSender;

import java.util.List;

/**
 * Created by MrBlobman on 9/13/2015.
 */
public class CommandPermissionDeniedEvent extends CommandEvent {
    private ChatMessageBulk deniedMessage;
    private boolean sendDeniedMessage;

    public CommandPermissionDeniedEvent(String command, List<String> args, CommandSender sender, ChatMessageBulk deniedMessage, boolean sendDeniedMessage) {
        super(command, args, sender);
        this.deniedMessage = deniedMessage;
        this.sendDeniedMessage = sendDeniedMessage;
    }

    public ChatMessageBulk getDeniedMessage() {
        return deniedMessage;
    }

    public void setDeniedMessage(ChatMessageBulk deniedMessage) {
        this.deniedMessage = deniedMessage;
    }

    public boolean sendDeniedMessage() {
        return sendDeniedMessage;
    }

    public void setSendDeniedMessage(boolean sendDeniedMessage) {
        this.sendDeniedMessage = sendDeniedMessage;
    }

    @Override
    public String getName() {
        return "CommandPermissionDeniedEvent";
    }
}
