package io.github.mrblobman.crosschat.server.plugin.loading;

import java.util.HashSet;
import java.util.Set;

/**
 * Created on 17/11/2015.
 */
public class LoadConstraints {
    private final String pluginName;
    private final LoadPrecedenceLevel level;
    private Set<String> loadAfter;

    /**
     * @param pluginName the (CaSe InsEnSiTiVe) plugin name, should be unique
     * @param level the load level
     */
    public LoadConstraints(String pluginName, LoadPrecedenceLevel level) {
        this.pluginName = pluginName.toLowerCase();
        this.level = level;
        loadAfter = new HashSet<>();
    }

    public LoadPrecedenceLevel getLoadPrecedenceLevel() {
        return this.level;
    }

    public String getPluginName() {
        return this.pluginName;
    }

    public void addLoadAfter(String pluginId) {
        this.loadAfter.add(pluginId.toLowerCase());
    }

    public void removeLoadAfter(String pluginId) {
        this.loadAfter.remove(pluginId.toLowerCase());
    }

    public Set<LoadConstraint> getConstraints() {
        Set<LoadConstraint> constraints = new HashSet<>();
        this.loadAfter.forEach(loadAfterThis ->
                constraints.add(new LoadConstraint(loadAfterThis, pluginName)));
        return constraints;
    }

    @Override
    public String toString() {
        return pluginName + "-" + level.name();
    }
}
