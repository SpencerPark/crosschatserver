package io.github.mrblobman.crosschat.server.command.defaultcommands;

import io.github.mrblobman.crosschat.network.data.ChatColor;
import io.github.mrblobman.crosschat.network.data.ChatMessage;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.command.management.CommandHandle;
import io.github.mrblobman.crosschat.server.command.management.CommandHandler;
import io.github.mrblobman.crosschat.server.command.management.CommandSender;
import io.github.mrblobman.crosschat.server.model.*;
import io.github.mrblobman.crosschat.server.permission.DefaultPermissions;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by MrBlobman on 9/13/2015.
 */
public class ChannelCommands implements CommandHandler {
    private static final Pattern INT_PATTERN = Pattern.compile("\\d+");
    private ChannelManager channels;
    private PlayerRegistry players;

    public ChannelCommands(ChannelManager channels, PlayerRegistry players) {
        this.channels = channels;
        this.players = players;
    }

    @CommandHandle(command = {"channel|c", "list|l"}, permission = DefaultPermissions.COMMAND_LIST_CHANNELS, description = "Get information about the channels.")
    private void list(CommandSender sender) {
        sender.sendMessage(channels.channelInfo());
    }

    @CommandHandle(command = {"channel|c", "join|j"}, permission = DefaultPermissions.COMMAND_JOIN_CHANNEL, description = "Join the given channel.")
    private void join(Player player, String targetChannel) {
        ChatChannel channel;
        if (INT_PATTERN.matcher(targetChannel).matches()) {
            channel = channels.getById(Integer.parseInt(targetChannel));
            if (channel == null) {
                player.sendMessage(new ChatMessageBulk("Unknown channel id.").setColor(ChatColor.RED));
                return;
            }
        } else {
            channel = channels.getByName(targetChannel, false);
            if (channel == null) {
                List<ChatChannel> possibleChannels = channels.searchFor(targetChannel);
                if (possibleChannels.isEmpty()) {
                    player.sendMessage(new ChatMessageBulk("There are no channels with the name "+targetChannel+".").setColor(ChatColor.RED));
                } else {
                    ChatMessageBulk msg = new ChatMessageBulk("Unknown channel. ").setColor(ChatColor.RED);
                    msg.append("Did you mean:").setColor(ChatColor.YELLOW);
                    possibleChannels.forEach(possibleChannel -> {
                        msg.append(" " + possibleChannel.getDisplayName());
                        msg.setTooltip(ChatColor.AQUA + "Click to resend the command with", ChatColor.AQUA + possibleChannel.getDisplayName() + " as the targetChannel.");
                        msg.setClickAction(ChatMessage.CLICK_RUN_COMMAND, "/channel join " + possibleChannel.getDisplayName());
                    });
                }
                return;
            }
        }

        try {
            player.switchChannels(channel);
        } catch (ChannelException e) {
            player.sendMessage(new ChatMessageBulk("Cannot join "+targetChannel+". "+e.getLocalizedMessage()).setColor(ChatColor.RED));
        }
    }
}
