package io.github.mrblobman.crosschat.server.event.player;

import io.github.mrblobman.crosschat.auth.data.GameProfile;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.event.CancelableEvent;
import io.github.mrblobman.crosschat.server.model.Player;

/**
 * This event is fired upon successful login to the server.
 * The connected client has yet to become a {@link Player}.
 * @see PlayerJoinEvent
 */
public class PlayerLoginEvent extends CancelableEvent {
    private GameProfile gameProfile;
    private ChatMessageBulk disconnectMessage = new ChatMessageBulk("Login canceled.");

    public PlayerLoginEvent(GameProfile profile) {
        this.gameProfile = profile;
    }

    public GameProfile getGameProfile() {
        return this.gameProfile;
    }

    public ChatMessageBulk getDisconnectMessage() {
        return this.disconnectMessage;
    }

    public void setDisconnectMessage(ChatMessageBulk disconnectMessage) {
        this.disconnectMessage = disconnectMessage;
    }

    @Override
    public String getName() {
        return "PlayerLoginEvent";
    }
}
