package io.github.mrblobman.crosschat.server.plugin.loading;

/**
 * Created on 17/11/2015.
 */
public class CircularLoadDependencyException extends Exception {
    public CircularLoadDependencyException() {
    }

    public CircularLoadDependencyException(String s) {
        super(s);
    }

    public CircularLoadDependencyException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public CircularLoadDependencyException(Throwable throwable) {
        super(throwable);
    }
}
