package io.github.mrblobman.crosschat.server.event.channel;

import io.github.mrblobman.crosschat.server.event.Cancelable;
import io.github.mrblobman.crosschat.server.model.ChatChannel;
import io.github.mrblobman.crosschat.server.model.Player;

/**
 * Created by MrBlobman on 15-09-04.
 */
public class ChannelChangeEvent extends ChannelEvent implements Cancelable {
    private boolean isCanceled = false;
    private ChatChannel oldChannel;
    private Player player;

    public ChannelChangeEvent(ChatChannel newChannel, ChatChannel oldChannel, Player player) {
        super(newChannel);
        this.oldChannel = oldChannel;
        this.player = player;
    }

    public ChatChannel getOldChannel() {
        return oldChannel;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public void setCanceled(boolean cancel) {
        this.isCanceled = cancel;
    }

    @Override
    public boolean isCanceled() {
        return this.isCanceled;
    }

    @Override
    public String getName() {
        return "ChannelChangeEvent";
    }
}
