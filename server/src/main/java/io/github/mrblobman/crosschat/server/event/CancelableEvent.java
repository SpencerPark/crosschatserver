package io.github.mrblobman.crosschat.server.event;

/**
 * Created by MrBlobman on 15-09-03.
 */
public abstract class CancelableEvent extends Event implements Cancelable {
    private boolean isCanceled = false;

    public void setCanceled(boolean cancel) {
        this.isCanceled = cancel;
    }

    public boolean isCanceled() {
        return this.isCanceled;
    }
}
