package io.github.mrblobman.crosschat.server;

import io.netty.channel.Channel;
import io.netty.channel.ChannelId;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by MrBlobman on 15-08-31.
 */
public class ClientManager {
    private Map<ChannelId, Client> clientLookup = new ConcurrentHashMap<>();

    public ClientManager() {}

    /**
     * This will create a new client from the given channel
     * or return the existing client registered for the channel.
     * @param channel the {@link Channel} that the client is using
     *                to communicate with the server
     * @return the newly created or existing {@link Client}
     */
    public Client wrap(Channel channel) {
        if (clientLookup.containsKey(channel.id())) return clientLookup.get(channel.id());
        Client client = new Client(this, channel);
        this.clientLookup.put(channel.id(), client);
        return client;
    }

    /**
     * Lookup a Client based on their {@link ChannelId}.
     * @param channelId the {@link ChannelId} obtained with {@link Channel#id()}
     *                  for the {@link Client}
     * @return the {@link Client} registered with {@code channelId} or null if
     *         none are registered
     */
    public Client lookup(ChannelId channelId) {
        return this.clientLookup.get(channelId);
    }

    /**
     * Remove a ChannelId from the registry
     * @param channelId the {@link ChannelId} of the {@link Client} to remove.
     * @return true if a {@link Client} was removed, false otherwise
     */
    public boolean remove(ChannelId channelId) {
        return this.clientLookup.remove(channelId) != null;
    }

    /**
     * Remove a Client from the registry.
     * @param client the {@link Client} to remove.
     * @return true if a {@link Client} was removed, false otherwise
     */
    public boolean remove(Client client) {
        return remove(client.getChannelId());
    }
}
