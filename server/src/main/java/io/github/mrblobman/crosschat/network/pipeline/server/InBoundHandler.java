package io.github.mrblobman.crosschat.network.pipeline.server;

import io.github.mrblobman.crosschat.network.ServerLoginHandler;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.PacketIds;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.serverbound.ChannelMessagePacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.serverbound.CommandPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.serverbound.PrivateMessagePacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.serverbound.EncryptionResponsePacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.serverbound.LoginStartPacket;
import io.github.mrblobman.crosschat.server.Client;
import io.github.mrblobman.crosschat.server.CrossChatServer;
import io.github.mrblobman.crosschat.server.NetworkServer;
import io.github.mrblobman.crosschat.server.command.management.CommandException;
import io.github.mrblobman.crosschat.server.event.player.PlayerQuitEvent;
import io.github.mrblobman.crosschat.server.model.Player;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.UUID;
import java.util.logging.Level;

/**
 * Created by MrBlobman on 15-08-30.
 */
public class InBoundHandler extends SimpleChannelInboundHandler<Packet> {
    private Client client;
    private Player player = null;
    private ServerLoginHandler loginHandler;
    private NetworkServer netServer;
    private CrossChatServer server;

    public InBoundHandler(Client client, NetworkServer netServer, CrossChatServer server) {
        this.client = client;
        this.netServer = netServer;
        this.server = server;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        server.getLogger().info("Channel connected: " + ctx.channel().remoteAddress());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        server.getLogger().info("Channel disconnected: " + ctx.channel().remoteAddress());
        if (client.isLoggedIn()) {
            Player player = server.getOnlinePlayers().getByUUID(client.getGameProfile().getId());
            if (player != null) {
                server.getOnlinePlayers().unregister(player);
                PlayerQuitEvent event = new PlayerQuitEvent(player, PlayerQuitEvent.QuitReason.TIMED_OUT);
                server.getEventBus().boardBus(event);
            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        server.getLogger().log(Level.SEVERE, "Exception in channel. Disconnecting " + ctx.channel().remoteAddress() + ". Reason: " + cause.getLocalizedMessage());
        client.disconnect("ERROR: " + cause.getLocalizedMessage());
    }

    @Override
    protected void messageReceived(ChannelHandlerContext channelHandlerContext, Packet packet) throws Exception {
        switch(client.getState()) {
            case HANDSHAKE:
                //Nothing to do.
                break;
            case LOGIN:
                handleLogin(packet);
                break;
            case AUTHENTICATED:
                handleRegular(packet);
                break;
            case STATUS:
                handleStatus(packet);
                break;
        }
    }

    private void handleLogin(Packet packet) {
        if (packet instanceof LoginStartPacket) {
            if (this.loginHandler != null) return;
            LoginStartPacket loginStartPacket = (LoginStartPacket) packet;
            this.loginHandler = new ServerLoginHandler(netServer, client, loginStartPacket.getName(), server);
        } else if (packet instanceof EncryptionResponsePacket) {
            if (this.loginHandler == null) return;
            EncryptionResponsePacket encryptionResponsePacket = (EncryptionResponsePacket) packet;
            this.loginHandler.onEncryptionResponse(encryptionResponsePacket);
        }
    }

    private void handleRegular(Packet packet) throws CommandException {
        if (this.player == null) {
            UUID id = client.getGameProfile().getId();
            this.player = server.getOnlinePlayers().getByUUID(id);
            if (this.player == null) {
                this.player = server.getOnlinePlayers().register(this.client);
            }
        }
        switch (packet.getId()) {
            case PacketIds.ServerBound.CHANNEL_MESSAGE:
                ChannelMessagePacket channelMessagePacket = (ChannelMessagePacket) packet;
                player.getCurrentChannel().sendMessage(channelMessagePacket.getMessage());
                break;
            case PacketIds.ServerBound.PRIVATE_MESSAGE:
                PrivateMessagePacket privateMessagePacket = (PrivateMessagePacket) packet;
                this.server.sendPM(player, privateMessagePacket.getTarget(), privateMessagePacket.getMessage());
                break;
            case PacketIds.ServerBound.COMMAND:
                CommandPacket commandPacket = ((CommandPacket) packet);
                this.server.getCommandManager().dispatchCommand(player, commandPacket.getCommand());
                break;
        }
    }

    private void handleStatus(Packet packet) {
        System.out.println("[STATUS] "+packet);
    }
}
