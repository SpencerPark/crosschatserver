package io.github.mrblobman.crosschat.server.event.player;

import io.github.mrblobman.crosschat.server.model.Player;

/**
 * Created by Spencer on 9/11/2015.
 */
public class PlayerQuitEvent extends PlayerEvent {
    public enum QuitReason {
        TIMED_OUT, DISCONNECTED, KICKED;
    }

    private QuitReason reason;

    public PlayerQuitEvent(Player player, QuitReason reason) {
        super(player);
        this.reason = reason;
    }

    public QuitReason getReason() {
        return reason;
    }

    @Override
    public String getName() {
        return "PlayerQuitEvent";
    }
}
