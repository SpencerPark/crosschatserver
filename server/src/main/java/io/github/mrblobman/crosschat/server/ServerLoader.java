package io.github.mrblobman.crosschat.server;

import io.github.mrblobman.crosschat.configuration.ConfigParser;
import io.github.mrblobman.crosschat.configuration.FileConfiguration;
import io.github.mrblobman.crosschat.server.model.ConsoleCommandSender;

import java.io.File;
import java.util.Scanner;

/**
 * Created by MrBlobman on 15-08-28.
 */
public class ServerLoader {
    public static void main(String[] args) throws Exception {
        File configFile = new File("server.json");
        ConfigParser parser = new ConfigParser(true);
        FileConfiguration<ServerConfig> config = new FileConfiguration<>(ServerConfig.class, "server.json", configFile, parser);
        ServerConfig serverConfig = config.getData();

        CrossChatServer crossChatServer = new CrossChatServer(serverConfig);

        ConsoleCommandSender cmdSender = new ConsoleCommandSender(serverConfig.useColor());

        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            String cmd = scanner.nextLine();
            if (cmd.equalsIgnoreCase("stop")) {
                crossChatServer.stop();
                return;
            } else {
                crossChatServer.getCommandManager().dispatchCommand(cmdSender, cmd);
            }
        }
    }
}
