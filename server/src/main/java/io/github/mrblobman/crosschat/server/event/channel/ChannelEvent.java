package io.github.mrblobman.crosschat.server.event.channel;

import io.github.mrblobman.crosschat.server.event.Event;
import io.github.mrblobman.crosschat.server.model.ChatChannel;

/**
 * Created by MrBlobman on 15-09-04.
 */
public abstract class ChannelEvent extends Event {
    private ChatChannel channel;

    public ChannelEvent(ChatChannel channel) {
        this.channel = channel;
    }

    public ChatChannel getChannel() {
        return channel;
    }

    public void setChannel(ChatChannel channel) {
        this.channel = channel;
    }
}
