package io.github.mrblobman.crosschat.server.permission;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MrBlobman on 15-09-04.
 */
public class PermissionNode {
    private PermissionNode parent;
    private String name;
    private Map<String, PermissionNode> children;
    private boolean isWildcard = false;

    protected PermissionNode(PermissionNode parent, String name) {
        this(parent, name, new HashMap<>());
    }

    protected PermissionNode(PermissionNode parent, String name, Map<String, PermissionNode> children) {
        this.parent = parent;
        this.name = name.toLowerCase();
        if (name.equals("*")) this.isWildcard = true;
        this.children = children;
    }

    protected PermissionNode(PermissionNode parent, String name, JsonElement node) {
        if (!node.isJsonObject()) throw new JsonParseException("node is not a JsonObject. ("+name+")");
        this.parent = parent;
        this.name = name.toLowerCase();
        if (name.equals("*")) this.isWildcard = true;
        this.children = new HashMap<>();
        JsonObject children = node.getAsJsonObject();
        children.entrySet().forEach(entry -> this.children.put(entry.getKey().toLowerCase(), new PermissionNode(this, entry.getKey(), entry.getValue())));
    }

    public PermissionNode getParent() {
        return parent;
    }

    public boolean isRoot() {
        return this.parent != null;
    }

    public String getName() {
        return name;
    }

    public PermissionNode addChild(String name) {
        PermissionNode node = new PermissionNode(this, name);
        this.children.put(name.toLowerCase(), node);
        return node;
    }

    public PermissionNode getChild(String name) {
        return this.children.get(name.toLowerCase());
    }

    public boolean hasChild(String name) {
        return this.isWildcard || this.children.containsKey(name.toLowerCase());
    }

    public boolean hasWildcardChild() {
        return this.children.containsKey("*");
    }

    public boolean isWildcard() {
        return isWildcard;
    }

    public boolean isLeaf() {
        return this.children.isEmpty();
    }

    public void removeChild(String name) {
        if (name.equals("*"))
            this.children.clear();
        else
            this.children.remove(name.toLowerCase());
    }

    /**
     * Merge two {@link PermissionNode}s together.
     * This will add all permissions from {@code other} to
     * this {@link PermissionNode}.
     * @param other the other set of permissions to add to this
     *              permission store.
     */
    public void merge(PermissionNode other) {
        other.children.forEach((childName, child) -> this.children.merge(childName, child,
                (oldVal, val) -> {
                    oldVal.merge(val);
                    return oldVal;
                }
            )
        );
    }

    protected JsonObject serialize() {
        JsonObject serialized = new JsonObject();
        this.children.forEach((nodeName, childNode) -> serialized.add(nodeName, childNode.serialize()));
        return serialized;
    }

    @Override
    public String toString() {
        return "PermissionNode{" +
                "name='" + name + '\'' +
                ", children=" + children +
                '}';
    }

    protected void format(StringBuilder sb, int depth) {
        sb.append('\n');
        for (int i = 0; i < depth; i++) sb.append("    ");
        sb.append("'");
        sb.append(name);
        sb.append("'");
        for (int i = 0; i < depth; i++) sb.append("    ");
        this.children.forEach((name, child) -> child.format(sb, depth+1));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermissionNode that = (PermissionNode) o;

        //We only want to compare parent names because otherwise we get a stackoverflow with
        //the recurse down all the children.
        if (parent != null ? !parent.name.equals(that.parent.name) : that.parent != null) return false;
        if (!name.equals(that.name)) return false;
        return children.equals(that.children);

    }

    @Override
    public int hashCode() {
        int result = parent != null ? parent.name.hashCode() : 0;
        result = 31 * result + name.hashCode();
        result = 31 * result + children.hashCode();
        return result;
    }
}
