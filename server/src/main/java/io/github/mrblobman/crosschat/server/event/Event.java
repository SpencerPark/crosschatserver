package io.github.mrblobman.crosschat.server.event;

/**
 * Created by MrBlobman on 15-09-03.
 */
public abstract class Event {
    /**
     * @return a user-friendly name for this event
     */
    public abstract String getName();
}
