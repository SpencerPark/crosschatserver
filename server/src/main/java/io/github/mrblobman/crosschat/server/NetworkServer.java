package io.github.mrblobman.crosschat.server;

import io.github.mrblobman.crosschat.network.packets.Protocol;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.ProtocolImpl;
import io.github.mrblobman.crosschat.server.event.EventBus;
import io.github.mrblobman.crosschat.util.CryptUtil;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetSocketAddress;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

/**
 * Created by MrBlobman on 15-08-27.
 */
public class NetworkServer {
    public static final int COMPRESSION_THRESHOLD = 1000;

    private final String serverID = ""; //Long.toString(new Random(System.currentTimeMillis()).nextLong(), 16);
    private final KeyPair keyPair;
    private final Protocol serverProtocol = new ProtocolImpl();
    private final ClientManager clientManager;
    private final EventBus eventBus;
    private final Logger logger;
    private final CrossChatServer server;

    private final EventLoopGroup bossGroup = new NioEventLoopGroup();
    private final EventLoopGroup workerGroup = new NioEventLoopGroup();

    protected NetworkServer(int port, EventBus bus, Logger logger, CrossChatServer server) throws NoSuchAlgorithmException {
        this.eventBus = bus;
        this.logger = logger;
        this.server = server;
        this.clientManager = new ClientManager();
        this.keyPair = CryptUtil.generateRSAKeyPair();
        setupNetworkServer(port);
    }

    private void setupNetworkServer(int port) {
        ServerBootstrap bootstrap = new ServerBootstrap()
                .group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new CrossChatServerChannelInitializer(clientManager, serverProtocol, this, server))
                .childOption(ChannelOption.TCP_NODELAY, true)
                .childOption(ChannelOption.SO_KEEPALIVE, true);
        bootstrap.bind(new InetSocketAddress(port));
    }
    /**
     * Stop the server. Cleans up any resources.
     */
    public void stop() {
        this.logger.info("Stopping...");
        this.bossGroup.shutdownGracefully();
        this.workerGroup.shutdownGracefully();
    }

    public ClientManager getClientManager() {
        return this.clientManager;
    }

    public KeyPair getKeyPair() {
        return this.keyPair;
    }

    public String getServerID() {
        return this.serverID;
    }

    public Logger getLogger() {
        return this.logger;
    }

    public EventBus getEventBus() {
        return eventBus;
    }
}
