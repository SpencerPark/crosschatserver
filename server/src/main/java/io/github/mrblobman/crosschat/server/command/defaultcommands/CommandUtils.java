package io.github.mrblobman.crosschat.server.command.defaultcommands;

import io.github.mrblobman.crosschat.network.data.ChatColor;
import io.github.mrblobman.crosschat.network.data.ChatMessage;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.model.ChannelManager;
import io.github.mrblobman.crosschat.server.model.ChatChannel;
import io.github.mrblobman.crosschat.server.model.Player;
import io.github.mrblobman.crosschat.server.model.PlayerRegistry;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 13/12/2015.
 */
public class CommandUtils {

    /**
     * Build a command suggestion list for players with a similar name.
     * @param players the player registry to search for players.
     * @param givenName the name given in the original invocation.
     * @param commandFormat the command format for resending. This command can contain "{player}"
     *                      which will be replace with the suggested players name on invocation.
     * @return the message to send to the original sender.
     */
    public static ChatMessageBulk buildCommandSuggestionWithValidPlayer(PlayerRegistry players, String givenName, String commandFormat) {
        Player[] possiblePlayers = players.searchFor(givenName);
        if (possiblePlayers.length == 0) {
            return new ChatMessageBulk("There are no players with the name "+givenName+".").setColor(ChatColor.RED);
        } else {
            ChatMessageBulk msg = new ChatMessageBulk("Unknown player. ").setColor(ChatColor.RED);
            msg.append("Did you mean:").setColor(ChatColor.YELLOW);
            for (Player p : possiblePlayers) {
                msg.append(" " + p.getName());
                msg.setTooltip(ChatColor.AQUA + "Click to resend the command with", ChatColor.AQUA + p.getName() + " as the player.");
                msg.setClickAction(ChatMessage.CLICK_RUN_COMMAND, commandFormat.replace("{player}", p.getName()));
            }
            return msg;
        }
    }

    /**
     * Build a command suggestion name with valid channel names.
     * @param channels the channel manager to search for the channel in.
     * @param givenChannel the name of the channel given in the original invocation.
     * @param commandFormat the command format for resending. This command can contain "{channel}"
     *                      which will be replaced with the name of the suggested channel in the
     *                      new invocation.
     * @return the message to send to the original sender.
     */
    public static ChatMessageBulk buildCommandSuggestionWithValidChannel(ChannelManager channels, String givenChannel, String commandFormat) {
        List<ChatChannel> possibleChannels = channels.searchFor(givenChannel);
        if (possibleChannels.isEmpty()) {
            return new ChatMessageBulk("There are no channels with the name "+givenChannel+".").setColor(ChatColor.RED);
        } else {
            ChatMessageBulk msg = new ChatMessageBulk("Unknown channel. ").setColor(ChatColor.RED);
            msg.append("Did you mean:").setColor(ChatColor.YELLOW);
            possibleChannels.forEach(possibleChannel -> {
                msg.append(" " + possibleChannel.getDisplayName());
                msg.setTooltip(ChatColor.AQUA + "Click to resend the command with", ChatColor.AQUA + possibleChannel.getDisplayName() + " as the targetChannel.");
                msg.setClickAction(ChatMessage.CLICK_RUN_COMMAND, commandFormat.replace("{channel}", possibleChannel.getDisplayName()));
            });
            return msg;
        }
    }

    /**
     * Put varargs back together for reinvocation.
     * @param varargs the varargs to put together
     * @return the rebuilt message
     */
    public static String rebuildSplitVarargs(String... varargs) {
        return Arrays.stream(varargs).collect(Collectors.joining(" "));
    }
}
