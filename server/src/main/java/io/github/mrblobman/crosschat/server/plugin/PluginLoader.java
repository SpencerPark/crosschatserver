package io.github.mrblobman.crosschat.server.plugin;

import io.github.mrblobman.crosschat.configuration.ConfigParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by MrBlobman on 15-09-07.
 */
public class PluginLoader {
    private ConfigParser parser;
    private PluginManager manager;

    public PluginLoader(ConfigParser parser, PluginManager manager) {
        this.parser = parser;
        this.manager = manager;
    }

    public PluginConfig getPluginConfig(File jarFile) throws InvalidPluginException, IOException {
        try (JarFile pluginJar = new JarFile(jarFile)) {
            JarEntry pluginConfigEntry = pluginJar.getJarEntry("plugin.json");
            if (pluginConfigEntry == null) throw new InvalidPluginException("Cannot find plugin.json inside "+jarFile.getPath());
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(pluginJar.getInputStream(pluginConfigEntry)))) {
                return ConfigParser.parse(parser, reader, PluginConfig.class);
            }
        }
    }

    public Plugin load(File jarFile) throws PluginLoadExcetpion, InvalidPluginException {
        try {
            return load(jarFile, getPluginConfig(jarFile));
        } catch (IOException e) {
            throw new PluginLoadExcetpion(e);
        }
    }

    public Plugin load(File jarFile, PluginConfig config) throws PluginLoadExcetpion {
        try (URLClassLoader classLoader = new URLClassLoader(new URL[] {jarFile.toURI().toURL()}, this.getClass().getClassLoader())) {
            Class<?> jarClass = Class.forName(config.main, true, classLoader);
            Class<? extends Plugin> pluginClass = jarClass.asSubclass(Plugin.class);
            Plugin plugin = pluginClass.newInstance();
            Method init = pluginClass.getMethod("init", Class.class, String.class, String.class, PluginManager.class);
            init.invoke(plugin, pluginClass, config.name, config.version, this.manager);
            return plugin;
        } catch (NoSuchMethodException | ClassNotFoundException | InstantiationException | InvocationTargetException | IOException | IllegalAccessException e) {
            throw new PluginLoadExcetpion(e);
        }
    }
}
