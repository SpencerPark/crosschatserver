package io.github.mrblobman.crosschat.server.event;

/**
 * Created by MrBlobman on 15-09-03.
 */
public enum HandlePriority {
    LOWEST,
    LOW,
    NORMAL,
    HIGH,
    HIGHEST,
    MONITOR;
}
