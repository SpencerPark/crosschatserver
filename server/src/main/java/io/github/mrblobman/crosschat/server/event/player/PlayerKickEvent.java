package io.github.mrblobman.crosschat.server.event.player;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.model.Player;

/**
 * Created by MrBlobman on 9/27/2015.
 */
public class PlayerKickEvent extends CancelablePlayerEvent {
    private ChatMessageBulk reason;

    public PlayerKickEvent(Player player, ChatMessageBulk reason) {
        super(player);
        this.reason = reason;
    }

    public ChatMessageBulk getReason() {
        return reason;
    }

    public void setReason(ChatMessageBulk reason) {
        this.reason = reason;
    }

    @Override
    public String getName() {
        return "PlayerKickEvent";
    }
}
