package io.github.mrblobman.crosschat.server.command.defaultcommands;

import io.github.mrblobman.crosschat.server.command.management.CommandHandle;
import io.github.mrblobman.crosschat.server.command.management.CommandHandler;
import io.github.mrblobman.crosschat.server.command.management.CommandManager;
import io.github.mrblobman.crosschat.server.command.management.CommandSender;
import io.github.mrblobman.crosschat.server.permission.DefaultPermissions;

/**
 * Created on 11/12/2015.
 */
public class HelpCommand implements CommandHandler {
    private CommandManager commands;

    public HelpCommand(CommandManager commands) {
        this.commands = commands;
    }

    @CommandHandle(command = {"help|cmdinfo"}, permission = DefaultPermissions.EMPTY_PERMISSION, description = "Get help about the usage and availability of commands.")
    private void help(CommandSender sender, String... subCommand) {
        this.commands.displayHelp(sender, subCommand);
    }
}
