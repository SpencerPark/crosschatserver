package io.github.mrblobman.crosschat.server.event.player;

import io.github.mrblobman.crosschat.server.model.Player;

/**
 * Called when a {@link Player} joins the server.
 */
public class PlayerJoinEvent extends PlayerEvent {

    public PlayerJoinEvent(Player player) {
        super(player);
    }

    @Override
    public String getName() {
        return "PlayerJoinEvent";
    }
}
