package io.github.mrblobman.crosschat.server.model;

import io.github.mrblobman.crosschat.auth.data.GameProfile;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.data.MessageScope;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.clientbound.MessagePacket;
import io.github.mrblobman.crosschat.server.Client;
import io.github.mrblobman.crosschat.server.CrossChatServer;
import io.github.mrblobman.crosschat.server.command.management.CommandSender;
import io.github.mrblobman.crosschat.server.event.EventException;
import io.github.mrblobman.crosschat.server.event.player.PlayerKickEvent;
import io.github.mrblobman.crosschat.server.permission.Permissible;
import io.github.mrblobman.crosschat.server.permission.PermissionAttachment;
import io.github.mrblobman.crosschat.server.permission.PermissionGroup;

import java.util.UUID;
import java.util.logging.Level;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class Player implements CommandSender, MessageRecipient, Permissible {
    private Client client;
    private GameProfile profile;
    private PermissionAttachment permissions;
    private ChatChannel currentChannel;
    private CrossChatServer server;

    protected Player(CrossChatServer server, Client client, PermissionAttachment permissions) {
        if (!client.isLoggedIn()) throw new IllegalArgumentException("Client is not logged in yet");
        this.server = server;
        this.client = client;
        this.profile = client.getGameProfile();
        server.getChannelManager().getDefaultChannel().add(this);
        this.currentChannel = server.getChannelManager().getDefaultChannel();
        this.permissions = permissions;
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    public String getName() {
        return profile.getName();
    }

    public UUID getUniqueId() {
        return profile.getId();
    }

    public void kick(ChatMessageBulk message) {
        PlayerKickEvent event = new PlayerKickEvent(this, message);
        try {
            server.getEventBus().boardBus(event);
        } catch (EventException e) {
            server.getLogger().log(Level.WARNING, "Could not handle "+event.getName()+".", e);
        }
        if (event.isCanceled()) return;
        client.disconnect(event.getReason());
    }

    @Override
    public boolean hasPermission(String permission) {
        return permissions.hasPermission(permission);
    }

    @Override
    public void givePermission(String permission) {
        this.permissions.givePermission(permission);
    }

    @Override
    public void removePermission(String permission) {
        this.permissions.removePermission(permission);
    }

    @Override
    public void joinPermissionGroup(PermissionGroup permissionGroup) {
        this.permissions.giveGroup(permissionGroup);
    }

    @Override
    public boolean isInPermissionGroup(String permissionGroupName) {
        return this.permissions.isInGroup(permissionGroupName);
    }

    @Override
    public void leavePermissionGroup(String group) {
        this.permissions.removeGroup(group);
    }

    public String listPermissions() {
        return this.permissions.toString();
    }

    @Override
    public void sendMessage(ChatMessageBulk message) {
        sendMessage("", MessageScope.GENERIC, message);
    }

    @Override
    public void sendMessage(String sender, MessageScope scope, ChatMessageBulk message) {
        client.sendPacket(new MessagePacket(sender, scope, message));
    }

    public ChatChannel getCurrentChannel() {
        return currentChannel;
    }

    public void switchChannels(ChatChannel newChannel) throws ChannelException {
        newChannel.add(this, false);
        currentChannel.remove(this);
        this.currentChannel = newChannel;
    }
}
