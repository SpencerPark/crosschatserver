package io.github.mrblobman.crosschat.network.pipeline.server;

import io.github.mrblobman.crosschat.network.ClientState;
import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.Protocol;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.handshake.serverbound.HandshakePacket;
import io.github.mrblobman.crosschat.network.pipeline.common.PacketReadException;
import io.github.mrblobman.crosschat.server.Client;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;

import java.util.List;

/**
 * Created by MrBlobman on 15-08-30.
 */
public class PacketHandler extends ByteToMessageCodec<Packet> {
    private Protocol serverProtocol;
    private Client client;

    public PacketHandler(Protocol serverProtocol, Client client) {
        this.serverProtocol = serverProtocol;
        this.client = client;
    }

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Packet packet, ByteBuf byteBuf) throws Exception {
        //System.out.println("Sending -> 0x"+Integer.toHexString(packet.getId())+" Class "+packet.getClass().getName());
        //if (packet instanceof KickPacket) System.out.println(((KickPacket) packet).getMessage());
        Buffer buffer = new Buffer(byteBuf);
        buffer.writeVarInt(packet.getId());
        packet.encode(buffer);
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> out) throws Exception {
        if (!client.isConnected()) return;
        Buffer buffer = new Buffer(byteBuf);

        int id = buffer.readVarInt();
        Class<? extends Packet> packetType = serverProtocol.getRegistry(client.getState()).getPacketById(id, true);
        if (packetType == null) {
            buffer.skipBytes(buffer.readableBytes());
            System.out.println(client.getState());
            throw new PacketReadException("Unknown packet id (0x"+Integer.toHexString(id)+"). Skipping...");
        }

        Packet packet = Packet.decode(buffer, packetType);
        out.add(packet);

        //Handle the handshake right away
        if (packet instanceof HandshakePacket) {
            HandshakePacket handshakePacket = (HandshakePacket) packet;
            client.setProtocolId(handshakePacket.getProtocolVersion());
            if (client.getProtocolId() > serverProtocol.getVersionId()) {
                client.disconnect("Outdated server! Server is still on "+serverProtocol.getVersion());
                return;
            } else if (client.getProtocolId() < serverProtocol.getVersionId()) {
                client.disconnect("Outdated client! Server is on version "+serverProtocol.getVersion());
                return;
            }
            if (handshakePacket.getIntent() == HandshakePacket.LOGIN) {
                client.setClientState(ClientState.LOGIN);
            } else if (handshakePacket.getIntent() == HandshakePacket.STATUS) {
                client.setClientState(ClientState.STATUS);
            }
        }
    }
}
