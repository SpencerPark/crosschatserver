package io.github.mrblobman.crosschat.server.database;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalNotification;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import io.github.mrblobman.crosschat.configuration.ConfigParser;
import io.github.mrblobman.crosschat.configuration.FileConfiguration;
import io.github.mrblobman.crosschat.server.permission.PermissionGroup;
import io.github.mrblobman.crosschat.server.permission.PermissionStore;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by MrBlobman on 10/09/2015.
 */
public class FlatFileDB implements Database {
    private boolean isClosed = false;
    private Logger logger;
    private File root;
    private File permGroupRoot;
    private File trash;
    private ConfigParser parser = new ConfigParser(false);
    private Cache<UUID, PlayerData> playerDataCache;

    public FlatFileDB(Logger logger, File root) throws IOException {
        this.logger = logger;
        this.root = root;
        if (!this.root.exists()) this.root.createNewFile();
        this.permGroupRoot = new File(root, "permissionGroups");
        if (!this.permGroupRoot.exists()) this.permGroupRoot.mkdirs();
        this.trash = new File(root, "trash");
        if (!this.trash.exists()) this.trash.mkdirs();
        playerDataCache = CacheBuilder.newBuilder()
                .expireAfterAccess(15, TimeUnit.MINUTES)
                .removalListener(
                        (RemovalNotification<UUID, PlayerData> onRemove) -> this.savePlayerData(onRemove.getKey(), onRemove.getValue())
                )
                .build();
        addUUIDSupport();
        addPermissionStoreSupport();
        addPermissionGroupSupport();
        //Preload all permission groups into mem
        File[] files = this.permGroupRoot.listFiles();
        if (files == null) return;
        for (File permGroupFile : files)
            ConfigParser.parse(this.parser, permGroupFile, PermissionGroup.class);
    }

    private void addUUIDSupport() {
        parser.addCustomDeserializer(UUID.class, (json, typeOfT, context) -> {
            if (!json.isJsonPrimitive()) throw new JsonParseException("Element is not a primitive.");
            JsonPrimitive uuidPrim = json.getAsJsonPrimitive();
            if (!uuidPrim.isString()) throw new JsonParseException("Element is not a String");
            return UUID.fromString(uuidPrim.getAsString());
        });
        parser.addCustomSerializer(UUID.class, (uuid, typeOfT, context) -> new JsonPrimitive(uuid.toString()));
    }

    private void addPermissionStoreSupport() {
        parser.addCustomDeserializer(PermissionStore.class, (json, typeOfT, context) -> {
            if (!json.isJsonArray()) throw new JsonParseException("Element is not an array");
            return new PermissionStore(json.getAsJsonArray());
        });
        parser.addCustomSerializer(PermissionStore.class,
                (PermissionStore perms, Type typeOfT, JsonSerializationContext context) -> perms.serialize());
    }

    private void addPermissionGroupSupport() {
        parser.addCustomDeserializer(PermissionGroup.class, (json, typeOfT, context) -> {
            if (!json.isJsonObject()) throw new JsonParseException("Element is not an object.");
            return PermissionGroup.createGroup(json.getAsJsonObject());
        });
        parser.addCustomSerializer(PermissionGroup.class,
                (PermissionGroup permGroup, Type typeOfT, JsonSerializationContext context) -> permGroup.serialize());
    }

    @Override
    public PlayerData loadPlayerData(UUID id) {
        if (this.isClosed) throw new IllegalStateException("This database is closed");
        PlayerData data = this.playerDataCache.getIfPresent(id);
        if (data != null) return data;
        File playerFile = new File(this.root, id.toString()+".json");
        if (!playerFile.exists()) {
            try {
                playerFile.createNewFile();
            } catch (IOException e) {
                logger.info("");
            }
            data = PlayerData.blankData();
        } else {
            FileConfiguration<PlayerData> dataConfig = new FileConfiguration<>(PlayerData.class, playerFile, this.parser);
            try {
                data = dataConfig.getData();
                if (data == null) data = PlayerData.blankData();
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Could not load player data for "+id+". Using blank data.", e);
                data = PlayerData.blankData();
            }
        }
        this.playerDataCache.put(id, data);
        return data;
    }

    @Override
    public void savePlayerData(UUID id, PlayerData data) {
        if (this.isClosed) throw new IllegalStateException("This database is already closed");
        File playerFile = new File(this.root, id.toString()+".json");
        if (!playerFile.exists()) {
            try {
                playerFile.createNewFile();
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Could not save player data for "+id+".", e);
                return;
            }
        }
        FileConfiguration<PlayerData> dataConfig = new FileConfiguration<>(PlayerData.class, playerFile, this.parser);
        dataConfig.setData(data);
        try {
            dataConfig.save();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not save player data for "+id+".", e);
        }
    }

    @Override
    public void deletePlayerData(UUID id) {
        if (this.isClosed) throw new IllegalStateException("This database is already closed");
        File playerFile = new File(this.root, id.toString()+".json");
        if (!playerFile.exists()) return; //Already gone
        //Move to the trash
        playerFile.renameTo(new File(this.trash, id.toString()+".json"));
    }

    @Override
    public PermissionGroup loadPermissionGroup(String groupName) {
        File groupDataFile = new File(this.permGroupRoot, groupName+".json");
        if (!groupDataFile.exists()) {
            try {
                groupDataFile.createNewFile();
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Could not load permission group named: " + groupName + ". Using blank group.", e);
            }
            return PermissionGroup.createBlankGroup(groupName);
        }
        FileConfiguration<PermissionGroup> permissionGroup = new FileConfiguration<>(PermissionGroup.class, groupDataFile, this.parser);
        try {
            return permissionGroup.getData();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not load permission group named: " + groupName + ". Using blank group.", e);
            return PermissionGroup.createBlankGroup(groupName);
        }
    }

    @Override
    public void savePermissionGroup(PermissionGroup group) {
        File groupDataFile = new File(this.permGroupRoot, group.getGroupName()+".json");
        if (!groupDataFile.exists()) {
            try {
                groupDataFile.createNewFile();
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Could not save permission group named: " + group.getGroupName(), e);
                return;
            }
        }
        FileConfiguration<PermissionGroup> permissionGroup = new FileConfiguration<>(PermissionGroup.class, groupDataFile, this.parser);
        try {
            permissionGroup.setData(group);
            permissionGroup.save();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not save permission group named: " + group.getGroupName(), e);
        }
    }

    @Override
    public void deletePermissionGroup(String groupName) {
        File groupDataFile = new File(this.permGroupRoot, groupName+".json");
        if (!groupDataFile.exists()) return; //Already non-existant
        //Move it to the trash
        groupDataFile.renameTo(new File(this.trash, groupName+".json"));
    }

    @Override
    public void close() throws IOException {
        if (this.isClosed) return;
        this.playerDataCache.asMap().forEach(this::savePlayerData);
        this.playerDataCache.invalidateAll();
        this.isClosed = true;
    }
}
