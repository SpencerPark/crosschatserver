package io.github.mrblobman.crosschat.server.permission;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class PermissionGroup {
    private static Map<String, PermissionGroup> loadedGroups = new ConcurrentHashMap<>();

    public static PermissionGroup createGroup(String groupName, PermissionStore perms) {
        if (loadedGroups.containsKey(groupName.toLowerCase())) {
            //A group already exists. We can't have two groups with the same name so
            //merge the groups old perms with these new ones.
            PermissionGroup group = loadedGroups.get(groupName.toLowerCase());
            group.perms.merge(perms);
            return group;
        } else {
            PermissionGroup group = new PermissionGroup(groupName, perms);
            loadedGroups.put(groupName.toLowerCase(), group);
            return group;
        }
    }

    public static PermissionGroup createGroup(JsonObject serialized) throws JsonParseException {
        String groupName = serialized.get("name").getAsString();
        PermissionStore perms = new PermissionStore(serialized.get("perms").getAsJsonArray());
        return createGroup(groupName, perms);
    }

    public static PermissionGroup createBlankGroup(String name) {
        return createGroup(name, new PermissionStore());
    }

    public static PermissionGroup getGroup(String groupName) {
        return loadedGroups.get(groupName.toLowerCase());
    }

    private String groupName;
    private PermissionStore perms;

    private PermissionGroup(String groupName, PermissionStore perms) {
        this.groupName = groupName;
        this.perms = perms;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void addPermission(String permission) {
        perms.addPermission(permission);
    }

    public boolean hasPermission(String permission) {
        return perms.hasPermission(permission);
    }

    public JsonObject serialize() {
        JsonObject serialized = new JsonObject();
        serialized.addProperty("name", this.groupName);
        serialized.add("perms", this.perms.serialize());
        return serialized;
    }

    @Override
    public String toString() {
        return groupName + ": \n" + perms.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermissionGroup that = (PermissionGroup) o;

        return groupName.equals(that.groupName);

    }

    @Override
    public int hashCode() {
        return groupName.hashCode();
    }
}
