package io.github.mrblobman.crosschat.server.event;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class EventBus {
    private Map<Class<? extends Event>, HandlerList<? extends Event>> handlers = new ConcurrentHashMap<>();

    public void registerRoute(Class<? extends Event> eventClass) {
        handlers.put(eventClass, new HandlerList<>(eventClass));
    }

    public void unregisterRoute(Class<? extends Event> eventClass) {
        handlers.remove(eventClass);
    }

    public void registerStop(Class<? extends Event> route, EventBusStop busStop) {
        if (!handlers.containsKey(route)) {
            registerRoute(route);
        }
        HandlerList<? extends Event> handlerList = handlers.get(route);
        handlerList.register(busStop);
    }

    public void unregisterStop(Class<? extends Event> route, EventBusStop busStop) {
        if (!handlers.containsKey(route)) return;
        HandlerList<? extends Event> handlerList = handlers.get(route);
        handlerList.unregister(busStop);
    }

    public void boardBus(Event event) throws EventException {
        HandlerList<? extends Event> handlerList = handlers.get(event.getClass());
        if (handlerList != null) handlerList.handle(event);
    }
}
