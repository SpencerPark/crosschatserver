package io.github.mrblobman.crosschat.server.event.message;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.model.ChatChannel;
import io.github.mrblobman.crosschat.server.model.Player;

/**
 * Created by MrBlobman on 15-09-04.
 */
public class PlayerChannelMessageEvent extends PlayerMessageEvent {
    private ChatChannel recipient;

    public PlayerChannelMessageEvent(Player sender, ChatChannel channel, ChatMessageBulk message) {
        super(sender, message);
        this.recipient = channel;
    }

    public ChatChannel getRecipient() {
        return recipient;
    }

    public void setRecipient(ChatChannel recipient) {
        this.recipient = recipient;
    }

    @Override
    public String getName() {
        return "PlayerChannelMessageEvent";
    }
}
