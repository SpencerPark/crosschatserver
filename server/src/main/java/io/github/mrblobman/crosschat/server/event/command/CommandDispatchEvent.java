package io.github.mrblobman.crosschat.server.event.command;

import io.github.mrblobman.crosschat.server.command.management.CommandSender;

import java.util.List;

/**
 * Created by MrBlobman on 9/12/2015.
 */
public class CommandDispatchEvent extends CommandEvent {

    public CommandDispatchEvent(String command, List<String> args, CommandSender sender) {
        super(command, args, sender);
    }

    /**
     * @return a <b>mutable</b> copy of the arguments.
     */
    @Override
    public List<String> getArgs() {
        return args;
    }

    public void setCommand(String command) {
        if (command.startsWith("/")) command = command.substring(1);
        this.command = command;
    }

    public void setSender(CommandSender sender) {
        this.sender = sender;
    }

    /**
     * @return a user-friendly name for this event
     */
    @Override
    public String getName() {
        return "CommandDispatchEvent";
    }
}
