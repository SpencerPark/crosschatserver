package io.github.mrblobman.crosschat.server.event.player;

import io.github.mrblobman.crosschat.server.event.Event;
import io.github.mrblobman.crosschat.server.model.Player;

/**
 * Created by MrBlobman on 09/09/2015.
 */
public abstract class PlayerEvent extends Event {
    private Player player;

    public PlayerEvent(Player player) {
        this.player = player;
    }

    public Player getPlayer () {
        return this.player;
    }
}
