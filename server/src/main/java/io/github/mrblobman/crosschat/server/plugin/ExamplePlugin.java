package io.github.mrblobman.crosschat.server.plugin;

import io.github.mrblobman.crosschat.server.CrossChatServer;
import io.github.mrblobman.crosschat.server.event.EventBusStop;
import io.github.mrblobman.crosschat.server.event.EventHandler;
import io.github.mrblobman.crosschat.server.event.message.PlayerChannelMessageEvent;

/**
 * Created by MrBlobman on 15-09-07.
 */
public class ExamplePlugin extends Plugin {

    @Override
    public void onLoad() {
        this.getLogger().info("[ExamplePlugin] Loaded.");
    }

    @Override
    public void onEnable() {
        this.getLogger().info("[ExamplePlugin] Enabled.");
        this.registerEvents(new EventBusStop() {
            @EventHandler
            public void onMessageSend(PlayerChannelMessageEvent event) {
                getLogger().info("I'm listening to an event! (" + event.getName() + ")");
            }

            public void badMethod(PlayerChannelMessageEvent event) {
                //This method doesn't have the @EventHandler notation so it won't be called
            }
        });
        CrossChatServer server = this.getPluginManager().getServer();
    }

    @Override
    public void onDisable() {
        this.getLogger().info("[ExamplePlugin] Disabled.");
    }
}
