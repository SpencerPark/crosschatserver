package io.github.mrblobman.crosschat.server;

import io.github.mrblobman.crosschat.server.event.EventBus;
import io.github.mrblobman.crosschat.server.model.ChannelManager;
import io.github.mrblobman.crosschat.server.model.ChatChannel;

/**
 * Created by MrBlobman on 15-09-07.
 */
public class ChannelsConfig {
    private class DefaultChannelConfig {
        int talkPower = -1;
        String name;
        String description = "";
    }
    private class ChannelConfig {
        String displayName;
        String description;
        int talkPower;
        String permissionToJoin;
        int maxUsers;
    }
    DefaultChannelConfig defaultChannel;
    ChannelConfig[] channels;

    public ChannelManager create(EventBus eventBus) {
        ChannelManager manager = new ChannelManager(defaultChannel.name, defaultChannel.description, eventBus);
        if (defaultChannel.talkPower > 0) {
            manager.getDefaultChannel().setNeededTalkPower(defaultChannel.talkPower);
        }
        for (ChannelConfig channelConfig : channels) {
            ChatChannel channel = manager.create(channelConfig.displayName);
            if (channelConfig.description != null) channel.setDescription(channelConfig.description);
            if (channelConfig.talkPower > 0) channel.setNeededTalkPower(channelConfig.talkPower);
            if (channelConfig.permissionToJoin != null) channel.setPermissionToJoin(channelConfig.permissionToJoin);
            if (channelConfig.maxUsers > 0) channel.setMaxUsers(channelConfig.maxUsers);
        }
        return manager;
    }
}
