package io.github.mrblobman.crosschat.server;

import io.github.mrblobman.crosschat.configuration.ConfigParser;
import io.github.mrblobman.crosschat.configuration.FileConfiguration;
import io.github.mrblobman.crosschat.network.data.ChatColor;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.data.MessageScope;
import io.github.mrblobman.crosschat.server.command.defaultcommands.*;
import io.github.mrblobman.crosschat.server.command.management.CommandManager;
import io.github.mrblobman.crosschat.server.database.Database;
import io.github.mrblobman.crosschat.server.database.FlatFileDB;
import io.github.mrblobman.crosschat.server.event.EventBus;
import io.github.mrblobman.crosschat.server.event.EventException;
import io.github.mrblobman.crosschat.server.event.channel.ChannelChangeEvent;
import io.github.mrblobman.crosschat.server.event.channel.ChannelCreateEvent;
import io.github.mrblobman.crosschat.server.event.channel.ChannelDestroyedEvent;
import io.github.mrblobman.crosschat.server.event.command.CommandDispatchEvent;
import io.github.mrblobman.crosschat.server.event.command.CommandPermissionDeniedEvent;
import io.github.mrblobman.crosschat.server.event.message.PlayerChannelMessageEvent;
import io.github.mrblobman.crosschat.server.event.message.PlayerPrivateMessageEvent;
import io.github.mrblobman.crosschat.server.event.player.PlayerJoinEvent;
import io.github.mrblobman.crosschat.server.event.player.PlayerKickEvent;
import io.github.mrblobman.crosschat.server.event.player.PlayerLoginEvent;
import io.github.mrblobman.crosschat.server.event.player.PlayerQuitEvent;
import io.github.mrblobman.crosschat.server.model.ChannelManager;
import io.github.mrblobman.crosschat.server.model.Player;
import io.github.mrblobman.crosschat.server.model.PlayerRegistry;
import io.github.mrblobman.crosschat.server.plugin.InvalidPluginException;
import io.github.mrblobman.crosschat.server.plugin.PluginLoadExcetpion;
import io.github.mrblobman.crosschat.server.plugin.PluginManager;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

/**
 * Created by MrBlobman on 15-09-04.
 */
public class CrossChatServer {
    private boolean isRunning = true;
    private Logger logger;
    private EventBus eventBus;
    private ChannelManager channelManager;
    private NetworkServer networkServer;
    private PluginManager pluginManager;
    private PlayerRegistry onlinePlayers;
    private File pluginFolder;
    private ConfigParser configParser;
    private Database db;
    private CommandManager commandManager;

    public CrossChatServer(ServerConfig serverConfig) throws IOException, PluginLoadExcetpion, InvalidPluginException, NoSuchAlgorithmException {
        this.configParser = new ConfigParser(true);
        initEventBus();
        initLogger();
        initChannels();
        this.networkServer = new NetworkServer(serverConfig.getPort(), eventBus, logger, this);
        this.pluginManager = new PluginManager(this.configParser, this.eventBus, this);
        this.pluginFolder = new File("plugins");
        if (!pluginFolder.exists()) pluginFolder.mkdirs();
        File dataRoot = new File("data");
        if (!dataRoot.exists()) dataRoot.mkdirs();
        this.db = new FlatFileDB(this.logger, dataRoot);
        this.onlinePlayers = new PlayerRegistry(this);
        initCommandManager();
        pluginManager.loadAll();
    }

    private void initEventBus() {
        this.eventBus = new EventBus();
        this.eventBus.registerRoute(ChannelChangeEvent.class);
        this.eventBus.registerRoute(ChannelCreateEvent.class);
        this.eventBus.registerRoute(ChannelDestroyedEvent.class);
        this.eventBus.registerRoute(CommandDispatchEvent.class);
        this.eventBus.registerRoute(CommandPermissionDeniedEvent.class);
        this.eventBus.registerRoute(PlayerChannelMessageEvent.class);
        this.eventBus.registerRoute(PlayerPrivateMessageEvent.class);
        this.eventBus.registerRoute(PlayerJoinEvent.class);
        this.eventBus.registerRoute(PlayerKickEvent.class);
        this.eventBus.registerRoute(PlayerLoginEvent.class);
        this.eventBus.registerRoute(PlayerQuitEvent.class);
    }

    private void initLogger() throws IOException {
        this.logger = Logger.getLogger("CrossChat");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        File logFolder = new File("logs");
        if (!logFolder.exists()) logFolder.mkdirs();
        this.logger.addHandler(new FileHandler("logs/CrossChatServerLog-" + format.format(new Date()) + "-v%u.log"));
        this.logger.addHandler(new StreamHandler(System.out, new SimpleFormatter()));
    }

    private void initChannels() throws IOException {
        FileConfiguration<ChannelsConfig> configuration = new FileConfiguration<>(ChannelsConfig.class, "channels.json", new File("channels.json"), this.configParser);
        ChannelsConfig config = configuration.getData();
        this.channelManager = config.create(eventBus);
        this.eventBus.registerStop(PlayerQuitEvent.class, this.channelManager);
    }

    private void initCommandManager() {
        this.commandManager = new CommandManager(this);
        this.commandManager.registerHandler(new BroadcastCommand(this.channelManager));
        this.commandManager.registerHandler(new ChannelAdminCommands(this.channelManager, this.onlinePlayers));
        this.commandManager.registerHandler(new ChannelCommands(this.channelManager, this.onlinePlayers));
        this.commandManager.registerHandler(new PermissionCommands(this.db, this.onlinePlayers));
        this.commandManager.registerHandler(new HelpCommand(commandManager));
        this.commandManager.registerHandler(new AdminCommands(this.onlinePlayers));
    }

    public EventBus getEventBus() {
        return this.eventBus;
    }

    public PluginManager getPluginManager() {
        return this.pluginManager;
    }

    public Logger getLogger() {
        return logger;
    }

    public ChannelManager getChannelManager() {
        return channelManager;
    }

    public ConfigParser getConfigParser() {
        return configParser;
    }

    public PlayerRegistry getOnlinePlayers() {
        return this.onlinePlayers;
    }

    public Database getDataBase() {
        return this.db;
    }

    public Player join(Client client) {
        PlayerJoinEvent event = new PlayerJoinEvent(this.onlinePlayers.register(client));
        try {
            this.eventBus.boardBus(event);
        } catch (EventException e) {
            this.getLogger().severe("Error handling join event for "+client.getGameProfile().getName()+". REASON: "+e.getLocalizedMessage());
        }
        return event.getPlayer();
    }

    public void stop() {
        if (!isRunning) return;
        this.logger.info("Stopping...");
        this.networkServer.stop();
        try {
            this.db.close();
        } catch (IOException e) {
            this.logger.severe("Could not properly close db. REASON: " + e.getLocalizedMessage());
        }
    }

    public void sendPM(Player from, String to, ChatMessageBulk message) {
        Player recipient = this.onlinePlayers.getByName(to, false);
        if (recipient == null) {
            from.sendMessage(new ChatMessageBulk("Unknown player.").setColor(ChatColor.RED));
            return;
        }
        recipient.sendMessage(from.getName(), MessageScope.PRIVATE, message);
    }

    public CommandManager getCommandManager() {
        return this.commandManager;
    }
}
