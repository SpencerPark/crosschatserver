package io.github.mrblobman.crosschat.server.event.channel;

import io.github.mrblobman.crosschat.server.model.ChatChannel;

/**
 * Created by MrBlobman on 15-09-04.
 */
public class ChannelDestroyedEvent extends ChannelEvent {
    private ChatChannel userDestination;

    public ChannelDestroyedEvent(ChatChannel channel, ChatChannel userDestination) {
        super(channel);
        this.userDestination = userDestination;
    }

    public ChatChannel getUserDestination() {
        return userDestination;
    }

    public void setUserDestination(ChatChannel userDestination) {
        this.userDestination = userDestination;
    }

    @Override
    public String getName() {
        return "ChannelDestroyEvent";
    }
}
