package io.github.mrblobman.crosschat.server.command.management;

/**
 * Used to link all command handlers. The interface is
 * empty.
 */
public interface CommandHandler { }
