package io.github.mrblobman.crosschat.server.event;

import java.lang.reflect.Method;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class HandlerList<T extends Event> {
    private Map<HandlePriority, Set<Handler<T>>> handlers = new EnumMap<>(HandlePriority.class);
    private Class<T> type;

    public HandlerList(Class<T> type) {
        this.type = type;
        for (HandlePriority priority : HandlePriority.values()) {
            handlers.put(priority, new HashSet<>());
        }
    }

    public void register(Object handler) {
        Class<?> handleClass = handler.getClass();
        for (Method method : handleClass.getMethods()) {
            EventHandler eventHandlerAnnotation = method.getAnnotation(EventHandler.class);
            if (eventHandlerAnnotation == null) continue;
            if (method.getParameterCount() != 1) continue;
            if (type.equals(method.getParameterTypes()[0])) {
                //A little hack to keep this class generic
                handlers.get(eventHandlerAnnotation.priority()).add(new Handler<>(handler, method));
            }
        }
    }

    public void unregister(Object handler) {
        for (Set<Handler<T>> handles : handlers.values()) {
            handles.removeIf(handle -> handle.getHandler() == handler);
        }
    }

    public void handle(Event event) throws EventException {
        for (HandlePriority priority : HandlePriority.values()) {
            for (Handler<T> handle : handlers.get(priority)) {
                try {
                    handle.handle(event);
                } catch (Exception e) {
                    throw new EventException(e);
                }
            }
        }
    }
}
