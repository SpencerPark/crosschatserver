package io.github.mrblobman.crosschat.server.plugin;

import io.github.mrblobman.crosschat.server.plugin.loading.LoadConstraints;

/**
 * Created on 15-09-07.
 */
public class PluginConfig {
    public String name;
    public String version;
    public String main;
    public LoadConstraints loadConstraints;
}
