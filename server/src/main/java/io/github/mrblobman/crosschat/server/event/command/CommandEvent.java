package io.github.mrblobman.crosschat.server.event.command;

import com.google.common.collect.ImmutableList;
import io.github.mrblobman.crosschat.server.command.management.CommandSender;
import io.github.mrblobman.crosschat.server.event.CancelableEvent;

import java.util.List;

/**
 * Created by MrBlobman on 9/12/2015.
 */
public abstract class CommandEvent extends CancelableEvent {
    protected String command;
    protected List<String> args;
    protected CommandSender sender;

    public CommandEvent(String command, List<String> args, CommandSender sender) {
        this.command = command;
        this.args = args;
        this.sender = sender;
    }

    public String getCommand() {
        return command;
    }

    /**
     * @return an <b>immutable</b> copy of the arguments.
     */
    public List<String> getArgs() {
        return ImmutableList.copyOf(args);
    }

    public CommandSender getSender() {
        return sender;
    }

    public void setSender(CommandSender sender) {
        this.sender = sender;
    }
}
