package io.github.mrblobman.crosschat.server.command.management;

import io.github.mrblobman.crosschat.network.data.ChatColor;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.command.management.args.ArgDescription;
import io.github.mrblobman.crosschat.server.command.management.args.Argument;
import io.github.mrblobman.crosschat.server.command.management.args.ArgumentFormatter;
import io.github.mrblobman.crosschat.server.event.EventBus;
import io.github.mrblobman.crosschat.server.event.EventException;
import io.github.mrblobman.crosschat.server.event.command.CommandPermissionDeniedEvent;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class CommandRegistry {
	private Map<String, SubCommand> baseCommands = new HashMap<>();
	private Map<SubCommand, HandleInvoker> invokers = new HashMap<>();
    private Logger logger;
    private EventBus eventBus;

	CommandRegistry(Logger logger, EventBus eventBus) {
        this.logger = logger;
        this.eventBus = eventBus;
    }
	
	public void register(CommandHandler commandHandler) {
		MethodLoop:
			for (Method method : commandHandler.getClass().getDeclaredMethods()) {
				CommandHandle handlerAnnotation = method.getAnnotation(CommandHandle.class);
				//Move on, this method isnt annotated
				if (handlerAnnotation == null) continue;
				//Check that min requirements are met
				if (method.getParameterCount() < 1) {
					logger.log(Level.WARNING, "Cannot register method " + method.getName() + ". Not enough parameters. Requires first param as sender.");
					continue;
				}
				Parameter[] methodParams = method.getParameters();
				//Build parsing data arrays
                Argument<?>[] arguments = new Argument[methodParams.length-1];
				for (int i = 1; i < methodParams.length-(method.isVarArgs() ? 1 : 0); i++) {
					ArgumentFormatter<?> formatter;
                    Class<?> paramType = methodParams[i].getType();
					if (String.class.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.STRING;
					} else if (Integer.class.isAssignableFrom(paramType) || Integer.TYPE.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.INTEGER;
					} else if (Double.class.isAssignableFrom(paramType) || Double.TYPE.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.DOUBLE;
					} else if (Long.class.isAssignableFrom(paramType) || Long.TYPE.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.LONG;
					} else if (Short.class.isAssignableFrom(paramType) || Short.TYPE.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.SHORT;
					} else if (Float.class.isAssignableFrom(paramType) || Float.TYPE.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.FLOAT;
					} else if (Boolean.class.isAssignableFrom(paramType) || Boolean.TYPE.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.BOOLEAN;
					} else {
						logger.log(Level.WARNING, "Cannot register method "+method.getName()+". Unknown parameter parse type ("+paramType.getName()+"). Accepted types are String, Integer, Long, Short, Double, Float and org.bukkit.Color.");
						continue MethodLoop;
					}
                    ArgDescription desc = methodParams[i].getAnnotation(ArgDescription.class);
                    String name;
                    if (desc != null && !desc.name().equals("")) {
                        name = desc.name();
                    } else {
                        name = methodParams[i].isNamePresent() ? methodParams[i].getName() : "arg" + (i - 1);
                    }
                    if (desc != null && desc.description().length == 0) {
                        arguments[i - 1] = new Argument<>(formatter, name, desc.description(), false);
                    } else {
                        arguments[i - 1] = new Argument<>(formatter, name, false);
                    }
				}
				if (method.isVarArgs()) {
                    int lastIndex = methodParams.length - 1;
                    ArgumentFormatter<?> formatter;
                    //We need to handle the last arg differently because it is a var arg
					Class<?> paramType = methodParams[lastIndex].getType();
					if (String[].class.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.STRING;
					} else if (Integer[].class.isAssignableFrom(paramType) || int[].class.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.INTEGER;
					} else if (Double[].class.isAssignableFrom(paramType) || double[].class.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.DOUBLE;
					} else if (Long[].class.isAssignableFrom(paramType) || long[].class.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.LONG;
					} else if (Short[].class.isAssignableFrom(paramType) || short[].class.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.SHORT;
					} else if (Float[].class.isAssignableFrom(paramType) || float[].class.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.FLOAT;
					} else if (Boolean[].class.isAssignableFrom(paramType) || boolean[].class.isAssignableFrom(paramType)) {
						formatter = ArgumentFormatter.BOOLEAN;
					} else {
						logger.log(Level.WARNING, "Cannot register method "+method.getName()+". Unknown parameter parse type ("+paramType.getName()+"). Accepted types are String, Integer, Long, Short, Double, Float and org.bukkit.Color.");
						continue;
					}
                    ArgDescription desc = methodParams[lastIndex].getAnnotation(ArgDescription.class);
                    String name;
                    if (desc != null && !desc.name().equals("")) {
                        name = desc.name();
                    } else {
                        name = methodParams[lastIndex].isNamePresent() ? methodParams[lastIndex].getName() : "arg" + (lastIndex);
                    }
                    if (desc != null && desc.description().length == 0) {
                        arguments[lastIndex-1] = new Argument<>(formatter, name, desc.description(), true);
                    } else {
                        arguments[lastIndex-1] = new Argument<>(formatter, name, true);
                    }
				}
				//Verify the sender type is valid
				Class<?> senderType = methodParams[0].getType();
				if (!CommandSender.class.isAssignableFrom(senderType)) {
					logger.log(Level.WARNING, "Cannot register method " + method.getName() + ". Invalid sender type " + senderType.getSimpleName() + ". Must be accessible from org.bukkit.CommandSender.");
					continue;
				}
				//Register the sub command
				SubCommand cmd = addSubCommand(handlerAnnotation.command(), handlerAnnotation.permission());
				if (cmd == null) {
					logger.log(Level.WARNING, "Cannot register method " + method.getName() + ". Invalid sub command.");
					continue;
				}
				//-1 for first param sender type
				int minArgs = method.getParameterCount()-1;
				//Last arg is not required
				if (method.isVarArgs()) minArgs--;
				//Finally create the invoker
				this.invokers.put(cmd, new HandleInvoker(cmd, handlerAnnotation.description(), commandHandler, method, senderType, arguments, minArgs));
				logger.log(Level.INFO, "Successfully registered " + method.getName() + " in " + commandHandler.getClass().getSimpleName() + " for " + cmd.toString());
			}
	}

	protected SubCommand getSubCommand(String[] command) {
		if (command == null || command.length < 1) return null;
		SubCommand cmd = this.getBaseCommand(command[0]);
		if (cmd == null) return null;
		for (int i = 1; i < command.length; i++) {
			cmd = cmd.getSubCommand(command[i]);
			if (cmd == null) return null;
		}
		return cmd;
	}

    private SubCommand getBaseCommand(String baseCommand) {
        for (String baseAlias : baseCommand.split("\\|")) {
            if (this.baseCommands.containsKey(baseAlias.toLowerCase())) {
                return this.baseCommands.get(baseAlias.toLowerCase());
            }
        }
        return null;
    }

	protected SubCommand addSubCommand(String[] command, String permission) {
		if (command == null || command.length < 1) return null;
		SubCommand superCmd = addBaseCommand(command[0], permission);
		for (int i = 1; i < command.length; i++) {
			String[] subCommandAliases = command[i].split("\\|");
			SubCommand subCmd;
			if (subCommandAliases.length > 1) {
				subCmd = new SubCommand(subCommandAliases[0], Arrays.copyOfRange(subCommandAliases, 1, subCommandAliases.length), permission, superCmd);
			} else {
				subCmd = new SubCommand(subCommandAliases[0], new String[0], permission, superCmd);
			}
			subCmd.addPermission(permission);
			superCmd.addSubCommand(subCmd);
			superCmd = subCmd;
		}
		return superCmd;
	}

    private SubCommand addBaseCommand(String baseCommand, String permission) {
        String[] baseAliases = baseCommand.split("\\|");
        SubCommand base = getBaseCommand(baseAliases[0]);
        if (base != null) {
            base.addPermission(permission);
            for (int i = 1; i < baseAliases.length; i++)
                if (!base.getAliases().contains(baseAliases[i]))
                    base.getAliases().add(baseAliases[i]);
            return base;
        }
        if (baseAliases.length > 1) {
            base = new SubCommand(baseAliases[0], Arrays.copyOfRange(baseAliases, 1, baseAliases.length), permission, null);
        } else {
            base = new SubCommand(baseAliases[0], new String[0], permission, null);
        }
        for (String alias : baseAliases)
            this.baseCommands.put(alias.toLowerCase(), base);
        return base;
    }

	/**
	 * Usage designed for tab complete.
	 * @param enteredCommand the partial command entered
	 * @return a List containing the possible sub commands that may follow, will never return null
	 */
	public List<String> getPossibleSubCommands(String[] enteredCommand) {
		SubCommand sub = this.getSubCommand(enteredCommand);
		if (sub == null) {
			//Try to partially fix the last arg
			sub = this.getSubCommand(Arrays.copyOfRange(enteredCommand, 0, enteredCommand.length-1));
			if (sub == null) {
				//Nope they are lost
				return new ArrayList<>();
			}
            return sub.getSubCommands().stream()
                    .filter(possibleArg -> possibleArg.startsWith(enteredCommand[enteredCommand.length - 1]))
                    .collect(Collectors.toList());
		} else {
			return sub.getSubCommands();
		}
	}
	
	/**
	 * 
	 * @param sender
	 * @param command
	 * @return true iff the base command is registered with this registry and an attempt to execute was preformed
	 * @throws Exception
	 */
	protected boolean handleCommand(CommandSender sender, String[] command) throws CommandException {
		if (command == null || command.length < 1) throw new IllegalArgumentException("command was empty");
		SubCommand cmd = this.getBaseCommand(command[0]);
		if (cmd == null) {
			return false;
		}
		SubCommand next;
		int i;
		for (i = 1; i < command.length; i++) {
			next = cmd.getSubCommand(command[i]);
			if (next == null) break; //We went as far as we could go
			else cmd = next;
		}
		if (!cmd.canExecute(sender)) {
            String commandName = command[0];
            List<String> args = Arrays.asList(command).subList(1, command.length);
            CommandPermissionDeniedEvent event = new CommandPermissionDeniedEvent(
                    commandName,
                    args,
                    sender,
                    new ChatMessageBulk("You do not have permission to execute "+cmd.toString()+".").setColor(ChatColor.RED),
                    true
            );
            try {
                this.eventBus.boardBus(event);
            } catch (EventException e) {
                this.logger.log(Level.SEVERE, "Error handling "+event.getName()+" for "+sender.getName()+". Command permission will be treated as default (denied due to lack of permission).", e);
                sender.sendMessage(new ChatMessageBulk("You do not have permission to execute "+cmd.toString()+".").setColor(ChatColor.RED));
                return true;
            }
            if (!event.isCanceled()) {
                if (event.sendDeniedMessage()) sender.sendMessage(event.getDeniedMessage());
                return true;
            }
            //Continue execution, the permission denial was cancelled so the sender is now allowed to execute the command
        }
		//Invoke the command
		HandleInvoker invoker = this.invokers.get(cmd);
		if (invoker == null) return false;
		//i is index of first arg
        try {
            invoker.invoke(sender, i < command.length ? Arrays.copyOfRange(command, i, command.length) : new String[0]);
        } catch (Exception e) {
            throw new CommandException(e);
        }
        return true;
	}
	
	public void displayHelp(CommandSender sender, String[] cmdGiven) {
        if (cmdGiven.length > 0) {
            SubCommand subCommand = getSubCommand(cmdGiven);
            if (subCommand == null) {
                sender.sendMessage(new ChatMessageBulk("No commands match the query.").setColor(ChatColor.RED));
                return;
            }
            String commandString = subCommand.toString();
            boolean[] sentSomething = {false};
            this.invokers.forEach((cmd, invoker) -> {
                if (cmd.toString().startsWith(commandString) && cmd.canExecute(sender)) {
                    invoker.sendDescription(sender);
                    sentSomething[0] = true;
                }
            });
            if (!sentSomething[0])
                sender.sendMessage(new ChatMessageBulk("No commands you are allowed to execute match the query.").setColor(ChatColor.RED));
        } else {
            boolean[] sentSomething = {false};
            this.invokers.forEach((cmd, invoker) -> {
                if (cmd.canExecute(sender))
                    invoker.sendDescription(sender);
                sentSomething[0] = true;
            });
            if (!sentSomething[0])
                sender.sendMessage(new ChatMessageBulk("No commands you are allowed to execute match the query.").setColor(ChatColor.RED));
        }
	}


}
