package io.github.mrblobman.crosschat.server.plugin;

import io.github.mrblobman.crosschat.server.event.EventBusStop;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created on 15-09-05.
 */
public abstract class Plugin {
    private static Map<Class<? extends Plugin>, Plugin> loadedPlugins = new HashMap<>();

    public static Plugin getInstance(Class<? extends Plugin> plugin) {
        return loadedPlugins.get(plugin);
    }

    private String name;
    private String version;
    private PluginManager pluginManager;
    private boolean isEnabled = false;

    protected Plugin() {}

    private void init(Class<? extends Plugin> pluginClass, String name, String version, PluginManager manager) throws InstantiationException {
        if (loadedPlugins.containsKey(pluginClass)) throw new InstantiationException("Plugin "+pluginClass.getCanonicalName()+" already instantiated.");
        loadedPlugins.put(pluginClass, this);
        this.name = name;
        this.version = version;
        this.pluginManager = manager;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public PluginManager getPluginManager() {
        return this.pluginManager;
    }

    public abstract void onLoad();

    public abstract void onEnable();

    public abstract void onDisable();

    protected final void enable() {
        if (this.isEnabled) return;
        this.isEnabled = true;
        this.onEnable();
    }

    protected final void disable() {
        if (!this.isEnabled) return;
        this.isEnabled = false;
        this.getPluginManager().unregisterEeventHandlers(this);
        this.onDisable();
    }

    protected final boolean isEnabled() {
        return this.isEnabled;
    }

    protected void registerEvents(EventBusStop busStop) {
        this.pluginManager.registerEventHandlers(busStop, this);
    }

    public Logger getLogger() {
        return this.pluginManager.getLogger();
    }
}
