package io.github.mrblobman.crosschat.server.command.defaultcommands;

import io.github.mrblobman.crosschat.network.data.ChatColor;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.command.management.CommandHandle;
import io.github.mrblobman.crosschat.server.command.management.CommandHandler;
import io.github.mrblobman.crosschat.server.command.management.CommandSender;
import io.github.mrblobman.crosschat.server.model.Player;
import io.github.mrblobman.crosschat.server.model.PlayerRegistry;
import io.github.mrblobman.crosschat.server.permission.DefaultPermissions;

/**
 * Created on 13/12/2015.
 */
public class AdminCommands implements CommandHandler {
    private PlayerRegistry players;
    
    public AdminCommands(PlayerRegistry players) {
        this.players = players;
    }
    
    @CommandHandle(command = {"kick"}, permission = DefaultPermissions.COMMAND_ADMIN_KICK, description = "Kick a user from the server.")
    private void kick(CommandSender sender, String toKick, String... reason) {
        String kickReason = "Kick: " + CommandUtils.rebuildSplitVarargs(reason);

        Player player = players.getByName(toKick, false);
        if (player == null) {
            sender.sendMessage(CommandUtils.buildCommandSuggestionWithValidPlayer(players, toKick, "/kick {player} "+kickReason));
            return;
        }

        player.kick(ChatMessageBulk.fromDecoratedString(kickReason));
    }

    @CommandHandle(command = {"userinfo|uinfo"}, permission = DefaultPermissions.COMMAND_ADMIN_USERINFO, description = "Get information about a specific user.")
    private void userInfo(CommandSender sender, String user) {
        Player player = players.getByName(user, false);
        if (player == null) {
            sender.sendMessage(CommandUtils.buildCommandSuggestionWithValidPlayer(players, user, "/userinfo {player}"));
            return;
        }

        ChatMessageBulk message = new ChatMessageBulk("User info for "+player.getName()+":").setColor(ChatColor.AQUA);
        message.append("\n\tUUID: ").setColor(ChatColor.AQUA).append(player.getUniqueId().toString()).setColor(ChatColor.RED);
        message.append("\n\tChannel: ").setColor(ChatColor.AQUA).append(player.getCurrentChannel().getDisplayName()).setColor(ChatColor.RED);
        message.append("\n\tPermissions: ").setColor(ChatColor.AQUA).append(player.listPermissions()).setColor(ChatColor.RED);
        sender.sendMessage(message);
    }
}
