package io.github.mrblobman.crosschat.server.model;

import io.github.mrblobman.crosschat.server.Client;
import io.github.mrblobman.crosschat.server.CrossChatServer;

import java.util.Iterator;
import java.util.Map;
import java.util.Spliterator;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

/**
 * Created by MrBlobman on 09/09/2015.
 */
public class PlayerRegistry implements Iterable<Player> {
    private Map<UUID, Player> players = new ConcurrentHashMap<>();
    private CrossChatServer server;

    public PlayerRegistry(CrossChatServer server) {
        this.server = server;
    }

    /**
     * Unregister the player with the given {@code playerId}
     * @param playerId the UUID to unregister.
     * @see Player#getUniqueId()
     */
    public void unregister(UUID playerId) {
        this.players.remove(playerId);
    }

    /**
     * Unregister the given {@code player}.
     * @param player the {@link Player} to unregister.
     */
    public void unregister(Player player) {
        unregister(player.getUniqueId());
    }

    /**
     * Add the {@code Player} to this registry.
     * @param client the {@link Client} to add.
     * @return the newly created {@link Player} for the given client
     */
    public Player register(Client client) {
        if (!client.isLoggedIn()) throw new IllegalArgumentException("The client is not logged in yet");
        return players.put(client.getGameProfile().getId(),
                new Player(
                        this.server,
                        client,
                        this.server.getDataBase().loadPlayerData(client.getGameProfile().getId()).getPermissions()
                )
        );
    }

    /**
     * Lookup a player by uuid. This is the most efficient method
     * of lookup and should be used if a {@link UUID} is available
     * @param id the {@link UUID} of the player you are looking for.
     * @return the {@link Player} with the given {@code id} or null
     * if no players could be found.
     */
    public Player getByUUID(UUID id) {
        return this.players.get(id);
    }

    /**
     * Lookup a player by name.
     * @param name the name of the player you are looking for
     * @param caseSensitive true if the lookup should be case-sensitive,
     *                      false otherwise.
     * @return the {@link Player} with the given name or null if none are found
     */
    public Player getByName(String name, boolean caseSensitive) {
        if (caseSensitive) {
            return players.values().parallelStream()
                    .filter(player -> player.getName().equals(name))
                    .findAny()
                    .orElse(null);
        } else {
            return players.values().parallelStream()
                    .filter(player -> player.getName().equalsIgnoreCase(name))
                    .findAny()
                    .orElse(null);
        }
    }

    /**
     * Used to lookup a player when only a portion of their name is known.
     * @param nameFragment the known portion of the name.
     * @return an array containing all players who's name contains the fragment
     */
    public Player[] searchFor(String nameFragment) {
        return players.values().parallelStream()
                .filter(player -> player.getName().contains(nameFragment))
                .toArray(Player[]::new);
    }

    /**
     * Used to lookup players by name with the added functionality
     * of a regular expression matcher.
     * @param namePattern the regular expression all returned {@link Player}'s
     *                    names match.
     * @return an array of {@link Player}s that all have names matching the
     * {@code namePattern}
     */
    public Player[] getByPattern(String namePattern) {
        return players.values().parallelStream()
                .filter(player -> player.getName().matches(namePattern))
                .toArray(Player[]::new);
    }

    @Override
    public Iterator<Player> iterator() {
        return this.players.values().iterator();
    }

    @Override
    public void forEach(Consumer<? super Player> action) {
        this.players.values().forEach(action);
    }

    @Override
    public Spliterator<Player> spliterator() {
        return this.players.values().spliterator();
    }
}
