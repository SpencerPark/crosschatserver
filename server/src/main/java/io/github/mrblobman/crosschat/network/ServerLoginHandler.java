package io.github.mrblobman.crosschat.network;


import io.github.mrblobman.crosschat.auth.AuthClient;
import io.github.mrblobman.crosschat.auth.data.GameProfile;
import io.github.mrblobman.crosschat.auth.data.response.ErrorResponse;
import io.github.mrblobman.crosschat.auth.data.response.HasJoinedResponse;
import io.github.mrblobman.crosschat.auth.data.response.Response;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.EncryptionRequestPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.LoginSuccessPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.SetCompressionPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.serverbound.EncryptionResponsePacket;
import io.github.mrblobman.crosschat.server.Client;
import io.github.mrblobman.crosschat.server.CrossChatServer;
import io.github.mrblobman.crosschat.server.NetworkServer;
import io.github.mrblobman.crosschat.server.event.player.PlayerLoginEvent;
import io.github.mrblobman.crosschat.util.CryptUtil;

import javax.crypto.SecretKey;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.util.Arrays;

/**
 * Created by MrBlobman on 15-08-27.
 */
public class ServerLoginHandler {
    private Client client;
    private String unofficialUsername;
    private byte[] verifyToken;
    private NetworkServer netServer;
    private CrossChatServer server;

    public ServerLoginHandler(NetworkServer netServer, Client client, String unofficialUsername, CrossChatServer server) {
        this.netServer = netServer;
        this.client = client;
        this.unofficialUsername = unofficialUsername;
        this.server = server;
        EncryptionRequestPacket packet = new EncryptionRequestPacket(netServer.getServerID(),
                                                                     netServer.getKeyPair().getPublic());
        client.sendPacket(packet);
        this.verifyToken = packet.getVerifyToken();
    }

    public void onEncryptionResponse(EncryptionResponsePacket packet) {
        PrivateKey redPaint = netServer.getKeyPair().getPrivate();
        try {
            if (!Arrays.equals(this.verifyToken, packet.getVerifyToken(redPaint))) {
                client.disconnect("Incorrect verification token.");
                return;
            }
        } catch (GeneralSecurityException e) {
            client.disconnect("Cannot get verify token.");
            netServer.getLogger().severe("Cannot get verify token for " + client + ". REASON: "+e.getClass().getSimpleName()+"->"+e.getLocalizedMessage());
            return;
        }
        //System.out.println("DEBUG -> Correct response");
        SecretKey brownPaint;
        try {
            brownPaint = packet.getSharedKey(redPaint);
            client.enableEncryption(brownPaint);
        } catch (GeneralSecurityException e) {
            client.disconnect("Cannot enable encryption.");
            return;
        }

        //Encryption exchange is successful. Log them in.
        String serverHash;
        try {
            serverHash = CryptUtil.generateServerHash(brownPaint, netServer.getKeyPair().getPublic(), netServer.getServerID());
        } catch (GeneralSecurityException | UnsupportedEncodingException e) {
            client.disconnect("Cannot hash login info.");
            return;
        }
        startVerification(serverHash);
    }

    private void startVerification(String serverId) {
        Thread authThread = new Thread(() -> {

            try {
                Response response = AuthClient.hasJoinedServer(this.unofficialUsername, serverId);
                if (response instanceof ErrorResponse) {
                    ErrorResponse err = (ErrorResponse) response;
                    client.disconnect(err.getException().getErrorMsg());
                } else {
                    GameProfile profile = ((HasJoinedResponse) response).getProfile();
                    client.setProfile(profile);
                    PlayerLoginEvent event = new PlayerLoginEvent(profile);
                    netServer.getEventBus().boardBus(event);
                    if (event.isCanceled()) {
                        client.disconnect(event.getDisconnectMessage());
                        return;
                    }
                    client.sendPacket(new SetCompressionPacket(NetworkServer.COMPRESSION_THRESHOLD));
                    client.enableCompression(NetworkServer.COMPRESSION_THRESHOLD);
                    client.sendPacket(new LoginSuccessPacket(profile.getId(), profile.getName()));
                    client.setClientState(ClientState.AUTHENTICATED);
                    server.join(client);
                }
            } catch (Exception e) {
                client.disconnect("Unable to log you in.");
                this.server.getLogger().warning(String.format("Could not log in %s. %s: %s", this.unofficialUsername, e.getClass().getName(), e.getLocalizedMessage()));
            }
        });
        authThread.setName("Authentication{"+unofficialUsername+"}");
        authThread.start();
    }
}
