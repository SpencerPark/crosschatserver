package io.github.mrblobman.crosschat.server.database;

import io.github.mrblobman.crosschat.server.permission.PermissionGroup;

import java.io.Closeable;
import java.util.UUID;

/**
 * Created by MrBlobman on 15-09-04.
 */
public interface Database extends Closeable {
    PlayerData loadPlayerData(UUID id);
    void savePlayerData(UUID id, PlayerData data);
    void deletePlayerData(UUID id);

    PermissionGroup loadPermissionGroup(String groupName);
    void savePermissionGroup(PermissionGroup group);
    void deletePermissionGroup(String groupName);
}
