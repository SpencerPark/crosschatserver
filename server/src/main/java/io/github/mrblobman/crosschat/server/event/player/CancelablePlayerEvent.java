package io.github.mrblobman.crosschat.server.event.player;

import io.github.mrblobman.crosschat.server.event.CancelableEvent;
import io.github.mrblobman.crosschat.server.model.Player;

/**
 * Created by MrBlobman on 09/09/2015.
 */
public abstract class CancelablePlayerEvent extends CancelableEvent {
    private Player player;

    public CancelablePlayerEvent(Player player) {
        this.player = player;
    }

    public Player getPlayer () {
        return this.player;
    }
}
