package io.github.mrblobman.crosschat.server.command.management;

import io.github.mrblobman.crosschat.server.model.MessageRecipient;
import io.github.mrblobman.crosschat.server.model.Player;
import io.github.mrblobman.crosschat.server.permission.Permissible;

/**
 * Created by MrBlobman on 9/12/2015.
 */
public interface CommandSender extends Permissible, MessageRecipient{

    /**
     * Convenience method to check if this sender
     * can be safely cast to a player.
     * @return true if this sender is a {@link Player}, false otherwise
     */
    boolean isPlayer();

    /**
     * Convenience method to get some String that represents this sender.
     * @return a name for this sender.
     */
    String getName();
}
