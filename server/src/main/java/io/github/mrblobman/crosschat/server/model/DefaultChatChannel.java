package io.github.mrblobman.crosschat.server.model;

import io.github.mrblobman.crosschat.server.event.EventBus;

/**
 * Created by MrBlobman on 15-09-04.
 */
public class DefaultChatChannel extends ChatChannel {

    protected DefaultChatChannel(int channelId, String displayName, String description, EventBus bus) {
        super(channelId, displayName, description, bus);
    }

    public void add(Player player) {
        try {
            super.add(player, true);
        } catch (ChannelException e) {
            //Will never happen
        }
    }

    @Override
    public void add(Player player, boolean force) throws ChannelException {
        super.add(player, true);
    }

    @Override
    public boolean canJoin(Player player) {
        return true;
    }

    @Override
    public void setMaxUsers(int maxUsers) {
        throw new UnsupportedOperationException("The default channel cannot have a max number of users.");
    }

    @Override
    public void setPermissionToJoin(String permissionToJoin) {
        throw new UnsupportedOperationException("The default channel cannot require a permission to join.");
    }
}
