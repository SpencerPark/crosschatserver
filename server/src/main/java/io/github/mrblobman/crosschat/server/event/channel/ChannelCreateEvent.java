package io.github.mrblobman.crosschat.server.event.channel;

import io.github.mrblobman.crosschat.server.model.ChatChannel;

/**
 * Created by MrBlobman on 15-09-04.
 */
public class ChannelCreateEvent extends ChannelEvent {

    public ChannelCreateEvent(ChatChannel channel) {
        super(channel);
    }

    @Override
    public String getName() {
        return "ChannelCreateEvent";
    }
}
