package io.github.mrblobman.crosschat.server.permission;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MrBlobman on 15-09-04.
 */
public class PermissionStore {
    private Map<String, PermissionNode> perms = new HashMap<>();

    public PermissionStore() {}

    public PermissionStore(JsonArray serialized) {
        serialized.forEach(element -> {
            if (!element.isJsonObject()) throw new JsonParseException("Element in array is not a json object");
            JsonObject rootNode = element.getAsJsonObject();
            rootNode.entrySet().forEach(entry -> perms.put(entry.getKey().toLowerCase(), new PermissionNode(null, entry.getKey(), entry.getValue())));
        });
    }

    public void addPermission(String permission) {
        if (permission.length() <= 0) return;
        if (permission.contains("*") && !permission.endsWith("*")) throw new IllegalArgumentException("Wildcard must be placed at the end of the permission");
        String[] parts = permission.toLowerCase().split("\\.");

        PermissionNode parentNode = perms.get(parts[0]);
        if (parentNode == null) {
            parentNode = new PermissionNode(null, parts[0]);
            perms.put(parentNode.getName(), parentNode);
        }

        PermissionNode workingNode;
        for (int i = 1; i < parts.length; i++) {
            workingNode = parentNode.getChild(parts[i]);
            if (workingNode == null)
                workingNode = parentNode.addChild(parts[i]);
            parentNode = workingNode;
        }
    }

    public boolean hasPermission(String permission) {
        if (permission.length() <= 0) return true;
        if (permission.contains("*") && !permission.endsWith("*")) throw new IllegalArgumentException("Wildcard must be placed at the end of the permission");
        String[] parts = permission.split("\\.");

        if (perms.containsKey("*")) return true;

        PermissionNode parentNode = perms.get(parts[0].toLowerCase());
        if (parentNode == null) return false;

        PermissionNode workingNode;
        for (int i = 1; i < parts.length; i++) {
            if (parentNode.hasWildcardChild()) return true;
            workingNode = parentNode.getChild(parts[i]);
            if (workingNode == null) return false;
            parentNode = workingNode;
        }
        //We didn't find any null nodes so this store had every sub node
        return true;
    }

    public void removePermission(String permission) {
        if (permission.length() <= 0) return;
        if (permission.contains("*") && !permission.endsWith("*")) throw new IllegalArgumentException("Wildcard must be placed at the end of the permission");
        String[] parts = permission.split("\\.");

        //Remove everything
        if (parts[0].equals("*")) {
            this.perms.clear();
            return;
        }

        PermissionNode parentNode = perms.get(parts[0].toLowerCase());
        if (parentNode == null) return;

        if (parts.length > 1) {
            //We need to keep looking
            PermissionNode workingNode = parentNode;
            for (int i = 1; i < parts.length-1; i++) {
                workingNode = workingNode.getChild(parts[i]);
                if (workingNode == null) return; //They don't have the permission we want removed
            }
            //Working node is now the furthest node down the chain
            workingNode.removeChild(parts[parts.length-1]);
        } else {
            //This is a deep as we need to look. They are removing a root.
            this.perms.remove(parts[0].toLowerCase());
        }
    }

    /**
     * Merge two {@link PermissionStore}s together.
     * This will add all permissions from {@code other} to
     * this {@link PermissionStore}.
     * @param other the other set of permissions to add to this
     *              permission store.
     */
    public void merge(PermissionStore other) {
        if (other == null) return;
        other.perms.forEach((childName, child) -> this.perms.merge(childName, child,
                        (oldVal, val) -> {
                            oldVal.merge(val);
                            return oldVal;
                        }
                )
        );
    }

    public JsonArray serialize() {
        JsonArray serialized = new JsonArray();
        this.perms.forEach((name, node) -> {
            JsonObject element = new JsonObject();
            element.add(name, node.serialize());
            serialized.add(element);
        });
        return serialized;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        this.perms.forEach((name, node) -> node.format(sb, 0));
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermissionStore that = (PermissionStore) o;

        return perms.equals(that.perms);

    }

    @Override
    public int hashCode() {
        return perms.hashCode();
    }
}
