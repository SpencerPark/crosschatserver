package io.github.mrblobman.crosschat.server.model;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.data.ConsoleColors;
import io.github.mrblobman.crosschat.network.data.MessageScope;
import io.github.mrblobman.crosschat.server.command.management.CommandSender;
import io.github.mrblobman.crosschat.server.permission.PermissionGroup;

/**
 * Created by MrBlobman on 9/13/2015.
 */
public class ConsoleCommandSender implements CommandSender {
    private boolean useColor;

    public ConsoleCommandSender(boolean useColor) {
        this.useColor = useColor;
    }

    @Override
    public boolean isPlayer() {
        return false;
    }

    @Override
    public String getName() {
        return "SERVER";
    }

    @Override
    public void sendMessage(ChatMessageBulk message) {
        System.out.println(translate(message));
    }

    @Override
    public void sendMessage(String from, MessageScope scope, ChatMessageBulk message) {
        System.out.println("["+scope.name()+"] "+from+" -> "+ translate(message));
    }

    @Override
    public boolean hasPermission(String permission) {
        return true;
    }

    @Override
    public void givePermission(String permission) {
        //Do nothing, console already has all permissions
    }

    @Override
    public void removePermission(String permission) {
        //Do nothing, console has all permissions
    }

    @Override
    public boolean isInPermissionGroup(String permissionGroupName) {
        return true;
    }

    @Override
    public void joinPermissionGroup(PermissionGroup group) {
        //Do nothing, console is in all groups
    }

    @Override
    public void leavePermissionGroup(String group) {
        //Do nothing, console is in all groups
    }

    private String translate(ChatMessageBulk message) {
        if (useColor) {
            return ConsoleColors.translate(message);
        } else {
            return message.getCleanText();
        }
    }
}
