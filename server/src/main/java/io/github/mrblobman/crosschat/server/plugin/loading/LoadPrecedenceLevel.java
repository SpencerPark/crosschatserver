package io.github.mrblobman.crosschat.server.plugin.loading;

/**
 * Created on 17/11/2015.
 */
public enum LoadPrecedenceLevel {
    /**
     * This plugin is a library and should be in the
     * first phase of loading plugins.
     */
    LIBRARY,
    /**
     * This plugin is a core plugin and should be
     * loaded in the second phase of loading plugins.
     */
    CORE_PLUGIN,
    /**
     * This plugin is a standard plugin and should be
     * loaded in the third (last) phase of loading plugins.
     */
    PLUGIN
}
