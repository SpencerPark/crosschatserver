package io.github.mrblobman.crosschat.server.plugin.loading;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created on 17/11/2015.
 */
public class LoadGraph {
    private Set<LoadConstraint> constraints;
    private Set<String> trackedPlugins;
    private List<String> loadOrder;
    private Set<String> problemConstraints;
    private boolean dirty = true;

    public LoadGraph() {
        constraints = new HashSet<>();
        trackedPlugins = new HashSet<>();
        loadOrder = new ArrayList<>();
        problemConstraints = new HashSet<>();
    }

    /**
     * Add a set of constraints to this graph. These constraints
     * will modify and/or add plugins to the {@link #getLoadOrder()}.
     * @param constraints the constraints to add
     */
    public void addConstraints(LoadConstraints constraints) {
        this.dirty = true;
        this.trackedPlugins.add(constraints.getPluginName());
        this.constraints.addAll(constraints.getConstraints());
    }

    /**
     * Add a set of constraints to this graph. These constraints
     * will modify and/or add plugins to the {@link #getLoadOrder()}.
     * @param constraints the constraints to add
     */
    public void addConstraints(LoadConstraints... constraints) {
        for (LoadConstraints constraint : constraints) addConstraints(constraint);
    }

    /**
     * Remove all constraints with for the plugin {@code pluginName}.
     * @param pluginName the name of the plugin to remove from the graph.
     */
    public void removeConstraints(String pluginName) {
        this.trackedPlugins.remove(pluginName.toLowerCase());
        this.constraints.removeIf(constraint ->
                constraint.getSecond().equalsIgnoreCase(pluginName));
    }

    /**
     * Get the order in which to load the plugins in this graph
     * such that they enforce their order constraints.
     * @return a list with the first plugin to load at the head
     * and the last at the tail
     * @throws CircularLoadDependencyException if the graph contains
     * a dependency cycle (A -&gt; B, B -&gt; C, C-&gt;A). After this
     * exception is tossed the problematic plugins can be found with
     * {@link LoadGraph#getCyclicDependents()}.
     */
    public List<String> getLoadOrder() throws CircularLoadDependencyException {
        if (dirty) buildLoadOrder(Collections.emptySet());
        return Collections.unmodifiableList(loadOrder);
    }

    /**
     * Get a list of cyclic dependents. The combination of these plugin's
     * {@link LoadConstraints} results in a cycle in which a load order cannot
     * be determined.
     * @return the problematic plugins
     */
    public Set<String> getCyclicDependents() {
        if (dirty) try {
            buildLoadOrder(Collections.emptySet());
        } catch (CircularLoadDependencyException ignored) { }
        return this.problemConstraints;
    }

    private void buildLoadOrder(Set<String> exclude) throws CircularLoadDependencyException {
        this.dirty = false;
        List<String> sorted = new ArrayList<>();

        Queue<String> roots = new LinkedList<>();
        Map<String, Integer> degrees = new HashMap<>();
        trackedPlugins.stream()
                .filter(plugin -> !exclude.contains(plugin))
                .forEach(plugin -> degrees.put(plugin, 0));
        constraints.stream()
                .filter(constraint -> !exclude.contains(constraint.getFirst()) && !exclude.contains(constraint.getSecond()))
                .forEach(constraint -> degrees.merge(constraint.getSecond(), 1, Integer::sum));
        degrees.forEach((node, degree) -> {
            if (degree == 0) roots.add(node);
        });

        Map<String, Set<String>> edges = new HashMap<>();
        constraints.stream()
                .filter(constraint -> !exclude.contains(constraint.getFirst()) && !exclude.contains(constraint.getSecond()))
                .forEach(constraint -> {
                    Set<String> existing = edges.get(constraint.getFirst());
                    if (existing == null) {
                        existing = new HashSet<>();
                        edges.put(constraint.getFirst(), existing);
                    }
                    existing.add(constraint.getSecond());
                });

        while (!roots.isEmpty()) {
            String node = roots.poll();
            sorted.add(node);
            if (edges.containsKey(node))
                roots.addAll(edges.get(node).stream()
                        .filter(second -> degrees.computeIfPresent(second, (key, old) -> old - 1) == 0)
                        .collect(Collectors.toList()));
            edges.remove(node);
        }

        this.loadOrder = sorted;
        if (!edges.isEmpty()) {
            this.problemConstraints = edges.keySet();
            this.buildLoadOrder(this.problemConstraints); //Run again without the bad apples to ruin the batch
            throw new CircularLoadDependencyException("Load graph is not acyclic. Skipped plugins " + problemConstraints.toString());
        }
    }
}
