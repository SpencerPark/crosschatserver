package io.github.mrblobman.crosschat.server.model;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.data.MessageScope;

/**
 * Created by MrBlobman on 15-09-04.
 */
public interface MessageRecipient {

    /**
     * Send a {@link MessageScope#GENERIC} message with
     * no sender.
     * @param message the message to send.
     */
    void sendMessage(ChatMessageBulk message);

    /**
     * Allows for more control over the send params.
     * Sends a message to this recipient.
     * @param from the name of the entity sending the message.
     * @param scope the scope of the message. See {@link MessageScope} for scope
     *              explanations.
     * @param message the message to send.
     */
    void sendMessage(String from, MessageScope scope, ChatMessageBulk message);
}
