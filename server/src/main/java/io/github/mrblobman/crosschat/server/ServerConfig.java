package io.github.mrblobman.crosschat.server;

/**
 * Created by MrBlobman on 15-09-07.
 */
public class ServerConfig {
    private int port;
    private boolean useColor;

    public ServerConfig(int port, boolean useColor) {
        this.port = port;
        this.useColor = useColor;
    }

    public int getPort() {
        return port;
    }

    public boolean useColor() {
        return useColor;
    }
}
