package io.github.mrblobman.crosschat.server.model;

import io.github.mrblobman.crosschat.configuration.Hide;
import io.github.mrblobman.crosschat.configuration.Reveal;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.data.MessageScope;
import io.github.mrblobman.crosschat.server.event.EventBus;
import io.github.mrblobman.crosschat.server.event.EventException;
import io.github.mrblobman.crosschat.server.event.channel.ChannelChangeEvent;
import io.github.mrblobman.crosschat.server.permission.DefaultPermissions;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by MrBlobman on 15-09-03.
 */
@Hide
public class ChatChannel implements MessageRecipient, Iterable<Player> {
    private int channelId;
    @Reveal private String displayName;
    @Reveal private String description;
    private final Set<Player> users;
    @Reveal private int talkPower = -1;
    @Reveal private String permissionToJoin = "";
    @Reveal private int maxUsers = -1;
    private EventBus eventBus;

    protected ChatChannel(int channelId, String displayName, String description, EventBus bus) {
        this.channelId = channelId;
        this.displayName = displayName;
        this.description = description;
        this.eventBus = bus;
        this.users = new HashSet<>();
    }

    public void remove(Player player) {
        users.remove(player);
    }

    /**
     * Add a player to this channel.
     * @param player the player to add to the channel
     * @param force true if the player should forcefully be added to the channel.
     *              This means skipping the checks for permissions and max users.
     * @throws ChannelException if the player could not be added to the channel.
     */
    public void add(Player player, boolean force) throws ChannelException {
        if (!force && this.requiresPermissionToJoin() && !player.hasPermission(permissionToJoin))
            throw new ChannelException("Insufficient permission. Requires the "+permissionToJoin+" to join.");
        if (!force && this.isFull() && !player.hasPermission(DefaultPermissions.JOIN_FULL_CHANNELS))
            throw new ChannelException("Channel is full.");
        ChannelChangeEvent event = new ChannelChangeEvent(this, player.getCurrentChannel(), player);
        try {
            eventBus.boardBus(event);
        } catch (EventException e) {
            throw new ChannelException(e);
        }
        if (!force && event.isCanceled())
            throw new ChannelException("Channel join canceled.");
        users.add(player);
    }

    /**
     * Check if a player can join the channel.
     * @param player the player to test.
     * @return true if the player can join the channel, false otherwise
     */
    public boolean canJoin(Player player) {
        return !(this.requiresPermissionToJoin() && !player.hasPermission(permissionToJoin))
                && (!this.isFull() || player.hasPermission(DefaultPermissions.JOIN_FULL_CHANNELS));
    }

    public boolean isInChannel(Player player) {
        return this.users.contains(player);
    }

    @Override
    public void sendMessage(ChatMessageBulk message) {
        synchronized (users) {
            users.parallelStream()
                    .forEach(user -> user.sendMessage(this.getDisplayName(), MessageScope.CHANNEL, message));
        }
    }

    @Override
    public void sendMessage(String from, MessageScope scope, ChatMessageBulk message) {
        synchronized (users) {
            users.parallelStream()
                    .forEach(user -> user.sendMessage(from, scope, message));
        }
    }

    public int getChannelId() {
        return channelId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNeededTalkPower() {
        return talkPower;
    }

    public void setNeededTalkPower(int talkPower) {
        this.talkPower = talkPower;
    }

    public boolean requiresTalkPower() {
        return this.talkPower > 0;
    }

    public String getPermissionToJoin() {
        return this.permissionToJoin;
    }

    public void setPermissionToJoin(String permissionToJoin) {
        this.permissionToJoin = permissionToJoin;
    }

    public boolean requiresPermissionToJoin() {
        return this.permissionToJoin != null && !this.permissionToJoin.equals("");
    }

    public int getMaxUsers() {
        return maxUsers;
    }

    public void setMaxUsers(int maxUsers) {
        this.maxUsers = maxUsers;
    }

    public int getUserCount() {
        return this.users.size();
    }

    public boolean hasUserLimit() {
        return this.maxUsers > 0;
    }

    public boolean isFull() {
        return hasUserLimit() && this.maxUsers >= users.size();
    }

    @Override
    public Iterator<Player> iterator() {
        return users.iterator();
    }

    @Override
    public void forEach(Consumer<? super Player> action) {
        users.forEach(action);
    }

    @Override
    public Spliterator<Player> spliterator() {
        return users.spliterator();
    }
}
