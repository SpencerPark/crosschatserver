package io.github.mrblobman.crosschat.server.command.management;

import io.github.mrblobman.crosschat.network.data.ChatColor;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.CrossChatServer;
import io.github.mrblobman.crosschat.server.event.EventException;
import io.github.mrblobman.crosschat.server.event.command.CommandDispatchEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by MrBlobman on 9/12/2015.
 */
public class CommandManager {
    private static Pattern ARG_PATTERN = Pattern.compile("(?:(['\"])(.*?)(?<!\\\\)(?>\\\\\\\\)*\\1|([^\\s]+))");
    private CommandRegistry commandRegistry;
    private CrossChatServer server;

    public CommandManager(CrossChatServer server) {
        this.commandRegistry = new CommandRegistry(server.getLogger(), server.getEventBus());
        this.server = server;
    }

    public void registerHandler(CommandHandler handler) {
        commandRegistry.register(handler);
    }

    public void dispatchCommand(CommandSender sender, String command) throws CommandException {
        String[] parsedCommand = parseCommandString(command);
        if (parsedCommand.length == 0) return;
        String cmd = parsedCommand[0];
        List<String> args = new ArrayList<>(parsedCommand.length-1);
        for (int i = 1; i < parsedCommand.length; i++ ) args.add(parsedCommand[i]);
        CommandDispatchEvent event = new CommandDispatchEvent(cmd, args, sender);
        try {
            this.server.getEventBus().boardBus(event);
        } catch (EventException e) {
            server.getLogger().log(Level.SEVERE, "Error handling " + event.getName() + " for " + sender.getName(), e);
            sender.sendMessage(new ChatMessageBulk("An internal error has occurred while attempting to process the command."));
            return;
        }
        if (event.isCanceled()) return;
        args.add(0, event.getCommand());
        parsedCommand = args.toArray(new String[args.size()]);
        if (!this.commandRegistry.handleCommand(sender, parsedCommand)) {
            sender.sendMessage(new ChatMessageBulk("Unknown command. ("+command+")").setColor(ChatColor.RED));
            sender.sendMessage(new ChatMessageBulk("Acceptable commands are the following: ").setColor(ChatColor.YELLOW));
            commandRegistry.displayHelp(sender, parsedCommand);
        }
    }

    private String[] parseCommandString(String command) {
        if (command.startsWith("/")) {
            //Trim the /
            command = command.substring(1);
        }
        List<String> matches = new ArrayList<>();
        Matcher m = ARG_PATTERN.matcher(command);
        while (m.find()) {
            if (m.group(2) != null) {
                matches.add(m.group(2));
            } else if (m.group(3) != null) {
                matches.add(m.group(3));
            }
        }
        return matches.toArray(new String[matches.size()]);
    }

    public void displayHelp(CommandSender sender, String[] cmdGiven) {
        this.commandRegistry.displayHelp(sender, cmdGiven);
    }
}
