package io.github.mrblobman.crosschat.server.permission;

/**
 * Created by MrBlobman on 15-09-04.
 */
public class DefaultPermissions {
    public static final String JOIN_FULL_CHANNELS = "crosschat.channel.joinfull";

    public static final String COMMAND_BROADCAST = "crosschat.commands.broadcast";

    public static final String COMMAND_CHANNEL_MOVE_OTHER = "crosschat.commands.admin.channel.moveother";
    public static final String COMMAND_CHANNEL_KICK = "crosschat.commands.admin.channel.kick";

    public static final String COMMAND_LIST_CHANNELS = "crosschat.commands.list";
    public static final String COMMAND_JOIN_CHANNEL = "crosschat.commands.join";

    public static final String COMMAND_PERMS_ADDGROUP = "crosschat.commands.addgroup";
    public static final String COMMAND_PERMS_REMOVEGROUP = "crosschat.commands.removegroup";
    public static final String COMMAND_PERMS_GIVE_PERM = "crosschat.commands.giveperm";
    public static final String COMMAND_PERMS_GIVE_GROUP = "crosschat.commands.givegroup";
    public static final String COMMAND_PERMS_TAKE_PERM = "crosschat.commands.takeperm";
    public static final String COMMAND_PERMS_TAKE_GROUP = "crosschat.commands.takegroup";
    public static final String COMMAND_PERMS_LIST = "crosschat.commands.listperms";

    public static final String EMPTY_PERMISSION = "";

    public static final String COMMAND_ADMIN_KICK = "crosschat.commands.admin.kick";
    public static final String COMMAND_ADMIN_USERINFO = "crosschat.commands.admin.userinfo";
}
