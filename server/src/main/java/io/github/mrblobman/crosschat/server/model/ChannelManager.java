package io.github.mrblobman.crosschat.server.model;

import io.github.mrblobman.crosschat.network.data.ChatColor;
import io.github.mrblobman.crosschat.network.data.ChatMessage;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.data.MessageScope;
import io.github.mrblobman.crosschat.server.event.EventBus;
import io.github.mrblobman.crosschat.server.event.EventBusStop;
import io.github.mrblobman.crosschat.server.event.EventException;
import io.github.mrblobman.crosschat.server.event.EventHandler;
import io.github.mrblobman.crosschat.server.event.channel.ChannelCreateEvent;
import io.github.mrblobman.crosschat.server.event.channel.ChannelDestroyedEvent;
import io.github.mrblobman.crosschat.server.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by MrBlobman on 15-09-04.
 */
public class ChannelManager implements MessageRecipient, EventBusStop {
    private int topId = 1;
    private final Set<ChatChannel> channels = new HashSet<>();
    private DefaultChatChannel defaultChannel;
    private EventBus eventBus;

    public ChannelManager(String defaultChannelName, String description, EventBus eventBus) {
        this.eventBus = eventBus;
        this.defaultChannel = new DefaultChatChannel(0, defaultChannelName, description, eventBus);
        channels.add(this.defaultChannel);
    }

    /**
     * Get the default channel.
     * @return the default channel.
     */
    public DefaultChatChannel getDefaultChannel() {
        return this.defaultChannel;
    }

    /**
     * Lookup a channel by its id.
     * @param id the id of the channel
     * @return the channel with the given id or null if none are found.
     */
    public ChatChannel getById(int id) {
         synchronized (channels) {
             return channels.stream()
                     .filter(channel -> channel.getChannelId() == id)
                     .findAny()
                     .orElse(null);
         }
    }

    /**
     * Create a new channel.
     * @param displayName the name of the channel
     * @return the newly created and registered {@link ChatChannel}
     */
    public ChatChannel create(String displayName) {
        ChatChannel channel = new ChatChannel(topId++, displayName, "", eventBus);
        channels.add(channel);
        ChannelCreateEvent event = new ChannelCreateEvent(channel);
        try {
            eventBus.boardBus(event);
        } catch (EventException e) {
            e.printStackTrace();
        }
        return channel;
    }

    /**
     * Unregister {@code channel} and move all it's users to
     * the default channel.
     * @param channel the channel to destroy
     */
    public synchronized void destroy(ChatChannel channel) {
        if (channel instanceof DefaultChatChannel) throw new IllegalArgumentException("Cannot destroy a default channel.");
        final ChannelDestroyedEvent event = new ChannelDestroyedEvent(channel, defaultChannel);
        try {
            eventBus.boardBus(event);
        } catch (EventException e) {
            e.printStackTrace();
        }
        if (channels.remove(channel)) {
            channel.forEach(player -> {
                try {
                    event.getUserDestination().add(player, false);
                } catch (ChannelException exception) {
                    defaultChannel.add(player);
                }
                channel.remove(player);
            });
        }
    }

    /**
     * Find a channel by its display name. Note that lookup by id is quicker
     * and should be used over this method if possible.
     * @param displayName the display name of the channel
     * @param caseSensitive true if the search should be case-sensitive, false otherwise
     * @return the {@link ChatChannel} with the given name or null if none are found.
     */
    public ChatChannel getByName(String displayName, boolean caseSensitive) {
        synchronized (channels) {
            if (caseSensitive)
                return channels.stream()
                        .filter(channel -> channel.getDisplayName().equals(displayName))
                        .findAny()
                        .orElse(null);
            else
                return channels.stream()
                        .filter(channel -> channel.getDisplayName().equalsIgnoreCase(displayName))
                        .findAny()
                        .orElse(null);
        }
    }

    /**
     * Find the channel a player is in.
     * @param player the {@link Player} being searched for.
     * @return the {@link ChatChannel} the player is in or null
     * if the player could not be found.
     */
    public ChatChannel findUser(Player player) {
        synchronized (channels) {
            return channels.stream()
                    .filter(channel -> channel.isInChannel(player))
                    .findAny()
                    .orElse(null);
        }
    }

    /**
     * Search for a {@link ChatChannel} with a name similar to the
     * given filter.
     * eg. With the following channel names
     * {@code ["MrBlobman's Channel", "Another Channel", "Blob's Other Channel"]}
     * and a name filter of {@code "blob"} channels {@code ["MrBlobman's Channel", "Blob's Other Channel"]}
     * will be returned.
     * @param nameFilter the name filter.
     * @return the search results
     */
    public List<ChatChannel> searchFor(String nameFilter) {
        String searchFilter = nameFilter.toLowerCase();
        List<ChatChannel> results = new ArrayList<>();
        synchronized (channels) {
            channels.stream()
                    .filter(channel -> channel.getDisplayName().toLowerCase().contains(searchFilter))
                    .forEach(results::add);
        }
        return results;
    }

    /**
     * Search for a {@link ChatChannel} with a name similar to the
     * given filter.
     * eg. With the following channel names
     * {@code ["MrBlobman's Channel", "Another Channel", "Blob's Other Channel"]}
     * and a name filter of {@code "blob"} channels {@code ["MrBlobman's Channel", "Blob's Other Channel"]}
     * will be returned.
     * @param nameFilter the name filter.
     * @param results a list to add the search results to
     */
    public synchronized void searchFor(String nameFilter, List<ChatChannel> results) {
        String searchFilter = nameFilter.toLowerCase();
        synchronized (this) {
            channels.stream()
                    .filter(channel -> channel.getDisplayName().toLowerCase().contains(searchFilter))
                    .forEach(results::add);
        }
    }

    @Override
    public void sendMessage(ChatMessageBulk message) {
        this.channels.forEach(channel -> channel.forEach(
                    user -> user.sendMessage("Server", MessageScope.GLOBAL, message)
                )
        );
    }

    @Override
    public void sendMessage(String from, MessageScope scope, ChatMessageBulk message) {
        this.channels.parallelStream().forEach(channel -> channel.forEach(
                user -> user.sendMessage(from, scope, message)
        ));
    }

    public ChatMessageBulk channelInfo() {
        ChatMessageBulk msg = new ChatMessageBulk("Channel Summary:\n").setColor(ChatColor.AQUA);

        msg.append("Total players: ").setColor(ChatColor.AQUA);
        msg.append(String.valueOf(this.channels.stream()
                        .mapToInt(ChatChannel::getUserCount)
                        .sum())
        ).appendText("\n").setColor(ChatColor.RED);

        this.channels.stream()
                .sorted((channel1, channel2) -> channel1.getChannelId() - channel2.getChannelId())
                .forEach(channel -> {
                    msg.append("\t" + channel.getChannelId() + ". " + channel.getDisplayName()).setColor(ChatColor.AQUA);
                    msg.setTooltip(ChatColor.GRAY + "Click to join this channel",
                            channel.requiresPermissionToJoin() ? ChatColor.DARK_GRAY + "Requires permission: " + channel.getPermissionToJoin() : ChatColor.DARK_GRAY + "No permission needed.",
                            channel.requiresTalkPower() ? ChatColor.GRAY + "Requires talk power level: " + channel.getNeededTalkPower() : ChatColor.GRAY + "No talk power needed.",
                            ChatColor.DARK_GRAY + "Description: " + channel.getDescription()
                    );
                    msg.setClickAction(ChatMessage.CLICK_RUN_COMMAND, "/channel join " + channel.getChannelId());
                    msg.append("["
                            + channel.getUserCount()
                            + "/"
                            + (channel.hasUserLimit() ? channel.getMaxUsers() : "*")
                            + "]\n").setColor(ChatColor.RED);
                    List<String> playerInfo = new ArrayList<>(channel.getUserCount());
                    channel.forEach(player -> playerInfo.add((playerInfo.size() % 2 == 0 ? ChatColor.YELLOW : ChatColor.GOLD) + player.getName()));
                });
        return msg;
    }

    @EventHandler
    public void playerQuitHandler(PlayerQuitEvent event) {
        this.findUser(event.getPlayer()).remove(event.getPlayer());
    }
}
