package io.github.mrblobman.crosschat.server.plugin;

import io.github.mrblobman.crosschat.configuration.ConfigParser;
import io.github.mrblobman.crosschat.server.CrossChatServer;
import io.github.mrblobman.crosschat.server.event.Event;
import io.github.mrblobman.crosschat.server.event.EventBus;
import io.github.mrblobman.crosschat.server.event.EventBusStop;
import io.github.mrblobman.crosschat.server.event.EventHandler;
import io.github.mrblobman.crosschat.server.plugin.loading.CircularLoadDependencyException;
import io.github.mrblobman.crosschat.server.plugin.loading.LoadGraph;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created on 15-09-04.
 */
public class PluginManager {
    private Set<Plugin> loadedPlugins = new HashSet<>();
    private Map<Plugin, Set<EventBusStop>> registeredBusStops = new HashMap<>();
    private PluginLoader pluginLoader;
    private EventBus eventBus;
    private ConfigParser parser;
    private CrossChatServer server;

    public PluginManager(ConfigParser parser, EventBus eventBus, CrossChatServer server) {
        this.parser = parser;
        this.pluginLoader = new PluginLoader(parser, this);
        this.eventBus = eventBus;
        this.server = server;
    }

    public Plugin loadPlugin(File jarFile) throws PluginLoadExcetpion, InvalidPluginException {
        Plugin plugin = pluginLoader.load(jarFile);
        if (this.loadedPlugins.contains(plugin)) {
            plugin.disable();
            throw new PluginLoadExcetpion("Plugin already loaded.");
        }
        plugin.onLoad();
        this.loadedPlugins.add(plugin);
        plugin.enable();
        return plugin;
    }

    public void enable(String pluginName) {
        this.loadedPlugins.stream()
                .filter(plugin -> plugin.getName().equalsIgnoreCase(pluginName))
                .findFirst()
                .ifPresent(Plugin::enable);
    }

    public void disable(String pluginName) {
        this.loadedPlugins.stream()
                .filter(plugin -> plugin.getName().equalsIgnoreCase(pluginName))
                .findFirst()
                .ifPresent(Plugin::disable);
    }

    public void loadAll() throws IOException, PluginLoadExcetpion, InvalidPluginException {
        File pluginDirectory = new File("."+File.separator+"plugins");
        if (!pluginDirectory.exists()) {
            pluginDirectory.createNewFile();
            //We just made the folder so the wont be any plugins in it
            return;
        }

        File[] folderContents = pluginDirectory.listFiles();
        if (folderContents == null) return;
        List<File> jars = new ArrayList<>();
        //Sort out the jars
        for (File file : folderContents) {
            if (file.isFile() && file.getName().endsWith(".jar")) {
                //We don't want to load the plugin data folders
                jars.add(file);
            }
        }

        Map<String, File> pluginConfigs = new HashMap<>();
        LoadGraph loadGraph = new LoadGraph();
        jars.forEach(file -> {
            try {
                PluginConfig config = pluginLoader.getPluginConfig(file);
                loadGraph.addConstraints(config.loadConstraints);
                pluginConfigs.put(config.name.toLowerCase(), file);
            } catch (InvalidPluginException | IOException e) {
                e.printStackTrace();
            }
        });

        List<String> loadOrder;
        try {
            loadOrder = loadGraph.getLoadOrder();
        } catch (CircularLoadDependencyException e) {
            StringBuilder skipped = new StringBuilder();
            skipped.append("A dependency circle was discovered. Cannot load the plugins [");
            loadGraph.getCyclicDependents().stream()
                    .peek(plugin -> skipped.append(plugin).append(", ")).forEach(loadGraph::removeConstraints);
            skipped.setLength(skipped.length() - 2);
            skipped.append("].");
            getLogger().warning(skipped.toString());
            try {
                loadOrder = loadGraph.getLoadOrder();
            } catch (CircularLoadDependencyException e1) {
                throw new IllegalStateException("The load graph was cyclic despite removing the first cyclic dependents from the graph.");
            }
        }

        for (String pluginName : loadOrder) {
            File toLoad = pluginConfigs.get(pluginName.toLowerCase());
            if (toLoad == null) continue;
            this.loadPlugin(toLoad);
        }
    }

    /**
     * Register all stops in a busStop.
     * @param busStop the {@link EventBusStop} that contains the stops
     * @param plugin the {@link Plugin} owning the listener.
     */
    public void registerEventHandlers(EventBusStop busStop, Plugin plugin) {
        boolean registeredOne = false;

        for (Method m : busStop.getClass().getMethods()) {
            if (m.getParameterCount() == 1) {
                if (m.isAnnotationPresent(EventHandler.class)) {
                    if (Event.class.isAssignableFrom(m.getParameterTypes()[0])) {
                        this.eventBus.registerStop((Class<? extends Event>) m.getParameterTypes()[0], busStop);
                        registeredOne = true;
                    }
                }
            }
        }

        if (registeredOne) {
            Set<EventBusStop> stops = this.registeredBusStops.get(plugin);
            if (stops == null) stops = new HashSet<>();
            stops.add(busStop);
            this.registeredBusStops.put(plugin, stops);
        }
    }

    public void unregisterEeventHandlers(Plugin plugin) {
        Set<EventBusStop> stops = this.registeredBusStops.get(plugin);
        if (stops == null) return;
        stops.forEach(busStop -> {
            for (Method m : busStop.getClass().getMethods()) {
                if (m.getParameterCount() == 1) {
                    if (m.isAnnotationPresent(EventHandler.class)) {
                        if (Event.class.isAssignableFrom(m.getParameterTypes()[0])) {
                            this.eventBus.unregisterStop((Class<? extends Event>) m.getParameterTypes()[0], busStop);
                        }
                    }
                }
            }
        });
        this.registeredBusStops.remove(plugin);
    }

    public Logger getLogger() {
        return this.server.getLogger();
    }

    public CrossChatServer getServer() {
        return server;
    }
}
