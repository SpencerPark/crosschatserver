package io.github.mrblobman.crosschat.server.plugin;

/**
 * Created by Mrblobman on 09/09/2015.
 */
public class InvalidPluginException extends Exception {

    public InvalidPluginException(String s) {
        super(s);
    }

    public InvalidPluginException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public InvalidPluginException(Throwable throwable) {
        super(throwable);
    }

    public InvalidPluginException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
