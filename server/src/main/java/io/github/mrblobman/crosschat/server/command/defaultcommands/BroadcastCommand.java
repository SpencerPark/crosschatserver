package io.github.mrblobman.crosschat.server.command.defaultcommands;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.command.management.CommandHandle;
import io.github.mrblobman.crosschat.server.command.management.CommandHandler;
import io.github.mrblobman.crosschat.server.command.management.CommandSender;
import io.github.mrblobman.crosschat.server.model.ChannelManager;
import io.github.mrblobman.crosschat.server.permission.DefaultPermissions;

/**
 * Created by MrBlobman on 9/12/2015.
 */
public class BroadcastCommand implements CommandHandler {
    private ChannelManager channels;

    public BroadcastCommand(ChannelManager channelSet) {
        this.channels = channelSet;
    }

    @CommandHandle(command = {"broadcast|bc"}, permission = DefaultPermissions.COMMAND_BROADCAST, description = "Send a message to all online players.")
    private void broadcast(CommandSender sender, String... message) {
        channels.sendMessage(ChatMessageBulk.fromDecoratedString(CommandUtils.rebuildSplitVarargs(message)));
    }
}
