package io.github.mrblobman.crosschat.server.event.message;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.model.Player;

/**
 * Created by MrBlobman on 15-09-04.
 */
public class PlayerPrivateMessageEvent extends PlayerMessageEvent {
    private Player recipient;

    public PlayerPrivateMessageEvent(Player sender, Player recipient, ChatMessageBulk message) {
        super(sender, message);
        this.recipient = recipient;
    }

    public Player getRecipient() {
        return recipient;
    }

    public void setRecipient(Player recipient) {
        this.recipient = recipient;
    }

    @Override
    public String getName() {
        return "PlayerPrivateMessageEvent";
    }
}
