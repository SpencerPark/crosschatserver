package io.github.mrblobman.crosschat.server.command.defaultcommands;

import io.github.mrblobman.crosschat.network.data.ChatColor;
import io.github.mrblobman.crosschat.network.data.ChatMessage;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.server.command.management.CommandHandle;
import io.github.mrblobman.crosschat.server.command.management.CommandHandler;
import io.github.mrblobman.crosschat.server.command.management.CommandSender;
import io.github.mrblobman.crosschat.server.database.Database;
import io.github.mrblobman.crosschat.server.model.Player;
import io.github.mrblobman.crosschat.server.model.PlayerRegistry;
import io.github.mrblobman.crosschat.server.permission.DefaultPermissions;
import io.github.mrblobman.crosschat.server.permission.PermissionGroup;
import io.github.mrblobman.crosschat.server.permission.PermissionStore;

/**
 * Created by MrBlobman on 9/26/2015.
 */
public class PermissionCommands implements CommandHandler {
    private Database db;
    private PlayerRegistry registry;

    public PermissionCommands(Database db, PlayerRegistry registry) {
        this.db = db;
        this.registry = registry;
    }

    @CommandHandle(command = {"permissions|perms", "add", "group"}, permission = DefaultPermissions.COMMAND_PERMS_ADDGROUP, description = "Add permissions to the given group or create it if it doesn't exist.")
    private void addGroup(CommandSender sender, String groupName, String... perms) {
        try {
            PermissionStore permStore = new PermissionStore();
            for (String perm : perms) {
                permStore.addPermission(perm);
            }

            boolean groupCreated = PermissionGroup.getGroup(groupName) == null;
            PermissionGroup group = PermissionGroup.createGroup(groupName, permStore);
            this.db.savePermissionGroup(group);
            if (groupCreated) sender.sendMessage(new ChatMessageBulk("Created group " + group.toString()).setColor(ChatColor.GREEN));
            else              sender.sendMessage(new ChatMessageBulk("Added perms to group " + group.toString()).setColor(ChatColor.GREEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @CommandHandle(command = {"permissions|perms", "remove", "group"}, permission = DefaultPermissions.COMMAND_PERMS_REMOVEGROUP, description = "Delete a permission group.")
    private void removeGroup(CommandSender sender, String groupName) {
        this.db.deletePermissionGroup(groupName);
        registry.forEach(player -> player.leavePermissionGroup(groupName));
        sender.sendMessage(new ChatMessageBulk("Removed permission group"));
    }

    @CommandHandle(command = {"permissions|perms", "give"}, permission = DefaultPermissions.COMMAND_PERMS_GIVE_PERM, description = "Give a user a permission.")
    private void givePerm(CommandSender sender, String recipient, String permission) {
        Player player = this.registry.getByName(recipient, false);
        if (player == null) {
            sender.sendMessage(new ChatMessageBulk("Unknown player "+recipient).setColor(ChatColor.RED));
            Player[] possiblePlayers = this.registry.searchFor(recipient);
            if (possiblePlayers.length == 0) return;
            ChatMessageBulk msg = new ChatMessageBulk("Did you mean: \n").setColor(ChatColor.YELLOW);
            for (Player p : possiblePlayers) {
                msg.append(p.getName());
                msg.setClickAction(ChatMessage.CLICK_RUN_COMMAND, "/permissions give "+p.getName()+" "+permission);
            }
            sender.sendMessage(msg);
            return;
        }
        player.givePermission(permission);
        sender.sendMessage(new ChatMessageBulk("Permission "+permission+" given to "+player.getName()).setColor(ChatColor.GREEN));
    }

    @CommandHandle(command = {"permissions|perms", "give", "group"}, permission = DefaultPermissions.COMMAND_PERMS_GIVE_GROUP, description = "Add a user to a permission group.")
    private void giveGroup(CommandSender sender, String recipient, String groupName) {
        Player player = this.registry.getByName(recipient, false);
        if (player == null) {
            sender.sendMessage(new ChatMessageBulk("Unknown player "+recipient).setColor(ChatColor.RED));
            Player[] possiblePlayers = this.registry.searchFor(recipient);
            if (possiblePlayers.length == 0) return;
            ChatMessageBulk msg = new ChatMessageBulk("Did you mean: \n").setColor(ChatColor.YELLOW);
            for (Player p : possiblePlayers) {
                msg.append(p.getName());
                msg.setClickAction(ChatMessage.CLICK_RUN_COMMAND, "/permissions give group "+p.getName()+" "+groupName);
            }
            sender.sendMessage(msg);
            return;
        }
        PermissionGroup group = PermissionGroup.getGroup(groupName);
        if (group == null) {
            sender.sendMessage(new ChatMessageBulk("Unknown group: "+groupName).setColor(ChatColor.RED));
            return;
        }
        player.joinPermissionGroup(group);
        sender.sendMessage(new ChatMessageBulk(player.getName()+" added to group "+group.getGroupName()).setColor(ChatColor.GREEN));
    }

    @CommandHandle(command = {"permissions|perms", "take"}, permission = DefaultPermissions.COMMAND_PERMS_TAKE_PERM, description = "Take a permission from a user.")
    private void takePerm(CommandSender sender, String recipient, String permission) {
        Player player = this.registry.getByName(recipient, false);
        if (player == null) {
            sender.sendMessage(new ChatMessageBulk("Unknown player "+recipient).setColor(ChatColor.RED));
            Player[] possiblePlayers = this.registry.searchFor(recipient);
            if (possiblePlayers.length == 0) return;
            ChatMessageBulk msg = new ChatMessageBulk("Did you mean: \n").setColor(ChatColor.YELLOW);
            for (Player p : possiblePlayers) {
                msg.append(p.getName());
                msg.setClickAction(ChatMessage.CLICK_RUN_COMMAND, "/permissions take "+p.getName()+" "+permission);
            }
            sender.sendMessage(msg);
            return;
        }
        player.removePermission(permission);
        sender.sendMessage(new ChatMessageBulk("Permission "+permission+" removed from "+player.getName()));
    }

    @CommandHandle(command = {"permissions|perms", "take", "group"}, permission = DefaultPermissions.COMMAND_PERMS_TAKE_GROUP, description = "Remove a user from a permission group.")
    private void takeGroup(CommandSender sender, String recipient, String groupName) {
        Player player = this.registry.getByName(recipient, false);
        if (player == null) {
            sender.sendMessage(new ChatMessageBulk("Unknown player "+recipient).setColor(ChatColor.RED));
            Player[] possiblePlayers = this.registry.searchFor(recipient);
            if (possiblePlayers.length == 0) return;
            ChatMessageBulk msg = new ChatMessageBulk("Did you mean: \n").setColor(ChatColor.YELLOW);
            for (Player p : possiblePlayers) {
                msg.append(p.getName());
                msg.setClickAction(ChatMessage.CLICK_RUN_COMMAND, "/permissions give group "+p.getName()+" "+groupName);
            }
            sender.sendMessage(msg);
            return;
        }
        PermissionGroup group = PermissionGroup.getGroup(groupName);
        if (group == null) {
            sender.sendMessage(new ChatMessageBulk("Unknown group: "+groupName).setColor(ChatColor.RED));
            return;
        }
        player.leavePermissionGroup(groupName);
        sender.sendMessage(new ChatMessageBulk(player.getName()+" removed from group "+group.getGroupName()).setColor(ChatColor.GREEN));
    }

    @CommandHandle(command = {"permissions|perms", "list"}, permission = DefaultPermissions.COMMAND_PERMS_LIST, description = "List all of the permissions a player has.")
    private void listPerms(CommandSender sender, String target) {
        Player player = this.registry.getByName(target, false);
        if (player == null) {
            sender.sendMessage(new ChatMessageBulk("Unknown player "+target).setColor(ChatColor.RED));
            Player[] possiblePlayers = this.registry.searchFor(target);
            if (possiblePlayers.length == 0) return;
            ChatMessageBulk msg = new ChatMessageBulk("Did you mean: \n").setColor(ChatColor.YELLOW);
            for (Player p : possiblePlayers) {
                msg.append(p.getName());
                msg.setClickAction(ChatMessage.CLICK_RUN_COMMAND, "/permissions list "+p.getName());
            }
            sender.sendMessage(msg);
            return;
        }
        sender.sendMessage(new ChatMessageBulk(player.listPermissions()).setColor(ChatColor.AQUA));
    }
}
