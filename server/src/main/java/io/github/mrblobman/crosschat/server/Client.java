package io.github.mrblobman.crosschat.server;

import io.github.mrblobman.crosschat.auth.data.GameProfile;
import io.github.mrblobman.crosschat.network.ClientState;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.clientbound.KickPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.DisconnectPacket;
import io.github.mrblobman.crosschat.network.pipeline.common.CompressionHandler;
import io.github.mrblobman.crosschat.network.pipeline.common.EncryptionHandler;
import io.github.mrblobman.crosschat.network.pipeline.common.NullHandler;
import io.github.mrblobman.crosschat.network.pipeline.server.InBoundHandler;
import io.netty.channel.Channel;
import io.netty.channel.ChannelId;

import javax.crypto.SecretKey;
import java.security.GeneralSecurityException;

/**
 * Created by MrBlobman on 15-08-30.
 */
public class Client {
    private ClientManager manager;
    private int protocolId;
    private Channel connection;
    private GameProfile profile;
    private ClientState state = ClientState.HANDSHAKE;

    public Client(ClientManager owner, Channel channel){
        this.manager = owner;
        this.connection = channel;
    }

    public void sendPacket(Packet packet) {
        this.connection.writeAndFlush(packet);
    }

    public void disconnect(String message) {
        disconnect(new ChatMessageBulk(message));
    }

    public void disconnect(ChatMessageBulk message) {
        Packet dcMessage;
        if (state == ClientState.AUTHENTICATED) {
            dcMessage = new KickPacket(message);
        } else {
            dcMessage = new DisconnectPacket(message);
        }
        this.connection.writeAndFlush(dcMessage).addListener(afterWrite -> this.connection.close());
        manager.remove(this);
    }

    public void enableEncryption(SecretKey brownPaint) throws GeneralSecurityException {
        this.connection.pipeline().replace("encryption", "encryption", new EncryptionHandler(brownPaint));
    }

    public void disableEncryption() {
        this.connection.pipeline().replace("encryption", "encryption", new NullHandler());
    }
    public void enableCompression(int threshold) {
        this.connection.pipeline().replace("compression", "compression", new CompressionHandler(threshold));
    }

    public void disableCompression() {
        this.connection.pipeline().replace("compression", "compression", new NullHandler());
    }

    public void switchHandler(InBoundHandler handler) {
        this.connection.pipeline().replace("handler", "handler", handler);
    }

    public boolean isConnected() {
        return this.connection.isOpen();
    }

    public boolean isLoggedIn() {
        return this.profile != null;
    }

    public ClientState getState() {
        return this.state;
    }

    public void setClientState(ClientState state) {
        this.state = state;
    }

    public ChannelId getChannelId() {
        return this.connection.id();
    }

    public int getProtocolId() {
        return this.protocolId;
    }

    public void setProtocolId(int protocolId) {
        this.protocolId = protocolId;
    }

    public void setProfile(GameProfile profile) {
        this.profile = profile;
    }

    public GameProfile getGameProfile() {
        return this.profile;
    }
    
    @Override
    public String toString() {
        if (this.profile != null) return this.profile.getName() + "{" + this.profile.getId() + "}";
        else                            return this.connection.remoteAddress().toString();
    }
}
