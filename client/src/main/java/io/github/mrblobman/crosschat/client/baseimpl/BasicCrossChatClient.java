package io.github.mrblobman.crosschat.client.baseimpl;

import io.github.mrblobman.crosschat.auth.data.GameProfile;
import io.github.mrblobman.crosschat.client.core.CrossChatClient;
import io.github.mrblobman.crosschat.client.core.Message;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.data.ConsoleColors;

/**
 * Created on 04/10/2015.
 */
public class BasicCrossChatClient extends CrossChatClient {

    public BasicCrossChatClient(GameProfile profile, String accessToken) {
        super(profile, accessToken);
    }

    @Override
    protected void onDisconnect(String reason) {
        System.out.println("DISCONNECTED -> " + reason);
    }

    @Override
    protected void onDisconnect(ChatMessageBulk reason) {
        disconnect(ConsoleColors.translate(reason));
    }

    @Override
    public void onMessageReceived(Message message) {
        System.out.println(message.getMessage());
        switch (message.getScope()) {
            case GENERIC:
                System.out.println(ConsoleColors.translate(message.getMessage()));
                return;
            case GLOBAL:
                System.out.println("SERVER -> " + ConsoleColors.translate(message.getMessage()));
                return;
            case CHANNEL:
                System.out.println("CHANNEL[" + message.getSender() + "] -> " + ConsoleColors.translate(message.getMessage()));
                return;
            case PRIVATE:
                System.out.println("PRIVATE[" + message.getSender() + "] -> " + ConsoleColors.translate(message.getMessage()));
                return;
        }
    }
}
