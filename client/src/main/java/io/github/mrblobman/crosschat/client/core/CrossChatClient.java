package io.github.mrblobman.crosschat.client.core;

import io.github.mrblobman.crosschat.auth.data.GameProfile;
import io.github.mrblobman.crosschat.client.baseimpl.CrossChatClientChannelInitializer;
import io.github.mrblobman.crosschat.client.baseimpl.Server;
import io.github.mrblobman.crosschat.network.ClientLoginHandler;
import io.github.mrblobman.crosschat.network.ClientState;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.packets.Protocol;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.ProtocolImpl;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.logging.Logger;

/**
 * Created by MrBlobman on 15-09-02.
 */
public abstract class CrossChatClient {
    private CrossChatServer server;
    private ClientLoginHandler loginHandler;
    private ClientState state = ClientState.HANDSHAKE;
    private Protocol protocol = new ProtocolImpl();
    private GameProfile profile;
    private String accessToken;
    private Logger logger;

    private final EventLoopGroup eventLoop = new NioEventLoopGroup();

    public CrossChatClient(GameProfile profile, String accessToken) {
        this.accessToken = accessToken;
        this.logger = Logger.getLogger("CrossChat");
        this.profile = profile;
    }

    public void connect(String host, int port) {
        if (this.server != null) {
            this.server.close();
        }
        Bootstrap bootstrap = new Bootstrap()
                .group(eventLoop)
                .channel(NioSocketChannel.class)
                .handler(new CrossChatClientChannelInitializer(this))
                .option(ChannelOption.SO_KEEPALIVE, true)
                .option(ChannelOption.TCP_NODELAY, true);
        bootstrap.connect(host, port).addListener((ChannelFuture future) -> {
            if (future.isSuccess()) {
                server = new Server(future.channel(), protocol, host, port);
                loginHandler = new ClientLoginHandler(this, server);
            } else {
                disconnect("Failed to connect to server. " + future.cause().getLocalizedMessage());
            }
        });
    }

    public ClientState getState() {
        return this.state;
    }

    public void setState(ClientState clientState) {
        this.state = clientState;
    }

    public boolean isConnected() {
        return (server != null) && (server.isConnected());
    }

    public void disconnect(String message) {
        if (this.server != null) this.server.close();
        this.server = null;
        this.onDisconnect(message);
    }

    public void disconnect(ChatMessageBulk message) {
        if (this.server != null) this.server.close();
        this.server = null;
        this.onDisconnect(message);
    }

    public Logger getLogger() {
        return this.logger;
    }

    public GameProfile getProfile() {
        return profile;
    }

    public String getAccessToken() {
        return accessToken;
    }
    
    public void setAccessToken(String token) {
        this.accessToken = token;
    }

    public ClientLoginHandler getLoginHandler() {
        return loginHandler;
    }

    public Protocol getProtocol() {
        return this.protocol;
    }

    public void setLoginHandler(ClientLoginHandler handler) {
        this.loginHandler = handler;
    }

    public CrossChatServer getConnectedServer() {
        return this.server;
    }

    protected abstract void onDisconnect(String reason);

    protected abstract void onDisconnect(ChatMessageBulk reason);

    public abstract void onMessageReceived(Message message);

}
