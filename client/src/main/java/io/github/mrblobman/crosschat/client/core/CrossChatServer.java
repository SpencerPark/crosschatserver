package io.github.mrblobman.crosschat.client.core;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.Protocol;
import io.netty.channel.ChannelFuture;
import io.netty.channel.SimpleChannelInboundHandler;

import javax.crypto.SecretKey;
import java.security.GeneralSecurityException;
import java.security.PublicKey;

/**
 * Created on 04/10/2015.
 */
public interface CrossChatServer {
    /**
     * Send a private message to the targeted player.
     * @param target the name of the recipient
     * @param message the message to send
     */
    void sendPrivateMessage(String target, ChatMessageBulk message);

    /**
     * Send a message to everyone in your current channel.
     * @param message the message to send
     */
    void sendChannelMessage(ChatMessageBulk message);

    /**
     * Execute a command.
     * @param command the command to execute
     */
    void sendCommand(String command);

    /**
     * Login to the server using the given {@code username}
     * @param username the username to login with.
     */
    void login(String username);

    /**
     * Check if you are connected to this server.
     * @return true if this server is connected, false otherwise.
     */
    boolean isConnected();

    /**
     * Get the current protocl being used for communication
     * with this server.
     * @return the {@link Protocol} being used for communication
     */
    Protocol getProtocol();

    /**
     * Get this server's id
     * @return the server id for this server
     */
    String getServerId();

    /**
     * Set the server id for this server
     * @param serverId the servers new id
     */
    void setServerId(String serverId);

    /**
     * Get the name of the host for this server
     * @return the name of this servers host
     */
    String getHost();

    /**
     * Get the port that this server is connected to.
     * @return the port
     */
    int getPort();

    /**
     * Enable encryption between you and this server
     * @param secret the secret key
     * @param publicKey the public key
     * @param verifyToken the verify token used to ensure that the exchange is successful
     * @throws GeneralSecurityException if encryption could not be enabled
     */
    void enableEncryption(SecretKey secret, PublicKey publicKey, byte[] verifyToken) throws GeneralSecurityException;

    /**
     * Disable encryption between you and this server
     */
    void disableEncryption();

    /**
     * Enable compression for packets larger than {@code threshold}
     * to and from the server.
     * @param threshold the minimum packet size for compression consideration
     */
    void enableCompression(int threshold);

    /**
     * Disable compression.
     */
    void disableCompression();

    /**
     * Switch out the handler used for inbound packets.
     * @param handler the new inbound handler.
     */
    void switchHandler(SimpleChannelInboundHandler<Packet> handler);

    /**
     * Close this server.
     * @return a ChannelFuture with information about the close.
     */
    ChannelFuture close();
}
