package io.github.mrblobman.crosschat.network;

import io.github.mrblobman.crosschat.auth.AuthClient;
import io.github.mrblobman.crosschat.auth.AuthException;
import io.github.mrblobman.crosschat.client.core.CrossChatClient;
import io.github.mrblobman.crosschat.client.core.CrossChatServer;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.DisconnectPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.EncryptionRequestPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.LoginSuccessPacket;
import io.github.mrblobman.crosschat.util.CryptUtil;

import javax.crypto.SecretKey;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.PublicKey;

/**
 * Created by MrBlobman on 15-09-02.
 */
public class ClientLoginHandler {
    private CrossChatClient client;
    private CrossChatServer server;

    public ClientLoginHandler(CrossChatClient client, CrossChatServer server) {
        this.client = client;
        this.server = server;
        sendHandshake();
    }

    private void sendHandshake() {
        client.setState(ClientState.LOGIN);
        server.login(client.getProfile().getName());
    }

    public void onEncryptionRequest(EncryptionRequestPacket packet) throws GeneralSecurityException {
        server.setServerId(packet.getServerID());
        SecretKey key = CryptUtil.generateSecretKey();

        if (!postJoin(key, packet.getPublicKey())) {
            client.disconnect("Could not log in to server.");
            return;
        }

        server.enableEncryption(key, packet.getPublicKey(), packet.getVerifyToken());
    }

    private boolean postJoin(SecretKey sharedSecret, PublicKey publicKey) {
        String serverId;
        try {
            serverId = CryptUtil.generateServerHash(sharedSecret, publicKey, server.getServerId());
        } catch (GeneralSecurityException | UnsupportedEncodingException e) {
            client.getLogger().severe("Could not hash server id. Reason: "+e.getLocalizedMessage());
            return false;
        }

        //System.out.println("Joining serverID="+serverId);
        try {
            AuthException err = AuthClient.joinServer(client.getAccessToken(), client.getProfile().getId(), serverId);
            if (err != null) {
                client.getLogger().severe("Could not join server. REASON: "+err.getErrorMsg());
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            client.getLogger().severe("Could not join server, "+e.getLocalizedMessage());
            return false;
        }
    }

    public void onLoginSuccess(LoginSuccessPacket packet) {
        this.client.setState(ClientState.AUTHENTICATED);
        this.client.setLoginHandler(null);
    }

    public void onDisconnect(DisconnectPacket packet) {
        //We need to start fresh
        this.client.setState(ClientState.HANDSHAKE);
        this.client.setLoginHandler(null);
        this.client.disconnect(packet.getMessage());
    }
}
