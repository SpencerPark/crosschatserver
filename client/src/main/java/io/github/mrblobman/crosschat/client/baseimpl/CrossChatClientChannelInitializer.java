package io.github.mrblobman.crosschat.client.baseimpl;

import io.github.mrblobman.crosschat.client.core.CrossChatClient;
import io.github.mrblobman.crosschat.network.pipeline.client.InBoundHandler;
import io.github.mrblobman.crosschat.network.pipeline.client.PacketHandler;
import io.github.mrblobman.crosschat.network.pipeline.common.Frame;
import io.github.mrblobman.crosschat.network.pipeline.common.NullHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * Created by MrBlobman on 15-09-02.
 */
public class CrossChatClientChannelInitializer extends ChannelInitializer<NioSocketChannel> {
    private static final NullHandler NULL_HANDLER = new NullHandler();

    private CrossChatClient client;

    public CrossChatClientChannelInitializer(CrossChatClient client) {
        this.client = client;
    }

    @Override
    protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
        //Setup the pipeline
        nioSocketChannel.pipeline()
                .addLast("encryption", NULL_HANDLER)
                .addLast("frame", new Frame())
                .addLast("compression", NULL_HANDLER)
                .addLast("packet", new PacketHandler(client))
                .addLast("handler", new InBoundHandler(client));
    }
}
