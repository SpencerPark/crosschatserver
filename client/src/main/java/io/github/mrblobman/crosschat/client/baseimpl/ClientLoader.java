package io.github.mrblobman.crosschat.client.baseimpl;

import io.github.mrblobman.crosschat.auth.AuthClient;
import io.github.mrblobman.crosschat.auth.data.Agent;
import io.github.mrblobman.crosschat.auth.data.GameProfile;
import io.github.mrblobman.crosschat.auth.data.response.AuthenticationResponse;
import io.github.mrblobman.crosschat.auth.data.response.ErrorResponse;
import io.github.mrblobman.crosschat.auth.data.response.Response;
import io.github.mrblobman.crosschat.auth.endpoint.AuthenticateEndpoint;
import io.github.mrblobman.crosschat.client.core.CrossChatClient;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;

import java.io.IOException;
import java.util.Scanner;
import java.util.UUID;

/**
 * Created on 15-09-03.
 */
public class ClientLoader {
    public static void main(String[] args) throws IOException {
        if (args.length < 4) throw new IllegalArgumentException("Required arguments: username, password, host, port");
        //Login to minecraft, this can be skipped if you already have an accessToken
        String clientToken = UUID.randomUUID().toString();
        String accessToken;
        GameProfile profile;
        try {
            Response authResp = AuthClient.visitAuthEndpoint(new AuthenticateEndpoint(
                    new Agent("minecraft", 1),
                    args[0], //username
                    args[1], //password
                    clientToken
            ));
            if (authResp instanceof ErrorResponse) {
                System.out.println(((ErrorResponse) authResp).getException().getErrorMsg());
                return;
            } else {
                AuthenticationResponse response = (AuthenticationResponse) authResp;
                System.out.println("Default client token: " + clientToken);
                clientToken = response.getClientToken();
                accessToken = response.getAccessToken();
                profile = response.getSelectedProfile();
                System.out.println("Returned access token: " + accessToken);
                System.out.println("Returned client token: " + clientToken);
                System.out.println("Returned selected profile: " + response.getSelectedProfile());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        CrossChatClient client = new BasicCrossChatClient(profile, accessToken);
        client.connect(args[2], Integer.parseInt(args[3]));

        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            String cmd = scanner.nextLine();
            if (cmd.equalsIgnoreCase("stop")) {
                client.disconnect("Quitting");
                return;
            } else if (cmd.startsWith("send")) {
                client.getConnectedServer().sendCommand(cmd.substring(5));
            } else {
                client.getConnectedServer().sendChannelMessage(ChatMessageBulk.fromDecoratedString(cmd));
            }
        }
    }
}
