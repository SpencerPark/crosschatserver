package io.github.mrblobman.crosschat.network.pipeline.client;

import io.github.mrblobman.crosschat.client.core.CrossChatClient;
import io.github.mrblobman.crosschat.network.io.Buffer;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.Protocol;
import io.github.mrblobman.crosschat.network.pipeline.common.PacketReadException;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;

import java.util.List;

/**
 * Created on 15-09-02.
 */
public class PacketHandler extends ByteToMessageCodec<Packet> {
    private Protocol serverProtocol;
    private CrossChatClient client;

    public PacketHandler(CrossChatClient client) {
        this.serverProtocol = client.getProtocol();
        this.client = client;
    }

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Packet packet, ByteBuf byteBuf) throws Exception {
        Buffer buffer = new Buffer(byteBuf);
        buffer.writeVarInt(packet.getId());
        packet.encode(buffer);
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> out) throws Exception {
        //System.out.println("DEBUG -> Received msg from "+channelHandlerContext.channel().remoteAddress()+" to "+channelHandlerContext.channel().localAddress());
        Buffer buffer = new Buffer(byteBuf);

        int id = buffer.readVarInt();
        Class<? extends Packet> packetType = serverProtocol.getRegistry(client.getState()).getPacketById(id, false);
        if (packetType == null) {
            buffer.skipBytes(buffer.readableBytes());
            throw new PacketReadException("Unknown packet id (0x"+Integer.toHexString(id)+").");
        }
        Packet packet = Packet.decode(buffer, packetType);
        //System.out.println("[RECIEVED] 0x"+Integer.toHexString(packet.getId()) + ". Class "+packet.getClass().getName());
        out.add(packet);
    }
}
