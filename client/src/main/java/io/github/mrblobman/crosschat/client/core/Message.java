package io.github.mrblobman.crosschat.client.core;

import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.data.MessageScope;

/**
 * Created on 04/10/2015.
 */
public class Message {
    private String sender;
    private MessageScope scope;
    private ChatMessageBulk message;

    public Message(String sender, MessageScope scope, ChatMessageBulk message) {
        this.sender = sender;
        this.scope = scope;
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public MessageScope getScope() {
        return scope;
    }

    public void setScope(MessageScope scope) {
        this.scope = scope;
    }

    public ChatMessageBulk getMessage() {
        return message;
    }

    public void setMessage(ChatMessageBulk message) {
        this.message = message;
    }
}
