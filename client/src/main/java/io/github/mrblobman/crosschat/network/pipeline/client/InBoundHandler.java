package io.github.mrblobman.crosschat.network.pipeline.client;

import io.github.mrblobman.crosschat.client.core.CrossChatClient;
import io.github.mrblobman.crosschat.client.core.Message;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.PacketIds;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.clientbound.KickPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.clientbound.MessagePacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.DisconnectPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.EncryptionRequestPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.LoginSuccessPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.clientbound.SetCompressionPacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.security.GeneralSecurityException;
import java.util.logging.Level;

/**
 * Created by MrBlobman on 15-09-03.
 */
public class InBoundHandler extends SimpleChannelInboundHandler<Packet> {
    private CrossChatClient client;

    public InBoundHandler(CrossChatClient client) {
        this.client = client;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        client.getLogger().info("Channel connected: " + ctx.channel().remoteAddress());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        client.getLogger().info("Channel disconnected: " + ctx.channel().remoteAddress());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        client.getLogger().log(Level.SEVERE, "Exception in channel. Disconnecting " + ctx.channel().remoteAddress() + ". Reason: " + cause.getClass().getSimpleName() + "->" + cause.getLocalizedMessage());
        client.disconnect("ERROR: " + cause.getLocalizedMessage());
    }

    @Override
    protected void messageReceived(ChannelHandlerContext channelHandlerContext, Packet packet) throws Exception {
        switch(client.getState()) {
            case HANDSHAKE:
                //Nothing to do.
                client.getLogger().warning("Received packet while handshaking. "+packet.toString());
                return;
            case LOGIN:
                handleLogin(packet);
                return;
            case AUTHENTICATED:
                handleRegular(packet);
                return;
            case STATUS:
                handleStatus(packet);
                return;
        }
    }

    private void handleLogin(Packet packet) throws GeneralSecurityException {
        if (client.getLoginHandler() == null) {
            client.getLogger().severe("No login handler available.");
            client.disconnect("No login handler available.");
        } else {
            switch (packet.getId()) {
                case PacketIds.ClientBound.ENCRYPTION_REQUEST:
                    client.getLoginHandler().onEncryptionRequest((EncryptionRequestPacket) packet);
                    return;
                case PacketIds.ClientBound.LOGIN_SUCCESS:
                    client.getLoginHandler().onLoginSuccess((LoginSuccessPacket) packet);
                    return;
                case PacketIds.ClientBound.DISCONNECT:
                    client.getLoginHandler().onDisconnect((DisconnectPacket) packet);
                    return;
                case PacketIds.ClientBound.SET_COMPRESSION:
                    client.getConnectedServer().enableCompression(((SetCompressionPacket) packet).getThreshold());
                    return;
                default:
                    client.getLogger().warning("Received packet while in login state. Id 0x" + Integer.toHexString(packet.getId()) + ". Class " + packet.getClass().getName());
            }
        }
    }

    private void handleRegular(Packet packet) {
        switch (packet.getId()) {
            case PacketIds.ClientBound.KICKED:
                client.disconnect(((KickPacket) packet).getMessage());
                break;
            case PacketIds.ClientBound.MESSAGE:
                MessagePacket messagePacket = (MessagePacket) packet;
                client.onMessageReceived(new Message(messagePacket.getSender(), messagePacket.getScope(), messagePacket.getMessage()));
                break;
            case PacketIds.ClientBound.POSTSET_COMPRESSION:
                client.getConnectedServer().enableCompression(((SetCompressionPacket) packet).getThreshold());
                break;
        }
        //System.out.println("[REGULAR] Id 0x"+Integer.toHexString(packet.getId())+". Class "+packet.getClass().getName());
    }

    private void handleStatus(Packet packet) {
        //System.out.println("[STATUS] Id 0x"+Integer.toHexString(packet.getId())+". Class "+packet.getClass().getName());
    }
}
