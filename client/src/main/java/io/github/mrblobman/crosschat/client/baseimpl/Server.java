package io.github.mrblobman.crosschat.client.baseimpl;

import io.github.mrblobman.crosschat.client.core.CrossChatServer;
import io.github.mrblobman.crosschat.network.data.ChatMessageBulk;
import io.github.mrblobman.crosschat.network.packets.Packet;
import io.github.mrblobman.crosschat.network.packets.Protocol;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.serverbound.ChannelMessagePacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.serverbound.CommandPacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.authenticated.serverbound.PrivateMessagePacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.handshake.serverbound.HandshakePacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.serverbound.EncryptionResponsePacket;
import io.github.mrblobman.crosschat.network.packets.v1_0_R0.login.serverbound.LoginStartPacket;
import io.github.mrblobman.crosschat.network.pipeline.common.CompressionHandler;
import io.github.mrblobman.crosschat.network.pipeline.common.EncryptionHandler;
import io.github.mrblobman.crosschat.network.pipeline.common.NullHandler;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.SimpleChannelInboundHandler;

import javax.crypto.SecretKey;
import java.security.GeneralSecurityException;
import java.security.PublicKey;

/**
 * Created by MrBlobman on 15-09-02.
 */
public class Server implements CrossChatServer {
    private Channel connection;
    private Protocol protocol;
    private String serverId;
    private String host;
    private int port;

    public Server(Channel connection, Protocol protocol, String host, int port) {
        this.connection = connection;
        this.protocol = protocol;
        this.host = host;
        this.port = port;
    }

    private void sendPacket(Packet packet) {
        if (!connection.isOpen()) throw new IllegalStateException("This server is disconnected.");
        connection.writeAndFlush(packet);
    }

    @Override
    public boolean isConnected() {
        return this.connection.isOpen();
    }

    @Override
    public void sendPrivateMessage(String target, ChatMessageBulk message) {
        sendPacket(new PrivateMessagePacket(target, message));
    }

    @Override
    public void sendChannelMessage(ChatMessageBulk message) {
        sendPacket(new ChannelMessagePacket(message));
    }

    @Override
    public void sendCommand(String command) {
        sendPacket(new CommandPacket(command));
    }

    @Override
    public void login(String username) {
        sendPacket(new HandshakePacket(
                getProtocol().getVersionId(),
                getHost(),
                getPort(),
                HandshakePacket.LOGIN
        ));
        sendPacket(new LoginStartPacket(username));
    }

    @Override
    public Protocol getProtocol() {
        return protocol;
    }

    @Override
    public String getServerId() {
        return serverId;
    }

    @Override
    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public void enableEncryption(SecretKey secretKey, PublicKey publicKey, byte[] verifyToken) throws GeneralSecurityException {
        sendPacket(new EncryptionResponsePacket(secretKey, publicKey, verifyToken));
        this.connection.pipeline().replace("encryption", "encryption", new EncryptionHandler(secretKey));
    }

    @Override
    public void disableEncryption() {
        this.connection.pipeline().replace("encryption", "encryption", new NullHandler());
    }

    @Override
    public void enableCompression(int threshold) {
        this.connection.pipeline().replace("compression", "compression", new CompressionHandler(threshold));
    }

    @Override
    public void disableCompression() {
        this.connection.pipeline().replace("compression", "compression", new NullHandler());
    }

    @Override
    public void switchHandler(SimpleChannelInboundHandler<Packet> handler) {
        this.connection.pipeline().replace("handler", "handler", handler);
    }

    @Override
    public ChannelFuture close() {
        return this.connection.close();
    }
}
